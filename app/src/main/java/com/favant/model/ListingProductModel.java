package com.favant.model;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bear on 8/30/2015.
 */
public class ListingProductModel extends Model {
    private static final String TAG = "ListingProduct";

    private int admin_id = 0;
    private float promotion_percent = 0;
    @NonNull
    private String name = "";
    private String product_sku = "";
    private int product_id = 0;
    private int parent_id = 0;
    private float original_price = 0.0f;
    private float final_price = 0.0f;
    private float percentage = 0.0f;
    private List<String> arr_images = new ArrayList<>();
    private String img_url = "";
    private ArrayList<DataChangeListener> listeners = new ArrayList<DataChangeListener>();


    public int getAdminId() {
        return admin_id;
    }

    public float getPromotionPercent() {
        return promotion_percent;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public String getProductSku() {
        return product_sku;
    }

    public int getProductId() {
        return product_id;
    }

    public int getParentId() {
        return parent_id;
    }

    public float getOriginalPrice() {
        return original_price;
    }

    public float getFinalPrice() {
        return final_price;
    }

    public float getPercentage() {
        return percentage;
    }

    public List<String> getArrImages() {
        return arr_images;
    }

    public String getImgUrl() {
        return img_url;
    }

    public void notifyDataChanged() {
        for (int i = 0; i < listeners.size(); i++) {
            DataChangeListener l = listeners.get(i);
            if (l != null) l.dataChanged();
        }
    }

    public void addOnDataChangedListener(DataChangeListener listener) {
        listeners.add(listener);
    }

    public void removeOnDataChangedListener(DataChangeListener listener) {
        listeners.remove(listener);
    }

    public static interface DataChangeListener {
        public void dataChanged();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ListingProductModel)) return super.equals(o);
        ListingProductModel o2 = (ListingProductModel) o;
        return product_id == o2.product_id;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
