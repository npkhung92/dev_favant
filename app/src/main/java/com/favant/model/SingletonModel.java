package com.favant.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.favant.util.PreferenceManager;

/**
 * Created by Hung on 11/24/2015.
 */
public class SingletonModel extends Model {
    private Context context = null;

    public Context getContext() {
        return context;
    }

    public void setContext(Context c) {
        context = c;
    }

    private String getPreferenceKey(String key) {
        return this.getClass().getName() + "|" + key;
    }

    public boolean saveToPreference(String key, String value) {
        if (context == null) return false;
        SharedPreferences sharedPref = context.getSharedPreferences(PreferenceManager.SHARED_PREFERENCE_KEY, Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = sharedPref.edit();
        Log.v("Favant", "save to shared preference: " + this.getClass().getName() + "|" + key + " => " + value);
        editor.putString(getPreferenceKey(key), value);
        editor.commit();
        return true;
    }

    public String getStringFromPreference(String key, String default_value) {
        if (context == null) return default_value;
        SharedPreferences sharedPref = context.getSharedPreferences(PreferenceManager.SHARED_PREFERENCE_KEY, Context.MODE_MULTI_PROCESS);
        String value = sharedPref.getString(getPreferenceKey(key), default_value);
        return value;
    }

    public String getStringFromPreference(String key) {
        return getStringFromPreference(key, null);
    }
}
