package com.favant.model;

import com.google.gson.Gson;

import java.io.Serializable;
import java.lang.reflect.Field;

/**
 * Created by Bear on 8/28/2015.
 */
public class Model implements Serializable {

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public void updateData(Model data) {
        Field[] fields = getClass().getDeclaredFields();
        for (Field field : fields) {
            updateData(data, field);
        }
    }

    private void updateData(Model data, Field field) {
        try {
            field.setAccessible(true);
            field.set(this, field.get(data));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
