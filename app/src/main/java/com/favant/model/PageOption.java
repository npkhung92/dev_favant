package com.favant.model;

import android.os.Bundle;

/**
 * Created by Bear on 8/28/2015.
 */
public class PageOption {
    public String page_type;
    public Bundle data;
    public boolean autoload_content;
    public boolean autoload_on_create;
    private boolean reverse;

    public PageOption setPageType(String pt) {
        page_type = pt;
        return this;
    }

    public PageOption setData(Bundle d) {
        data = d;
        return this;
    }

    public PageOption setAutoLoadContent(boolean b) {
        autoload_content = b;
        return this;
    }

    public PageOption setAutoloadOnCreate(boolean b) {
        autoload_on_create = b;
        return this;
    }

    public boolean isReverse() {
        return reverse;
    }

    public PageOption setReverse(boolean reverse) {
        this.reverse = reverse;
        return this;
    }
}