package com.favant.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASUS on 2/28/2016.
 */
public class FCategory extends Model {

    private int entity_id;
    private String name;
    private String url;
    private int level;
    private int parent_id;
    private int position;
    private int product_count;
//    private List<FCategory> child = new ArrayList<>();

    public int getPosition() {
        return position;
    }

    public int getParentId() {
        return parent_id;
    }

    public int getLevel() {
        return level;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public int getEntityId() {
        return entity_id;
    }

    public int getProductCount() {
        return product_count;
    }

//    public List<FCategory> getChildList() {
//        return child;
//    }
//
//    public void addChild(FCategory c) {
//        child.add(c);
//    }
}
