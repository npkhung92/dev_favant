package com.favant.model;

import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Hung on 11/24/2015.
 */
public class SearchHistory extends SingletonModel{
    private static final String SEARCH_HISTORY_KEY = "search_history";
    private static final int MAX_SIZE = 10;

    private static final String TAG = "SearchHistory";

    private static SearchHistory mSearchHistory;
    private static ArrayList<Runnable> updateListener = new ArrayList<Runnable>();

    private ArrayList<SearchModel> search_history = new ArrayList<SearchModel>();

    private SearchHistory() {

    }

    public void loadSearchHistory() {
        String search_history_json = getStringFromPreference(SEARCH_HISTORY_KEY);
        if (search_history_json == null) return;
        try {
            JSONArray arr = new JSONArray(search_history_json);
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj = arr.optJSONObject(i);
                if (obj != null) {
                    String input = obj.optString("user_input");
                    String text = obj.optString("html_text");
                    String url = obj.optString("url");
                    if (text != null && !text.equals("") && input != null && !input.equals("")) {
                        SearchModel sm = new SearchModel(input, text, url);
                        search_history.add(sm);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.v(TAG, String.valueOf(search_history));
    }

    public static SearchHistory getInstane() {
        if (mSearchHistory == null) mSearchHistory = new SearchHistory();
        return mSearchHistory;
    }

    public void addSearch(SearchModel search) {
        ArrayList<SearchModel> tmp = new ArrayList<SearchModel>();
        tmp.add(search);
        for (SearchModel sm : search_history) {
            if (tmp.size() >= MAX_SIZE) break;
            if (!sm.html_text.equals(search.html_text) || !sm.url.equals(search.url)) {
                tmp.add(sm);
            }
        }

        search_history.clear();
        search_history.addAll(tmp);
        save();
        for (Runnable r : updateListener) {
            r.run();
        }
        return;
    }

    private void save() {
        JSONArray arr = new JSONArray();
        for (SearchModel sm : search_history) {
            JSONObject obj = new JSONObject();
            try {
                obj.put("user_input", sm.user_input);
                obj.put("html_text", sm.html_text);
                obj.put("url", sm.url);

                arr.put(obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        saveToPreference(SEARCH_HISTORY_KEY, arr.toString());
    }

    public ArrayList<SearchModel> getSearchHistory() {
        ArrayList<SearchModel> tmp = new ArrayList<SearchModel>();
        tmp.addAll(search_history);

        return tmp;
    }

    public void deleteAll() {
        saveToPreference(SEARCH_HISTORY_KEY, "");

        search_history = new ArrayList<SearchModel>();

        for (Runnable r : updateListener) {
            r.run();
        }
    }

    public static void addOnUpdateListener(Runnable update) {
        updateListener.add(update);
    }

    public static class SearchModel {
        @NonNull
        public String user_input = "";
        @NonNull
        public String html_text = "";
        @NonNull
        public String url = "";

        public SearchModel(String i, String t, String u) {
            user_input = i;
            html_text = t;
            url = u;
        }


        @Override
        public String toString() {
            return html_text + ", " + url;
        }
    }
}
