package com.favant.model;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by Bear on 9/20/2015.
 */
public class Category extends Model {
    public int parent_category_id;
    public int category_id = 0;
    public String path = "";
    public String path_minimal = "";
    public String url_key = "";
    public String url_path = "";
    public String name = "";
    public int display_order;

    public Category(String name) {
        this.name = name;
    }

    public Category() {

    }

    public ArrayList<Category> child = new ArrayList<Category>();
    public int level = 1;

    public int getCategoryLevel() {
        int cat_level = path.split("/").length - 2;
        return cat_level;
    }

    public void addChild(Category c) {
        child.add(c);
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
