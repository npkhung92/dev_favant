package com.favant.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Hung on 12/13/2015.
 */
public class CartProduct extends Model {

    private static final String TAG = "CartProduct";
    public ProductDetailModel product_model;
    public Merchant merchant_model;
    public int quantity;
    public LinkedHashMap<ProductDetailModel.Attribute, ProductDetailModel.AttributeOption> user_options;
    public int shipping_to;
    public String shipping_carrier_code;


    public CartProduct(ProductDetailModel pm, Merchant m, int q, HashMap<ProductDetailModel.Attribute,
            ProductDetailModel.AttributeOption> uo, int st, String scc) {
        Log.v(TAG, "before " + String.valueOf(uo));
        if (uo == null)
            user_options = new LinkedHashMap<ProductDetailModel.Attribute, ProductDetailModel.AttributeOption>();
        if (uo != null || uo.entrySet().size() != 0) {
            //sort attribute id in a cart product
            Set<Map.Entry<ProductDetailModel.Attribute, ProductDetailModel.AttributeOption>> entries = uo.entrySet();
            CartAttributeComparator ca_comp = new CartAttributeComparator();
            List<Map.Entry<ProductDetailModel.Attribute, ProductDetailModel.AttributeOption>> listOfEntries = new ArrayList<Map.Entry<ProductDetailModel.Attribute, ProductDetailModel.AttributeOption>>(entries);
            Collections.sort(listOfEntries, ca_comp);
            user_options = new LinkedHashMap<ProductDetailModel.Attribute, ProductDetailModel.AttributeOption>(listOfEntries.size());
            for (Map.Entry<ProductDetailModel.Attribute, ProductDetailModel.AttributeOption> e : listOfEntries) {
                Log.v(TAG, "entry " + e);
                user_options.put(e.getKey(), e.getValue());
            }

            Log.v(TAG, "after " + String.valueOf(user_options));
        }

        product_model = pm;
        merchant_model = m;
        quantity = q;
//        user_options = uo;
        shipping_to = st;
        shipping_carrier_code = scc;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof CartProduct)) return false;
        CartProduct cp = (CartProduct) o;
        if (product_model.id != cp.product_model.id) return false;
        for (Map.Entry<ProductDetailModel.Attribute, ProductDetailModel.AttributeOption> e : user_options.entrySet()) {
            boolean found = false;
            for (Map.Entry<ProductDetailModel.Attribute, ProductDetailModel.AttributeOption> e1 : cp.user_options.entrySet()) {
                if (e.getKey().id == e1.getKey().id && e.getValue().option_id == e1.getValue().option_id) {
                    found = true;
                    break;
                }
            }
            if (!found) return false;
        }

        return true;
    }

    @Override
    public String toString() {
        String attr = "";
        for (Map.Entry<ProductDetailModel.Attribute, ProductDetailModel.AttributeOption> e : user_options.entrySet()) {
            attr += "{" + e.getKey().id + ", " + e.getKey().name + "}";
            attr += ": {" + e.getValue().product_option_id + ", " + e.getValue().value + "}, ";
        }
        attr = "[" + attr + "]";
        return "{id: " + product_model.id + ", merchant_id: " + merchant_model.merchant_id + "attribute:" + attr + "}";
    }

    class CartAttributeComparator implements Comparator<Map.Entry<ProductDetailModel.Attribute, ProductDetailModel.AttributeOption>> {

//        HashMap<ProductDetailModel.Attribute, ProductDetailModel.AttributeOption> map;
//
//        public CartAttributeComparator(HashMap<ProductDetailModel.Attribute, ProductDetailModel.AttributeOption> base) {
//            this.map = base;
//        }

        @Override
        public int compare(Map.Entry<ProductDetailModel.Attribute, ProductDetailModel.AttributeOption> lhs, Map.Entry<ProductDetailModel.Attribute, ProductDetailModel.AttributeOption> rhs) {
            String v1 = lhs.getKey().option;
            String v2 = rhs.getKey().option;
            return v1.compareTo(v2);
        }
    }
}
