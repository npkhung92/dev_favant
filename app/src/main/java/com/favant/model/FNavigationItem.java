package com.favant.model;

import com.favant.model.Model;

/**
 * Created by Hung on 10/27/2015.
 */
public class FNavigationItem extends Model {
    public static class NavigationObject {
        public final Integer[] category_id;
        public final String name;
        public final Integer flag_id;
        public final String url_key;
        public final boolean is_header;


        public NavigationObject(Integer[] category_id, String name, Integer flag_id, String url_key, boolean is_header) {
            this.category_id = category_id;
            this.name = name;
            this.flag_id = flag_id;
            this.url_key = url_key;
            this.is_header = is_header;
        }

        @Override
        public String toString() {
            return "[" + category_id + ", " + name + "]";
        }
    }
}
