package com.favant.model;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Bear on 9/27/2015.
 */
public class ProductDetailModel extends Model {
    public int id = 0;
    public String category_id = "";
    public String name = "";
    public String cat_path = "";
    public String sku_user = "";
    public String short_description = "";
    public String description = "";
    public float price = 0;
    public float final_price = 0;
    public float special_price = 0;
    public float promotion_percent = 0;
    public int is_promotion = 0;
    public int rating = 0;
    public int total_star = 0;
    public int total_hoidap = 0;
    public float good_review_percent = 0;
    public float average_star = 0;
    public int weight = 0;
    public ArrayList<String> images = new ArrayList<String>();

    public ArrayList<Attribute> attributes = new ArrayList<Attribute>();

    public int order_count = 0;
    public int view_count = 0;

    public int status = -1;
    public int status_new = -1;
    public int stock_status = -1;
    public int hethang = 0;
    public int in_stock = 0;
    public int brand_id;

    public void copyImagesFromJsonArray(JSONArray data) {
        if (data == null) return;
        for (int i = 0; i < data.length(); i++) {
            try {
                String img = data.getString(i);
                images.add(img);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void calculateFinalPrice() {
        if (is_promotion == 1 && special_price > 0) final_price = special_price;
        else final_price = price;
    }

    public static class Attribute {
        public int id = 0;
        public String name = "";
        public String option = "";
        public int type = 1;
        public ArrayList<AttributeOption> options = new ArrayList<AttributeOption>();

        @Override
        public String toString() {
            return "{attr_id: " + id + ", name: " + name + "}";
        }
    }

    public static class AttributeOption {
        public String option_id = "";
        public String value = "";
        public String product_option_id = "";
        public int color_id = 0;
        public String color_hex_rgb = "";
        public String image = "";

        @Override
        public String toString() {
            return "{option_id: " + option_id + ", value: " + value + ", color_id: " + color_id + "}";
        }
    }
}
