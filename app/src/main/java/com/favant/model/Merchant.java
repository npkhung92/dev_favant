package com.favant.model;

import java.util.ArrayList;

/**
 * Created by Hung on 11/15/2015.
 */
public class Merchant extends Model {
    public int merchant_id = 0;
    public String name = "";
    public String username = "";
    public String logo = "";
    public String telephone = "";

    public int score = 0;
    public float good_review_percent = 0;

    public int lotus = 0;
    public String lotus_class = "red";

    public boolean is_certified = false;

    public Loyalty loyalty = new Loyalty();

    public ArrayList<ShippingSupport> shipping_info = new ArrayList<ShippingSupport>();

    public int warehouse_city;

    public String fpt_id = "1318098211";
    public float promotion_app;

    public static class ShippingSupport {
        public float order_amount = 0;
        public float seller_support_fee = 0;
        public int position = 1;
        public boolean is_active = false;
    }

    public static class Loyalty {
        public boolean is_active;
        public long deactivated_time;
        public int percent;
        public long update_time;
    }
}
