package com.favant.fview.cart;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import com.favant.MasterActivity;
import com.favant.R;

/**
 * Created by Hung on 12/12/2015.
 */
public class CartPopup {
    public static final int STATE_VIEWING = 1;
    public static final int STATE_EDITING = 2;
    private MasterActivity mMasterActivity;
    private final Context context;
    private View popup_content;

    private View empty_view;
    private View continue_btn;
    private ListView cartList;
    private CartAdapter cartListAdapter;
    public CartPopup(Context c, MasterActivity ma) {
        context = c;
        mMasterActivity = ma;
    }

    private LayoutInflater getLayoutInflater() {
        return LayoutInflater.from(context);
    }

    public View findViewById(int res_id) {
        return popup_content.findViewById(res_id);
    }

    public void loadPopup() {
        popup_content = getLayoutInflater().inflate(R.layout.cart_popup, null);
    }

    public View getPopupContent() {
        return popup_content;
    }

    public void loadCartData(){
        empty_view = findViewById(R.id.empty_view);
        continue_btn = findViewById(R.id.continue_btn);
        cartList = (ListView) findViewById(R.id.cart_list);
        cartListAdapter = new CartAdapter(mMasterActivity, context);

//        cartList.setAdapter(cartListAdapter);

    }

}
