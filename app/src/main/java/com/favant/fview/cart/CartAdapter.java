package com.favant.fview.cart;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.favant.MasterActivity;
import com.favant.R;
import com.favant.fview.common.FImageView;
import com.favant.fview.common.FTextView;
import com.favant.model.CartProduct;

import java.util.List;

/**
 * Created by Hung on 12/13/2015.
 */
public class CartAdapter extends BaseAdapter {
    private MasterActivity mMasterActivity;
    private Context context;
    private List<CartProduct> cp_list;
    private int editState = CartPopup.STATE_VIEWING;

    public CartAdapter(MasterActivity ma, Context c){
        mMasterActivity = ma;
        context = c;
    }

    public void addCartList(List<CartProduct> _cpList) {
        cp_list.addAll(_cpList);
    }

    public LayoutInflater getLayoutInflater() {
        return LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return cp_list.size();
    }

    @Override
    public Object getItem(int position) {
        return cp_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            CartViewHolder vh = new CartViewHolder();
            view = getLayoutInflater().inflate(R.layout.cart_component, null);
            vh.pro_image = (FImageView) view.findViewById(R.id.pro_image);
            vh.pro_name = (FTextView) view.findViewById(R.id.pro_name);
            vh.pro_color = (FTextView) view.findViewById(R.id.pro_color);
            vh.pro_size = (FTextView) view.findViewById(R.id.pro_size);
            vh.unit_price = (FTextView) view.findViewById(R.id.unit_price);
            vh.remove_btn = view.findViewById(R.id.remove_btn);
            view.setTag(vh);
        }
        if (view.getTag() instanceof CartViewHolder) {
            CartViewHolder vh = new CartViewHolder();
            vh.pro_image.setImageResource(R.drawable.pro_img);
            vh.remove_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
        return view;
    }

    public void notifyEditStateChanged(int editState) {
        this.editState = editState;
        notifyDataSetChanged();
    }

    public void clearCart(){
        cp_list.clear();
    }

    public class CartViewHolder {
        public FImageView pro_image;
        public FTextView pro_name;
        public FTextView pro_size;
        public FTextView pro_color;
        public View remove_btn;
        public FTextView unit_price;
    }

}
