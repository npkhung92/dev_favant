package com.favant.fview.home.HomeFragment2;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.favant.MasterActivity;
import com.favant.R;
import com.favant.fview.common.FImageView;
import com.favant.util.UrlParser;

import java.util.ArrayList;

/**
 * Created by Hung on 11/28/2015.
 */
public class BannerAdapter2 extends BaseAdapter {
    private static final String TAG = "BannerAdapter";
    private final MasterActivity mMasterActivity;
    private ArrayList<Banner> banners = new ArrayList<Banner>();
    private ArrayList<Integer> img_id = new ArrayList<Integer>(4);

    public BannerAdapter2(MasterActivity ma) {
        mMasterActivity = ma;
    }

    @Override
    public int getCount() {
        return img_id.size();
    }

    @Override
    public Object getItem(int i) {
        return img_id.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Log.v(TAG, "index " + i);
        FImageView img;
        if (view == null) {
            img = new FImageView(mMasterActivity);
            img.setRatioW(640);
            img.setRatioH(300);
            img.setAdjustViewBounds(true);
            view = img;
        } else {
            img = (FImageView) view;
        }
//        final Banner b = (Banner) getItem(i);
//        img.setImageUrl(b.imgUrl, false);
        img.setImageResource(img_id.get(i));
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                UrlParser up = new UrlParser(mMasterActivity);
//                up.parseAndGoToPage(b.url, false);
            }
        });
        return view;
    }

    public void addExamBanner(){
        img_id.add(R.drawable.test_banner);
        img_id.add(R.drawable.test_banner);
        img_id.add(R.drawable.test_banner);
        img_id.add(R.drawable.test_banner);
        notifyDataSetChanged();
    }

    public void addBanner(String imgUrl, String url) {
        Banner b = new Banner(imgUrl, url);
        banners.add(b);

        notifyDataSetChanged();
    }

    public void reset() {
        banners.clear();
        img_id.clear();
        notifyDataSetChanged();
    }

    private static class Banner {
        public String imgUrl;
        public String url;

        public Banner(String iu, String u) {
            imgUrl = iu;
            url = u;
        }
    }
}
