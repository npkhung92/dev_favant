package com.favant.fview.home.HomeFragment2;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.favant.MasterActivity;
import com.favant.fview.common.ListingProductBox;
import com.favant.model.ListingProductModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hung on 11/29/2015.
 */
public class BestSellerAdapter2 extends BaseAdapter {
    private final MasterActivity mMasterActivity;
    private List<ListingProductModel> product_list = new ArrayList<>();

    public BestSellerAdapter2(MasterActivity ma) {
        mMasterActivity = ma;
    }

    @Override
    public int getCount() {
        return product_list.size();
    }

    @Override
    public Object getItem(int i) {
        return product_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = new ListingProductBox(mMasterActivity);
        }

        ((ListingProductBox) view).setListingProductModel((ListingProductModel) getItem(i));
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        return view;
    }

    public void addProduct(ListingProductModel pm) {
        product_list.add(pm);
        notifyDataSetChanged();
    }
    public void addProductList(List<ListingProductModel> product_list) {
        this.product_list.addAll(product_list);
        notifyDataSetChanged();
    }

    public void reset() {
        product_list.clear();
        notifyDataSetChanged();
    }
}
