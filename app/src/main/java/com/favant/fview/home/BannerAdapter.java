package com.favant.fview.home;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.favant.MasterActivity;
import com.favant.R;
import com.favant.fview.common.FImageView;

import java.util.ArrayList;

/**
 * Created by Hung on 11/1/2015.
 */
public class BannerAdapter extends RecyclerView.Adapter<BannerAdapter.ViewHolder> {

    private static final String TAG = "BannerAdapter";
    private final MasterActivity mMasterActivity;
    private ArrayList<Banner> banners = new ArrayList<Banner>();

    public BannerAdapter(MasterActivity ma) {
        mMasterActivity = ma;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.banner_component, parent, false);
//        FImageView img;
//            img = new FImageView(mMasterActivity);
//            img.setRatioW(590);
//            img.setRatioH(240);
//            img.setAdjustViewBounds(true);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        final Banner b = (Banner) banners.get(position);
//        holder.pro_img.setImageUrl(b.imgUrl, false);
        holder.pro_img.setImageResource(R.drawable.test_banner);
        if (position == banners.size() - 1) {
            FImageView iv = holder.pro_img;
            Log.v(TAG, banners.get(position).imgUrl);
        }
    }

    @Override
    public int getItemCount() {
        return banners.size();
    }

    public void addBanner(String imgUrl, String url) {
        Banner b = new Banner(imgUrl, url);
        banners.add(b);

        notifyDataSetChanged();
    }

    public void reset() {
        banners.clear();
        notifyDataSetChanged();
    }

    private static class Banner {
        public String imgUrl;
        public String url;

        public Banner(String iu, String u) {
            imgUrl = iu;
            url = u;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        FImageView pro_img;
        public ViewHolder(View itemView) {
            super(itemView);
            pro_img = (FImageView) itemView.findViewById(R.id.banner_img);
        }
    }
}
