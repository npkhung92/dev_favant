package com.favant.fview.home;

import android.util.Log;

import com.favant.R;
import com.favant.fview.common.FActionBar;
import com.favant.fview.common.FFragment;
import com.favant.fview.common.LeftNavigator.FLeftNavigationDrawer;
import com.favant.fview.common.OwlCarousel3;
import com.favant.model.ListingProductModel;
import com.favant.util.Net;
import com.favant.util.WebService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Hung on 10/27/2015.
 */
public class HomeFragment extends FFragment {
    private final static String TAG = "Favant_HomepageFragment";

    public HomeFragment() {
        setLayout(R.layout.master_page, R.layout.home_page);
    }

    @Override
    protected void loadFActionBar(FActionBar ab) {
        ab.showBarButton();
        ab.showLocationButton();
        ab.showSearchButton();
        ab.showCartButton();
        ab.showLogo();
        ab.setCallback(new HomeActionBarCallBack());
    }

    @Override
    protected void onPreCreate() {

    }

    @Override
    protected void onCreate() {
        loadBanner();
        loadNewArrivals();
        loadBestSellers();
    }

    private void loadBanner(){
//        final HomeCache homeCache = new HomeCache(MasterActivity.getInstance().getApplication());
//        final Cursor bannerCursor = homeCache.getBanner();
        final OwlCarousel3 owl = (OwlCarousel3) findViewById(R.id.banner_wrapper);
        final BannerAdapter adapter = new BannerAdapter(mMasterActivity);
        WebService.getJson("homepage/home-banner-slider/?for_app=1&get_version=1", new Net.JsonResultAdapter() {
            @Override
            public void success(Object res) {
                if (!(res instanceof JSONObject)) return;
                final JSONObject objVersion = (JSONObject) res;
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        WebService.getJson("homepage/home-banner-slider/", new Net.JsonResultAdapter() {
                            @Override
                            public void success(Object res) {
                                if (!(res instanceof JSONArray)) return;
                                JSONArray res2 = (JSONArray) res;

                                try {
                                    final String version = objVersion.getString("version");
                                    if (version == null || true) {
                                        final List<Map<String, String>> arrayBanner = new ArrayList<Map<String, String>>();
                                        Log.d(TAG, "version banner  diff");
                                        adapter.reset();
                                        for (int i = 0; i < res2.length(); i++) {
                                            final JSONObject obj = res2.optJSONObject(i);
                                            final String img_url = obj.optString("image");
                                            final String url = obj.optString("url");
                                            if (owl != null) {
                                                adapter.addBanner(img_url, url);
                                                //add data to cache array
                                                Map<String, String> banner = new HashMap<String, String>();
//                                                banner.put(HomeCache.BannerColumns.IMG_URL, img_url);
//                                                banner.put(HomeCache.BannerColumns.URL, url);
//                                                arrayBanner.add(banner);
                                                //homeCache.saveBanner(imgUrl, imgPath);
                                            }
                                        }
                                        owl.setAdapter(adapter);
//                                        if (mHomeFragment != null) mHomeFragment.hideLoading();
//                                        hidePullToRefresh();
                                        // save cache after load

//                                        saveBannerToDb(arrayBanner, version, countCacheData);
                                    } else {
//                                        hidePullToRefresh();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                };
                if (fragment_created.get()) r.run();
                else addOnCreateObserver(r);
            }
        });
    }

    private void loadNewArrivals() {
//        final Version ver = new Version(MasterActivity.getInstance().getApplicationContext());
//        final String dealVersion = ver.getVersion(HomeCache.TABLES.DEAL);
//        Log.d(TAG, "deal version cache: " + dealVersion);
//        final HomeCache homeCache = new HomeCache(MasterActivity.getInstance().getApplication());
//        final Cursor dealCursor = homeCache.getDataFromTable(HomeCache.TABLES.DEAL);
//        final int countCacheData = dealCursor.getCount();
//        Log.d(TAG, "count hot Deal cache : " + countCacheData);
        final OwlCarousel3 owl = (OwlCarousel3) findViewById(R.id.new_wrapper);
        final NewArrivalsAdapter adapter = new NewArrivalsAdapter(mMasterActivity);

        WebService.getJson("best-sell/?for_app=1&get_version=1", new Net.JsonResult() {

            public void error(Exception e) {
            }

            @Override
            public void success(final Object res) {
                if (!(res instanceof JSONObject)) return;
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        final JSONObject objVersion = (JSONObject) res;
                        WebService.getJson("best-sell/", new Net.JsonResult() {
                            public void error(Exception e) {
                            }

                            @Override
                            public void success(Object res) {
                                if (!(res instanceof JSONArray)) return;
                                JSONArray list = (JSONArray) res;
                                if (list != null) {

                                    Type array_listing_product_type = new TypeToken<ArrayList<ListingProductModel>>() {
                                    }.getType();
                                    Gson gson = new Gson();
                                    final List<ListingProductModel> arrayDeal = gson.fromJson(list.toString(), array_listing_product_type);
                                    adapter.addProductList(arrayDeal);
                                    owl.notifyDataSetChanged();
                                }
                            }
                        });
                    }

                };
                if (fragment_created.get()) r.run();
                else addOnCreateObserver(r);
            }
        });
    }

    private void loadBestSellers(){
//        final Version ver = new Version(MasterActivity.getInstance().getApplicationContext());
//        final String dealVersion = ver.getVersion(HomeCache.TABLES.DEAL);
//        Log.d(TAG, "deal version cache: " + dealVersion);
//        final HomeCache homeCache = new HomeCache(MasterActivity.getInstance().getApplication());
//        final Cursor dealCursor = homeCache.getDataFromTable(HomeCache.TABLES.DEAL);
//        final int countCacheData = dealCursor.getCount();
//        Log.d(TAG, "count hot Deal cache : " + countCacheData);
        final OwlCarousel3 owl = (OwlCarousel3) findViewById(R.id.best_seller_wrapper);
        final BestSellerAdapter adapter = new BestSellerAdapter(mMasterActivity);

        WebService.getJson("best-sell/?for_app=1&get_version=1", new Net.JsonResult() {

            public void error(Exception e) {
            }

            @Override
            public void success(final Object res) {
                if (!(res instanceof JSONObject)) return;
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        final JSONObject objVersion = (JSONObject) res;
                        WebService.getJson("best-sell/", new Net.JsonResult() {
                            public void error(Exception e) {
                            }

                            @Override
                            public void success(Object res) {
                                if (!(res instanceof JSONArray)) return;
                                JSONArray list = (JSONArray) res;
                                if (list != null) {

                                    Type array_listing_product_type = new TypeToken<ArrayList<ListingProductModel>>() {
                                    }.getType();
                                    Gson gson = new Gson();
                                    final List<ListingProductModel> arrayDeal = gson.fromJson(list.toString(), array_listing_product_type);
                                    adapter.addProductList(arrayDeal);
                                    owl.notifyDataSetChanged();
                                }
                            }
                        });
                    }

                };
                if (fragment_created.get()) r.run();
                else addOnCreateObserver(r);
            }
        });
    }

    private class HomeActionBarCallBack extends FActionBar.FActionBarCallBackAdapter {
        @Override
        public void barClicked() {
//            super.barClicked();
//            setLeftNavDrawer(new FLeftNavigationDrawer(getContext()));
            FLeftNavigationDrawer fnd = getLeftNavDrawer();
            if (fnd != null){
                if (!fnd.isShowing())
                    fnd.show();
                else
                    fnd.hide();
            }
        }
    }
}
