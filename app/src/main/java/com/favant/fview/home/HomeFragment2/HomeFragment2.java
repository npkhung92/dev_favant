package com.favant.fview.home.HomeFragment2;

import android.util.Log;

import com.favant.R;
import com.favant.fview.common.FActionBar;
import com.favant.fview.common.FFragment;
import com.favant.fview.common.LeftNavigator.FLeftNavigationDrawer;
import com.favant.fview.common.OwlCarousel2.OwlCarousel2;
import com.favant.model.ListingProductModel;
import com.favant.util.Net;
import com.favant.util.WebService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hung on 11/28/2015.
 */
public class HomeFragment2 extends FFragment {
    private final static String TAG = "Favant_HomepageFragment";

    public HomeFragment2() {
        setLayout(R.layout.master_page, R.layout.home_page);
    }

    @Override
    protected void loadFActionBar(FActionBar ab) {
        ab.showBarButton();
        ab.showLocationButton();
        ab.showSearchButton();
        ab.showCartButton();
        ab.showLogo();
        ab.setCallback(new HomeActionBarCallBack());
    }

    @Override
    protected void onPreCreate() {

    }

    @Override
    protected void onCreate() {
        loadBanner();
        loadNewArrivals();
//        loadBestSellers();
    }

    private void loadBanner(){
        final OwlCarousel2 owl = (OwlCarousel2) findViewById(R.id.banner_wrapper);
        final BannerAdapter2 adapter = new BannerAdapter2(mMasterActivity);
        adapter.addExamBanner();
        owl.setAdapter(adapter);
//        WebService.getJson("homepage/home-banner-slider/?for_app=1&get_version=1", new Net.JsonResultAdapter() {
//            @Override
//            public void success(Object res) {
//                if (!(res instanceof JSONObject)) return;
//                final JSONObject objVersion = (JSONObject) res;
//                Runnable r = new Runnable() {
//                    @Override
//                    public void run() {
//                        WebService.getJson("homepage/home-banner-slider/", new Net.JsonResultAdapter() {
//                            @Override
//                            public void success(Object res) {
//                                if (!(res instanceof JSONArray)) return;
//                                JSONArray res2 = (JSONArray) res;
//
//                                try {
//                                    final String version = objVersion.getString("version");
//                                    if (version == null || true) {
////                                        final List<Map<String, String>> arrayBanner = new ArrayList<Map<String, String>>();
//                                        Log.d(TAG, "version banner  diff");
//                                        adapter.reset();
//                                        for (int i = 0; i < res2.length(); i++) {
//                                            final JSONObject obj = res2.optJSONObject(i);
//                                            final String img_url = obj.optString("image");
//                                            final String url = obj.optString("url");
//                                            if (owl != null) {
//                                                adapter.addBanner(img_url, url);
//                                                //add data to cache array
//                                                Map<String, String> banner = new HashMap<String, String>();
////                                                banner.put(HomeCache.BannerColumns.IMG_URL, img_url);
////                                                banner.put(HomeCache.BannerColumns.URL, url);
////                                                arrayBanner.add(banner);
//                                                //homeCache.saveBanner(imgUrl, imgPath);
//                                            }
//                                        }
//                                        owl.setAdapter(adapter);
////                                        if (mHomeFragment != null) mHomeFragment.hideLoading();
////                                        hidePullToRefresh();
//                                        // save cache after load
//
////                                        saveBannerToDb(arrayBanner, version, countCacheData);
//                                    } else {
////                                        hidePullToRefresh();
//                                    }
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        });
//                    }
//                };
//                if (fragment_created.get()) r.run();
//                else addOnCreateObserver(r);
//            }
//        });
    }

    private void loadNewArrivals() {
        final OwlCarousel2 owl = (OwlCarousel2) findViewById(R.id.new_wrapper);
        final BestSellerAdapter2 adapter = new BestSellerAdapter2(mMasterActivity);

        WebService.getJson("khuyen-mai/restful/get-list/?p=1&s=20", new Net.JsonResultAdapter() {
            @Override
            public void success(Object res) {
                Log.v(TAG, String.valueOf(res));
                if (res instanceof JSONObject) {
                    final JSONArray list = ((JSONObject) res).optJSONArray("product_list");
                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            if (list != null) {


                                Type array_listing_product_type = new TypeToken<ArrayList<ListingProductModel>>() {
                                }.getType();
                                Gson gson = new Gson();
                                final List<ListingProductModel> pro_model_list = gson.fromJson(list.toString(), array_listing_product_type);
                                adapter.addProductList(pro_model_list);
                                owl.setAdapter(adapter);
                                owl.notifyDataSetChanged();

                            }

                        }
                    };
                    if (fragment_created.get()) r.run();
                    else addOnCreateObserver(r);

                }
            }

            @Override
            public void error(Exception e) {
                super.error(e);
                Log.e(TAG, String.valueOf(e));
            }
        });
    }


    private class HomeActionBarCallBack extends FActionBar.FActionBarCallBackAdapter {
        @Override
        public void barClicked() {
//            super.barClicked();
//            setLeftNavDrawer(new FLeftNavigationDrawer(getContext()));
            FLeftNavigationDrawer fnd = getLeftNavDrawer();
            if (fnd != null){
                if (!fnd.isShowing())
                    fnd.show();
                else
                    fnd.hide();
            }
        }
    }
}
