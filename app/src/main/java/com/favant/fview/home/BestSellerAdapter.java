package com.favant.fview.home;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.favant.MasterActivity;
import com.favant.R;
import com.favant.fview.common.FImageView;
import com.favant.fview.common.FTextView;
import com.favant.fview.common.ListingProductBox;
import com.favant.model.ListingProductModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hung on 11/1/2015.
 */
public class BestSellerAdapter extends RecyclerView.Adapter<BestSellerAdapter.ViewHolder> {

    private static final String TAG = "BestSellerAdapter";
    private final MasterActivity mMasterActivity;
    private List<ListingProductModel> product_list = new ArrayList<>();

    public BestSellerAdapter(MasterActivity ma) {
        mMasterActivity = ma;
    }
    @Override
    public BestSellerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listing_product_box, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(BestSellerAdapter.ViewHolder holder, int position) {
        ListingProductModel lpm = product_list.get(position);
        holder.product_name.setText(lpm.getName());
        holder.product_image.setImageUrl(lpm.getImgUrl());
        holder.pro_price.setText(String.valueOf(lpm.getOriginalPrice()));
        holder.pro_final_price.setText(String.valueOf(lpm.getFinalPrice()));
    }

    @Override
    public int getItemCount() {
        return product_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public FImageView product_image;
        public FTextView product_name;
        public FTextView pro_final_price;
        public FTextView pro_price;
        public ViewHolder(View itemView) {
            super(itemView);
            product_image = (FImageView) itemView.findViewById(R.id.pro_image);
            product_name = (FTextView) itemView.findViewById(R.id.pro_name);
            pro_final_price = (FTextView) itemView.findViewById(R.id.pro_final_price);
            pro_price = (FTextView) itemView.findViewById(R.id.pro_price);
        }
    }

    public void addProduct(ListingProductModel pm) {
        product_list.add(pm);
        notifyDataSetChanged();
    }
    public void addProductList(List<ListingProductModel> product_list) {
        this.product_list.addAll(product_list);
        notifyDataSetChanged();
    }

    public void reset() {
        product_list.clear();
        notifyDataSetChanged();
    }
}
