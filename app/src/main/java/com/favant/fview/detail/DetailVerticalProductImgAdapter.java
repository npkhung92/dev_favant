package com.favant.fview.detail;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.favant.MasterActivity;
import com.favant.R;
import com.favant.fview.common.FImageView;

import java.util.ArrayList;

/**
 * Created by Hung on 12/5/2015.
 */
public class DetailVerticalProductImgAdapter extends BaseAdapter {
    private static final String TAG = "DetailVerticalProductImgAdapter";
    private final MasterActivity mMasterActivity;
    private ArrayList<ProductImage> images = new ArrayList<ProductImage>();
    private ArrayList<Integer> img_id = new ArrayList<Integer>(4);

    public DetailVerticalProductImgAdapter(MasterActivity ma) {
        mMasterActivity = ma;
    }

    @Override
    public int getCount() {
        return img_id.size();
    }

    @Override
    public Object getItem(int i) {
        return img_id.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Log.v(TAG, "index " + i);
        FImageView img;
        if (view == null) {
            img = new FImageView(mMasterActivity);
            img.setRatioW(1);
            img.setRatioH(1);
            img.setAdjustViewBounds(true);
            view = img;
        } else {
            img = (FImageView) view;
        }
        img.setImageResource(img_id.get(i));
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        return view;
    }

    public void addExamImage(){
        img_id.add(R.drawable.pro_img);
        img_id.add(R.drawable.pro_img);
        img_id.add(R.drawable.pro_img);
        img_id.add(R.drawable.pro_img);
        notifyDataSetChanged();
    }

    public void addBanner(String imgUrl, String url) {
        ProductImage b = new ProductImage(imgUrl, url);
        images.add(b);

        notifyDataSetChanged();
    }

    public void reset() {
        images.clear();
        img_id.clear();
        notifyDataSetChanged();
    }

    private static class ProductImage {
        public String imgUrl;
        public String url;

        public ProductImage(String iu, String u) {
            imgUrl = iu;
            url = u;
        }
    }
}
