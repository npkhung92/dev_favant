package com.favant.fview.detail;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.favant.MasterActivity;
import com.favant.R;
import com.favant.fview.common.FImageView;
import com.favant.fview.common.VerticalOwlCarousel.VerticalOwlCarousel;

import java.util.ArrayList;

/**
 * Created by Hung on 12/5/2015.
 */
public class DetailProductImageAdapter extends BaseAdapter {
    private static final String TAG = "DetailProductImageAdapter";
    private final MasterActivity mMasterActivity;
    private int itemHeight;
    private ArrayList<ProductImage> images = new ArrayList<ProductImage>();
    private ArrayList<Integer> img_id = new ArrayList<Integer>(4);
    private ArrayList<VerticalOwlCarousel> voc_list = new ArrayList<>(4);

    public DetailProductImageAdapter(MasterActivity ma) {
        mMasterActivity = ma;
    }

    @Override
    public int getCount() {
        return voc_list.size();
    }

    @Override
    public Object getItem(int i) {
        return voc_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Log.v(TAG, "index " + i);
        VerticalOwlCarousel img;
        if (view == null) {
//            img = new FImageView(mMasterActivity);
//            img.setRatioW(1);
//            img.setRatioH(1);
//            img.setAdjustViewBounds(true);
//            view = img;
            img = new VerticalOwlCarousel(mMasterActivity);
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            img.setLayoutParams(params);
            img.showDots();
            view = img;
//            view = LayoutInflater.from(mMasterActivity).inflate(R.layout.detail_main_img_component, null);

        } else {
            img = (VerticalOwlCarousel) view;
        }
//        img.setImageResource(img_id.get(i));
        DetailVerticalProductImgAdapter img_adapter = new DetailVerticalProductImgAdapter(mMasterActivity);
        img_adapter.addExamImage();
        img.setAdapter(img_adapter);
        itemHeight = img.getItemHeight();
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        return view;
    }

    public int getItemHeight() {
        return itemHeight;
    }

    public void addExamVOC(){
        VerticalOwlCarousel voc = new VerticalOwlCarousel(mMasterActivity);
        voc_list.add(voc);
        voc_list.add(voc);
        voc_list.add(voc);
        voc_list.add(voc);
//        img_id.add(R.drawable.pro_img);
//        img_id.add(R.drawable.pro_img);
//        img_id.add(R.drawable.pro_img);
//        img_id.add(R.drawable.pro_img);
        notifyDataSetChanged();
    }

    public void addBanner(String imgUrl, String url) {
        ProductImage b = new ProductImage(imgUrl, url);
        images.add(b);

        notifyDataSetChanged();
    }

    public void reset() {
        voc_list.clear();
        images.clear();
        img_id.clear();
        notifyDataSetChanged();
    }

    private static class ProductImage {
        public String imgUrl;
        public String url;

        public ProductImage(String iu, String u) {
            imgUrl = iu;
            url = u;
        }
    }
}
