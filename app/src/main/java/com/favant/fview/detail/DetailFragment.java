package com.favant.fview.detail;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.favant.R;
import com.favant.fview.common.FActionBar;
import com.favant.fview.common.FColorBox;
import com.favant.fview.common.FFragment;
import com.favant.fview.common.FPopup;
import com.favant.fview.common.OwlCarousel2.OwlCarousel2;
import com.favant.fview.common.VerticalOwlCarousel.VerticalOwlCarousel;
import com.favant.model.ProductDetailModel;
import com.favant.util.AnimationListenerAdapter;
import com.favant.util.Net;
import com.favant.util.Screen;
import com.favant.util.WebService;
import com.wefika.flowlayout.FlowLayout;

import org.json.JSONException;
import org.json.JSONObject;

import it.sephiroth.android.library.widget.HListView;

/**
 * Created by Bear on 9/26/2015.
 */
public class DetailFragment extends FFragment {

    private static final int VIEW_MORE_STATE_SHOW = 1;
    private static final int VIEW_MORE_STATE_HIDE = 2;
    private static final int VIEW_MORE_STATE_ANIMATING = 3;
    private final int ANIMATE_TIME = 300;
    private static boolean detailInfoIsShowing = false;
    private static boolean detailSizeIsShowing = false;
    private static boolean detailRelatedItemIsShowing = false;
    private static boolean detailMaterialIsShowing = false;
    private int view_more_state = VIEW_MORE_STATE_HIDE;
    private Runnable mbackClickOverride = null;
    private ProductDetailModel product_model = null;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private int product_id = 0;
    private String url;
    private DetailRelatedItemAdapter mRelatedAdapter;
    private LinearLayout detail_info;
    private LinearLayout detail_size;
    private LinearLayout detail_option_bar;
    private LinearLayout detail_related_items;
    private LinearLayout detail_material;
    private View material_btn;
    private Button add_bag_btn;
    private LinearLayout info_btn;
    private RelativeLayout detail_size_btn;
    private RelativeLayout related_items_btn;
    private HListView color_wrapper;
    private View goto_store_popup;
    private View rating_wrapper;
    private FlowLayout material_wrapper;
    private OwlCarousel2 product_img;
    private DetailProductImageAdapter img_adapter;
    private VerticalOwlCarousel product_img2;
    private DetailVerticalProductImgAdapter img_adapter2;

    public DetailFragment() {
        setLayout(R.layout.master_page, R.layout.detail);
    }

    private boolean parseArguments() {
        Bundle bundle = getArguments();
        if (bundle == null) return false;
        product_id = bundle.getInt("id", 0);
        if (product_id == 0) return false;
        url = bundle.getString("url", "");
        return true;
    }

    @Override
    protected synchronized void onPreCreate() {
//        if (!parseArguments()) {
//            mMasterActivity.goBack();
//            return;
//        }
//        loadProductData();
    }

    @Override
    protected void loadFActionBar(FActionBar ab) {
        ab.showBackButton();
        ab.showCartButton();
        ab.showSearchButton();
        ab.showMoreButton();
        ab.setCallback(new DetailActionBarCallback());
    }

    @Override
    protected void onCreate() {
        updateUI();
        bindViewControl();
//        addRelatedItems();
        initData();
        addViewMore();
    }

    private LayoutInflater getInflater() {
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return li;
    }

    private void updateUI() {
        product_img = (OwlCarousel2) findViewById(R.id.product_img);
        img_adapter = new DetailProductImageAdapter(mMasterActivity);
        detail_option_bar = (LinearLayout) findViewById(R.id.detail_option_bar);
        detail_info = (LinearLayout) findViewById(R.id.detail_info);
        detail_related_items = (LinearLayout) findViewById(R.id.detail_related_item);
        detail_size = (LinearLayout) findViewById(R.id.detail_size);
        detail_material = (LinearLayout) findViewById(R.id.detail_material);
        add_bag_btn = (Button) detail_option_bar.findViewById(R.id.add_bag_btn);
        detail_size_btn = (RelativeLayout) detail_option_bar.findViewById(R.id.detail_size_btn);
        info_btn = (LinearLayout) findViewById(R.id.info_btn);
        related_items_btn = (RelativeLayout) detail_option_bar.findViewById(R.id.related_items_btn);
        goto_store_popup = detail_info.findViewById(R.id.goto_store_popup);
        rating_wrapper = detail_info.findViewById(R.id.rating_wrapper);

        material_btn = findViewById(R.id.material_btn);
        material_wrapper = (FlowLayout) detail_material.findViewById(R.id.material_wrapper);
        View mat_example = getInflater().inflate(R.layout.material_box, null);
        material_wrapper.addView(mat_example);

        color_wrapper = (HListView) findViewById(R.id.color_wrapper);
        ColorAdapter2 ca = new ColorAdapter2(mMasterActivity);
        for (int i = 0; i < 7; i++) {
            FColorBox fc = new FColorBox(getContext());
            ca.addColor(fc);
        }
        color_wrapper.setAdapter(ca);
        color_wrapper.setDividerWidth(20);

        img_adapter.addExamVOC();
//        img_adapter2.addExamImage();
        product_img.setAdapter(img_adapter);

    }

    private void initData() {
        WebService.getJson("service/product/view/?id=15675", new Net.JsonResult() {
            @Override
            public void success(final Object res) {
                final Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        product_model = new ProductDetailModel();
                        if (res instanceof JSONObject) {
                            JSONObject obj = (JSONObject) res;
                            product_model.id = obj.optInt("id");
                            product_model.name = obj.optString("name");
                            product_model.price = (float) obj.optDouble("price");
                            product_model.special_price = ((float) obj.optDouble("special_price"));
                            product_model.rating = obj.optInt("rating");
                        }
                    }
                };
                if (fragment_created.get()) r.run();
                else addOnCreateObserver(r);
            }

            @Override
            public void error(Exception e) {

            }
        });
    }

    private void bindViewControl() {


        detail_size_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!detailSizeIsShowing) {
//                    detail_size.setY(Screen.getHeight(getContext()));
                    detail_size.setVisibility(View.VISIBLE);
                    View size_blank_view = detail_size.findViewById(R.id.size_blank_view);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, detail_option_bar.getHeight() + color_wrapper.getHeight());
                    size_blank_view.setLayoutParams(params);
                    detailSizeIsShowing = true;
                    ValueAnimator anim = ObjectAnimator.ofFloat(detail_size, "translationY", Screen.getHeight(getContext()), 0);
                    anim.setDuration(ANIMATE_TIME);
                    anim.start();
//                    add_bag_btn.bringToFront();
                } else {

                    ValueAnimator anim = ObjectAnimator.ofFloat(detail_size, "translationY", 0, Screen.getHeight(getContext()));
                    anim.setDuration(ANIMATE_TIME);
                    anim.start();
                    anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            detail_size.setVisibility(View.GONE);
                            detailSizeIsShowing = false;
                        }
                    });
                }
            }
        });
        info_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!detailInfoIsShowing) {
                    detail_info.setVisibility(View.VISIBLE);
                    View info_blank_view = detail_info.findViewById(R.id.info_blank_view);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, detail_option_bar.getHeight() + color_wrapper.getHeight());
                    info_blank_view.setLayoutParams(params);
                    ValueAnimator anim = ObjectAnimator.ofFloat(detail_info, "translationY", Screen.getHeight(getContext()), 0);
                    anim.setDuration(ANIMATE_TIME);
                    anim.start();
                    detailInfoIsShowing = true;
                } else {
                    ValueAnimator anim = ObjectAnimator.ofFloat(detail_info, "translationY", 0, Screen.getHeight(getContext()));
                    anim.setDuration(ANIMATE_TIME);
                    anim.start();
                    anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            detail_info.setVisibility(View.GONE);
                            detailInfoIsShowing = false;
                        }
                    });
                }
            }
        });
//        related_items_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!detailRelatedItemIsShowing) {
//                    detail_related_items.setVisibility(View.VISIBLE);
//                    ValueAnimator anim = ObjectAnimator.ofFloat(detail_related_items, "translationY", Screen.getHeight(getContext()), 0);
//                    anim.setDuration(ANIMATE_TIME);
//                    anim.start();
//                    detailRelatedItemIsShowing = true;
//                } else {
//                    ValueAnimator anim = ObjectAnimator.ofFloat(detail_related_items, "translationY", 0, Screen.getHeight(getContext()));
//                    anim.setDuration(ANIMATE_TIME);
//                    anim.start();
//                    anim.addListener(new AnimatorListenerAdapter() {
//                        @Override
//                        public void onAnimationEnd(Animator animation) {
//                            detail_related_items.setVisibility(View.GONE);
//                            detailRelatedItemIsShowing = false;
//                        }
//                    });
//                }
//            }
//        });

        material_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                View material_blank_view = detail_material.findViewById(R.id.material_blank_view);
                if (!detailMaterialIsShowing) {
                    detail_material.setVisibility(View.VISIBLE);
                    View material_blank_view = detail_material.findViewById(R.id.material_blank_view);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, findViewById(R.id.detail_header).getHeight());
                    material_blank_view.setLayoutParams(params);
                    ValueAnimator anim = ObjectAnimator.ofFloat(detail_material, "translationY", - Screen.getHeight(getContext()), 0);
                    anim.setDuration(ANIMATE_TIME);
                    anim.start();
                    detailMaterialIsShowing = true;
                } else {
                    ValueAnimator anim = ObjectAnimator.ofFloat(detail_material, "translationY", 0, - Screen.getHeight(getContext()));
                    anim.setDuration(ANIMATE_TIME);
                    anim.start();
                    anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            detail_material.setVisibility(View.GONE);
                            detailMaterialIsShowing = false;
                        }
                    });
                }
            }
        });

        goto_store_popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View popup_content = getInflater().inflate(R.layout.detail_find_store, null);
                setPopupContent(popup_content);
                setPopupTitle("Find Store");
                if (!isPopupShown())
                    showPopup();
            }
        });

        rating_wrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View popup_content = getInflater().inflate(R.layout.detail_rating, null);
                setPopupContent(popup_content);
                setPopupTitle("Reviews");
                if (!isPopupShown())
                    showPopup();
            }
        });
    }

    private void addRelatedItems() {
        mRecyclerView = (RecyclerView) findViewById(R.id.related_items);
        mRecyclerView.setHasFixedSize(true);
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRelatedAdapter = new DetailRelatedItemAdapter(mMasterActivity, getContext());
        mRecyclerView.setAdapter(mRelatedAdapter);
    }

    // VIEW MORE
    private void addViewMore() {
        findViewById(R.id.view_more_overlay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideViewMore();
            }
        });

        findViewById(R.id.back_to_homepage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle data = new Bundle();
                data.putString("page_type", "first");
                router.gotoHomepage(data);
            }
        });

//        final View share_content = mInflater.inflate(R.layout.share, null);
//
//        share_content.findViewById(R.id.share_facebook).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (FacebookDialog.canPresentShareDialog(getContext(),
//                        FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
//                    FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(getActivity())
//                            .setLink(WebService.domain + product_model.cat_path)
//                            .build();
//                    UiLifecycleHelper fb_ui_helper = mMasterActivity.getFbUIHelper();
//                    fb_ui_helper.trackPendingDialogCall(shareDialog.present());
//                }
//                hidePopup();
//                hideViewMore();
//            }
//        });
//
//        share_content.findViewById(R.id.share_google).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent shareIntent = new PlusShare.Builder(getContext())
//                        .setType("text/plain")
//                        .setText("Cùng mua sắm nào!!!")
//                        .setContentUrl(Uri.parse(WebService.domain + product_model.cat_path))
//                        .getIntent();
//
//                startActivityForResult(shareIntent, 0);
//                hidePopup();
//                hideViewMore();
//            }
//        });

        findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                SPopup popup = getPopup();
//                if (popup != null) {
//                    popup.setTitle("Chia sẻ");
//                    popup.setContent(share_content);
//                    popup.show();
//                }
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, String.valueOf(Uri.parse(WebService.domain + product_model.cat_path)));
//                Log.v(TAG, String.valueOf(Uri.parse(WebService.domain + product_model.cat_path)));
                mMasterActivity.startActivity(Intent.createChooser(sendIntent, "Share with ..."));
            }
        });
    }

    private void showViewMore() {
        if (view_more_state != VIEW_MORE_STATE_HIDE) return;
        view_more_state = VIEW_MORE_STATE_ANIMATING;
        final View view_more = findViewById(R.id.view_more_wrapper);
        view_more.setVisibility(View.VISIBLE);

        Animation fadeIn = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
        view_more.startAnimation(fadeIn);

        fadeIn.setAnimationListener(new AnimationListenerAdapter() {
            @Override
            public void onAnimationEnd(Animation animation) {
                view_more.clearAnimation();
                view_more_state = VIEW_MORE_STATE_SHOW;
            }
        });
    }

    private void hideViewMore() {
        if (view_more_state != VIEW_MORE_STATE_SHOW) return;
        view_more_state = VIEW_MORE_STATE_ANIMATING;
        final View view_more = findViewById(R.id.view_more_wrapper);
        view_more.setVisibility(View.VISIBLE);

        Animation fadeOut = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
        fadeOut.setAnimationListener(new AnimationListenerAdapter() {
            @Override
            public void onAnimationEnd(Animation animation) {
                view_more.setVisibility(View.GONE);
                view_more.clearAnimation();
                view_more_state = VIEW_MORE_STATE_HIDE;
            }
        });
        view_more.startAnimation(fadeOut);
    }

    private void toggleViewMore() {
        if (view_more_state == VIEW_MORE_STATE_SHOW) hideViewMore();
        else showViewMore();
    }

    public void overrideBackClick(Runnable r) {
        mbackClickOverride = r;
    }

    //Popup
    public void showPopup() {
        FPopup p = getPopup();
        if (p != null) p.show();
    }

    public void hidePopup() {
        FPopup p = getPopup();
        if (p != null) p.hide();
    }

    public boolean isPopupShown() {
        FPopup p = getPopup();
        if (p != null) return p.isShown();
        return false;
    }

    public void setPopupContent(View v) {
        FPopup p = getPopup();
        if (p != null) p.setContent(v);
    }

    public void setPopupTitle(String text) {
        FPopup p = getPopup();
        if (p != null) p.setTitle(text);
    }

    public void addPopupListioner(FPopup.PopupListener listener) {
        FPopup popup = getPopup();
        if (popup != null) {
            popup.addPopupListener(listener);
        }
    }

    public void removePopupListioner(FPopup.PopupListener listener) {
        FPopup popup = getPopup();
        if (popup != null) {
            popup.removePopupListener(listener);
        }
    }

    @Override
    public boolean handleBackPressed() {
        if (!super.handleBackPressed()) {
            if (mbackClickOverride != null) {
                mbackClickOverride.run();
                return true;
            }
            return false;
        }
        return true;
    }

    private class DetailActionBarCallback extends FActionBar.FActionBarCallBackAdapter {
        @Override
        public void moreClicked() {
            toggleViewMore();
        }

        @Override
        public void cartClicked() {
            loadCartPopup();
            showCartPopup();
        }
    }
}
