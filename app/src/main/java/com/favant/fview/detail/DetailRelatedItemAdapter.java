package com.favant.fview.detail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.favant.MasterActivity;
import com.favant.R;
import com.favant.fview.common.FImageView;
import com.favant.fview.common.FTextView;
import com.favant.fview.common.ListingProductBox;

import java.util.ArrayList;

/**
 * Created by Hung on 10/19/2015.
 */
public class DetailRelatedItemAdapter extends RecyclerView.Adapter<DetailRelatedItemAdapter.ViewHolder> {

    private MasterActivity mMasterActivity;
    private Context context;
    private ArrayList<ListingProductBox> products = new ArrayList<ListingProductBox>();

    public DetailRelatedItemAdapter(MasterActivity ma, Context c) {
        this.mMasterActivity = ma;
        this.context = c;
    }

    public void addProduct(ListingProductBox lpb){
        products.add(lpb);
    }

    public void clearProduct(){
        products.clear();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public FTextView pro_final_price;
        public FImageView pro_image;
        public FTextView pro_name;
        public FTextView pro_price;
        public ViewHolder(View itemView) {
            super(itemView);
            pro_final_price = (FTextView) itemView.findViewById(R.id.pro_final_price);
            pro_image = (FImageView) itemView.findViewById(R.id.pro_image);
            pro_name = (FTextView) itemView.findViewById(R.id.pro_name);
            pro_price = (FTextView) itemView.findViewById(R.id.pro_price);
        }
    }
    @Override
    public DetailRelatedItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (parent == null) return null;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listing_product_box, parent, false);
        if (v == null) return null;
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(DetailRelatedItemAdapter.ViewHolder holder, int position) {
        holder.pro_image = products.get(position).getProduct_image();
        holder.pro_final_price = products.get(position).getPro_final_price();
        holder.pro_price = products.get(position).getPro_price();
        holder.pro_name = products.get(position).getProduct_name();
    }

    @Override
    public int getItemCount() {
        return products.size();
    }
}
