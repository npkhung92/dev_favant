package com.favant.fview.detail;

import com.favant.R;
import com.favant.fview.common.FActionBar;
import com.favant.fview.common.FFragment;

/**
 * Created by Hung on 1/10/2016.
 */
public class WritingReviewFragment extends FFragment {
    

    public WritingReviewFragment() {
        setLayout(master_page, R.layout.detail_write_review);
    }

    @Override
    protected void loadFActionBar(FActionBar ab) {

    }

    @Override
    protected void onPreCreate() {

    }

    @Override
    protected void onCreate() {

    }
}
