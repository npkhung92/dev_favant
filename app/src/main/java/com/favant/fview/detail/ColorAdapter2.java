package com.favant.fview.detail;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.favant.MasterActivity;
import com.favant.R;
import com.favant.fview.common.FColorBox;
import com.favant.fview.common.FImageView;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Hung on 12/11/2015.
 */
public class ColorAdapter2 extends BaseAdapter {
    private static final String TAG = "ColorAdapter";
    private final MasterActivity mMasterActivity;
    private ArrayList<FColorBox> color_list = new ArrayList<FColorBox>();
    private LinkedHashMap<Integer, ViewHolder> color_map = new LinkedHashMap<>();

    public ColorAdapter2(MasterActivity masterActivity) {
        this.mMasterActivity = masterActivity;
    }
    public void addColor(FColorBox cl){
        color_list.add(cl);
    }

    @Override
    public int getCount() {
        Log.v(TAG, String.valueOf(color_list.size()));
        return color_list.size();
    }

    @Override
    public Object getItem(int position) {
        return color_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder vh = new ViewHolder();
        if (convertView == null) {
            convertView = LayoutInflater.from(mMasterActivity).inflate(R.layout.color_box, parent, false);
            vh.color = (LinearLayout) convertView.findViewById(R.id.color);
            vh.color_bg = convertView.findViewById(R.id.color_bg);
            vh.color_image = (FImageView) convertView.findViewById(R.id.color_image);
            convertView.setTag(vh);
        }
        FColorBox fColorBox = (FColorBox) getItem(position);
        if (convertView != null && convertView.getTag() instanceof ViewHolder){
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            color_map.put(position, viewHolder);
            viewHolder.color_image.setImageResource(R.drawable.color_example);
            viewHolder.color_image.setAdjustViewBounds(true);
            viewHolder.color_bg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (Map.Entry<Integer, ViewHolder> e : color_map.entrySet()){
                        e.getValue().color_bg.setSelected(false);
                    }
                    color_map.get(position).color_bg.setSelected(true);
                    notifyDataSetChanged();
                }
            });
        }
        return convertView;
    }

    public class ViewHolder {

        public View color_bg;
        public LinearLayout color;
        public FImageView color_image;
    }
}
