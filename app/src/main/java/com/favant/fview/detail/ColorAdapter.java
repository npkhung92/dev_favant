package com.favant.fview.detail;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.favant.R;
import com.favant.fview.common.FColorBox;
import com.favant.fview.common.FImageView;
import com.favant.model.ProductDetailModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Hung on 11/12/2015.
 */
public class ColorAdapter extends RecyclerView.Adapter<ColorAdapter.ViewHolder> {
    private final static String TAG = "ColorAdapter";
    private ArrayList<FColorBox> color_list = new ArrayList<FColorBox>();
    private LinkedHashMap<Integer, View> color_map = new LinkedHashMap<Integer, View>();
    private static final boolean colorInActive = false;
    private int o = 0;

    public void addColor(FColorBox cl){
        color_list.add(cl);
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {

        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.color_box, parent, false);
        ViewHolder vh = new ViewHolder(v, new ViewHolder.VHClick() {
            @Override
            public void onChangeColor(View caller) {
                for (Map.Entry<Integer, View> e : color_map.entrySet()){
                    e.getValue().findViewById(R.id.color_bg).setSelected(false);
                }
                v.findViewById(R.id.color_bg).setSelected(true);
//                notifyDataSetChanged();
            }
        });
        color_map.put(parent.indexOfChild(v),v);
//        v.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int itemPosition = parent.indexOfChild(v);
//                setCurrentActiveColor(itemPosition);
//                color_map.put(itemPosition, v);
//            }
//        });
        o++;
        Log.v(TAG, String.valueOf(o));
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        ProductDetailModel.AttributeOption ao = new ProductDetailModel.AttributeOption();
//        ao.color_hex_rgb = "#e5101d";
//        final View fc = color_list.get(position);
//        color_list.get(position).setClickable(true);
//        if (position == 0)
//            holder.color_bg.setSelected(true);
//        holder.color_bg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setCurrentActiveColor(position);
//                Log.v(TAG, String.valueOf(position));
//            }
//        });
        holder.color_image.setImageResource(R.drawable.color_example);
        holder.color_image.setAdjustViewBounds(true);
//        Log.v(TAG, "hm color " + color_map);
    }

    @Override
    public int getItemCount() {
        return color_list.size();
    }

    private void setCurrentActiveColor(int pos){
        for (Map.Entry<Integer, View> e : color_map.entrySet()){
            e.getValue().findViewById(R.id.color_bg).setSelected(false);
        }
        color_map.get(pos).findViewById(R.id.color_bg).setSelected(true);
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public View color_bg;
        public LinearLayout color;
        public FImageView color_image;
        public VHClick listener;

        public ViewHolder(View itemView, VHClick listener) {
            super(itemView);
            this.listener = listener;
            color_bg = itemView.findViewById(R.id.color_bg);
            color = (LinearLayout) itemView.findViewById(R.id.color);
            color_image = (FImageView) itemView.findViewById(R.id.color_image);
            color_bg.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onChangeColor(v);
        }

        public interface VHClick {
            public void onChangeColor(View caller);
        }
    }
}
