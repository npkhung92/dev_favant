package com.favant.fview.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

import com.favant.R;
import com.favant.util.MainHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bear on 8/22/2015.
 */
public class FEditText extends EditText {
    private ArrayList<EditTextImeBackListener> mOnImeBackListeners = new ArrayList<EditTextImeBackListener>();

    private boolean shouldHideKeyboardForMe = true;
    private boolean attached;
    private List<SEditorAction> mOnEditorActionListeners = new ArrayList<SEditorAction>();
    public FEditText(Context context) {
        super(context);
        init(context, null);
    }

    public FEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }
    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        attached = true;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        attached = false;
    }

    private void init(Context context, AttributeSet attrs) {
        // Read layout attributes
        if (!isInEditMode()) {
            String font_weight = "none";
            if (attrs != null) {
                TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.Favant, 0, 0);
                try {
                    font_weight = a.getString(R.styleable.Favant_fontWeight);
                } finally {
                    a.recycle();
                }
            }
//            if (font_weight == null) font_weight = "none";
//            if (font_weight.equals("none")) {
//                Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/FuturaStd_Book.ttf");
//                this.setTypeface(type);
//            } else if (font_weight.equals("bold")) {
//                Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/FuturaStd_Bold.ttf");
//                this.setTypeface(type);
//            } else if (font_weight.equals("italic")) {
//                Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/FuturaStd_BookOblique.ttf");
//                this.setTypeface(type);
//            }
        }
    }

    @Override
    public boolean onKeyPreIme(final int keyCode, final KeyEvent event) {
        boolean handled = false;
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
            if (mOnImeBackListeners != null) {
                ArrayList<EditTextImeBackListener> tmpMOnImeBack = new ArrayList<EditTextImeBackListener>();
                tmpMOnImeBack.addAll(mOnImeBackListeners);
                for (EditTextImeBackListener listener : tmpMOnImeBack) {
                    if (listener != null) {
                        boolean tmp_handled = listener.onImeBack();
                        if (tmp_handled) handled = true;
                    }
                }
            }
            clearFocus();
        }

        if (handled) {
            MainHandler.postDelay(new Runnable() {

                @Override
                public void run() {
//                    Log.v(TAG, "dispatch super key");
                    FEditText.super.onKeyPreIme(keyCode, event);
                }
            }, 500);
        }

        if (handled) return true;
        return super.onKeyPreIme(keyCode, event);
    }

    public void addOnEditTextImeBackListener(EditTextImeBackListener listener) {
        mOnImeBackListeners.add(listener);
    }

    public void removeOnEditTextImeBackListener(EditTextImeBackListener search_text_back_listener) {
//        Log.v(TAG, "remove " + search_text_back_listener + " " + mOnImeBackListeners.contains(search_text_back_listener));
        mOnImeBackListeners.remove(search_text_back_listener);
    }

    @Override
    public void onEditorAction(int actionCode) {
        super.onEditorAction(actionCode);

        for (SEditorAction listener : mOnEditorActionListeners) {
            listener.onEditorAction(actionCode);
        }
    }

    public boolean isShouldHideKeyboardForMe() {
        return shouldHideKeyboardForMe;
    }

    public void setShouldHideKeyboardForMe(boolean shouldHideKeyboardForMe) {
        this.shouldHideKeyboardForMe = shouldHideKeyboardForMe;
    }

    public void addOnEditorActionListener(SEditorAction listener) {
        mOnEditorActionListeners.add(listener);
    }

    public static interface EditTextImeBackListener {
        public boolean onImeBack();
    }

    public static interface SEditorAction {
        public void onEditorAction(int actionCode);
    }
}
