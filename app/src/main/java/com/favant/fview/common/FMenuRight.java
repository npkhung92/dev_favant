package com.favant.fview.common;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.favant.R;
import com.favant.util.MainHandler;
import com.favant.util.Screen;
import com.favant.util.ViewChecker;

import java.util.ArrayList;

/**
 * Created by Bear on 9/15/2015.
 */
public class FMenuRight extends RelativeLayout {
    private final int ANIMATION_TIME = 300;
    private boolean ANIMATING_STATE;

    private ArrayList<MenuRightListener> listeners = new ArrayList<MenuRightListener>();

    public FMenuRight(Context context) {
        super(context);
        init(context, null);
    }

    public FMenuRight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FMenuRight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {

    }
    public void addMenuRightListener(MenuRightListener mrl) {
        if (mrl != null) listeners.add(mrl);
    }
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (ViewChecker.validate(findViewById(R.id.menu_right_overlay), View.class)) {
            findViewById(R.id.menu_right_overlay).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    hide();
                }
            });
        }
    }
    public void setContent(View v) {
        if (ViewChecker.validate(findViewById(R.id.menu_right), ViewGroup.class)) {
            ViewGroup vg = (ViewGroup) findViewById(R.id.menu_right);
            vg.removeAllViews();
            vg.addView(v);
        }
    }

    public void removeContent() {
        if (ViewChecker.validate(findViewById(R.id.menu_right), ViewGroup.class)) {
            ViewGroup vg = (ViewGroup) findViewById(R.id.menu_right);
            vg.removeAllViews();
        }
    }

    public void show() {
        if (ANIMATING_STATE) return;
        ANIMATING_STATE = true;
        MainHandler.post(new Runnable() {
            @Override
            public void run() {
                setVisibility(View.VISIBLE);
                bringToFront();
                if (ViewChecker.validate(findViewById(R.id.menu_right), View.class)) {
                    View menu_right = findViewById(R.id.menu_right);
//                    menu_right.setX(Screen.getWidth(getContext()));
                    ValueAnimator anim = ObjectAnimator.ofFloat(menu_right, "scaleX", Screen.getWidth(getContext()), 0);
                    anim.setDuration(ANIMATION_TIME);
                    anim.start();

                    anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            ANIMATING_STATE = false;
                            for (int i = 0; i < listeners.size(); i++) {
                                MenuRightListener mrl = listeners.get(i);
                                mrl.show();
                            }
                        }
                    });
                }
            }
        });
    }

    public void hide(final boolean reset, final boolean animation) {
        if (ANIMATING_STATE) return;
        ANIMATING_STATE = true;

        MainHandler.post(new Runnable() {
            @Override
            public void run() {
                if (ViewChecker.validate(findViewById(R.id.menu_right), View.class)) {
                    final View menu_right = findViewById(R.id.menu_right);
                    if (animation) {
                        final float current_x = menu_right.getX();
                        ValueAnimator anim = ObjectAnimator.ofFloat(menu_right, "scaleX", 0, Screen.getWidth(getContext()));
                        anim.setDuration(ANIMATION_TIME);
                        anim.start();

                        anim.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                setVisibility(View.GONE);
                                if (reset) removeContent();
                                ANIMATING_STATE = false;
                                for (int i = 0; i < listeners.size(); i++) {
                                    MenuRightListener mrl = listeners.get(i);
                                    mrl.hide();
                                }
                            }
                        });
                    } else {
                        setVisibility(View.INVISIBLE);
                        if (reset) removeContent();
                        ANIMATING_STATE = false;
                        for (int i = 0; i < listeners.size(); i++) {
                            MenuRightListener mrl = listeners.get(i);
                            mrl.hide();
                        }
                    }
                }
            }
        });
    }

    public boolean isShowing() {
        return getVisibility() == View.VISIBLE;
    }

    public void hide() {
        hide(true, true);
    }

    public void hideWithNoAnimation() {
        hide(true, false);
    }

    public static interface MenuRightListener {
        public void show();

        public void hide();
    }
}
