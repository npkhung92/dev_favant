package com.favant.fview.common.OwlCarousel2;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.favant.R;
import com.favant.fview.common.FImageView;
import com.favant.util.DpPx;
import com.favant.util.MainHandler;
import com.favant.util.Screen;
import com.favant.util.ViewChecker;

import java.util.concurrent.atomic.AtomicBoolean;

import it.sephiroth.android.library.widget.AbsHListView;

/**
 * Created by Hung on 11/28/2015.
 */
public class OwlCarousel2 extends RelativeLayout {
    private static final String TAG = "OwlCarousel2";
    public SingleScrollHListView mHListView;
    protected float nitem = 1;
    protected float space_between_item = 0;
    protected Drawable background = null;
    protected float padding_top = 0, padding_bottom = 0, padding_left = 0, padding_right = 0;
    protected int animate_time = 200;
    protected int autoload_ms = 0;
    protected int scroll_per_item = 1;
    private int item_count = 0;
    private boolean infinite_scroll = false;
    private boolean show_dot = false;
    private float dot_margin_bottom = 0;
    private Dots mDots;
    private OwlCarousel2Adapter mOwlCarousel2Adapter;
    private int autoscroll_ms;
    private AtomicBoolean view_is_visible = new AtomicBoolean();
    private boolean lock_scroll;

    public OwlCarousel2(Context context) {
        super(context);
        init(context, null);
    }

    public OwlCarousel2(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public OwlCarousel2(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (!isInEditMode()) {
            this.setHorizontalScrollBarEnabled(false);

            // Inflate inner_layout
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            inflater.inflate(R.layout.owl_carousel_2, this, true);

            if (ViewChecker.validate(findViewById(R.id.hlistview), SingleScrollHListView.class)) {
                mHListView = (SingleScrollHListView) findViewById(R.id.hlistview);
                mHListView.setSelector(new StateListDrawable());
                mOwlCarousel2Adapter = new OwlCarousel2Adapter();
                mHListView.setAdapter(mOwlCarousel2Adapter);
            }

            if (ViewChecker.validate(findViewById(R.id.dots), LinearLayout.class)
                    && ViewChecker.validate(findViewById(R.id.dots_wrapper), RelativeLayout.class)) {
                View dots_wrapper = findViewById(R.id.dots_wrapper);
                dot_margin_bottom = dots_wrapper.getPaddingBottom();
                mDots = new Dots((RelativeLayout) findViewById(R.id.dots_wrapper), (LinearLayout) findViewById(R.id.dots));
            }

            // Read inner_layout attributes
            if (attrs != null) {
                TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.Favant, 0, 0);

                try {
                    nitem = a.getFloat(R.styleable.Favant_nitem, nitem);
                    space_between_item = a.getDimension(R.styleable.Favant_space_between_item, space_between_item);
//                    background = a.getDrawable(R.styleable.Favant_background);
                    padding_top = a.getDimension(R.styleable.Favant_padding_top, padding_top);
                    padding_bottom = a.getDimension(R.styleable.Favant_padding_bottom, padding_bottom);
                    padding_left = a.getDimension(R.styleable.Favant_padding_left, padding_left);
                    padding_right = a.getDimension(R.styleable.Favant_padding_right, padding_right);

                    animate_time = a.getInteger(R.styleable.Favant_animate_time, animate_time);
                    if (mHListView != null) mHListView.setScrollTime(animate_time);

                    scroll_per_item = a.getInteger(R.styleable.Favant_scroll_per_item, scroll_per_item);
                    if (mHListView != null) mHListView.setScrollPerItem(scroll_per_item);

                    infinite_scroll = a.getBoolean(R.styleable.Favant_infinite_scroll, infinite_scroll);
                    show_dot = a.getBoolean(R.styleable.Favant_show_dot, show_dot);
                    dot_margin_bottom = a.getDimension(R.styleable.Favant_dot_margin_bottom, dot_margin_bottom);
                    autoscroll_ms = a.getInt(R.styleable.Favant_autoscroll_ms, autoscroll_ms);
                    if (show_dot && Math.floor(nitem) != nitem) {
                        show_dot = false;
                    }
                    autoload_ms = a.getInt(R.styleable.Favant_autoscroll_ms, autoload_ms);
                } finally {
                    a.recycle();
                }

                if (mDots != null && show_dot) {
                    mDots.setMarginBottom(dot_margin_bottom);
                }
                if (autoscroll_ms > 0) {
                    view_is_visible.set(true);
                    scroll(true);
                }
            }
        }
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (visibility == View.VISIBLE) {
            if (autoscroll_ms > 0) startAutoscroll();
            Log.v(TAG, "show loading");
        } else {
            if (autoscroll_ms > 0) stopAutoscroll();
            Log.v(TAG, "hide loading");
        }
    }

    private void stopAutoscroll() {
        view_is_visible.set(false);
        mHListView.smoothScrollToPositionFromLeft(mHListView.getCurrentItem(), 0);
    }

    private void startAutoscroll() {
        if (!view_is_visible.get()) {
            view_is_visible.set(true);
            scroll(true);
        }
    }

    private void scroll(boolean this_is_first_call) {
        if (!view_is_visible.get()) return;

        if (!this_is_first_call) {
            mHListView.smoothScrollToPositionFromLeft(mHListView.getCurrentItem() + scroll_per_item, 0, mHListView.scroll_time);
        }

        MainHandler.postDelay(new Runnable() {
            @Override
            public void run() {
                scroll(false);
            }
        }, autoscroll_ms);
    }

    /**
     * get calculated width of each item
     *
     * @return
     */
    private int getItemWidth() {
        int screen_width = Screen.getWidth(getContext());
        int w = (int) ((screen_width - space_between_item * (nitem - 1) - padding_left - padding_right) / nitem);
        return w;
    }

    public void setAdapter(BaseAdapter adapter) {
        if (mOwlCarousel2Adapter != null) {
            mOwlCarousel2Adapter.setCoreAdapter(adapter);
            mOwlCarousel2Adapter.notifyDataSetChanged();

            if (infinite_scroll) mHListView.setSelection(adapter.getCount() * 5000);

            if (show_dot) {
                mDots.reset();
                for (int i = 0; i < adapter.getCount(); i++) {
                    mDots.addDot();
                }
                if (adapter.getCount() > 1) {
                    mDots.show();
                    if (!lock_scroll) mHListView.unlockScroll();
                } else mHListView.lockScroll();

                mHListView.setOnScrollListener(new AbsHListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsHListView view, int scrollState) {
                    }

                    @Override
                    public void onScroll(AbsHListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        if (mHListView.getChildCount() > 0) {
                            int item_count = mOwlCarousel2Adapter.getCoreAdapter().getCount();
                            float last_visible = mHListView.getLastVisiblePosition() % item_count;
                            float first_visible = mHListView.getFirstVisiblePosition() % item_count;
                            float left = mHListView.getChildAt(0).getLeft();
                            float width = mHListView.getChildAt(0).getWidth();

                            mDots.gotoChildonHand(first_visible - left / width);
                        }
                    }
                });
            }
        }
    }

    public void notifyDataSetChanged() {
        setAdapter(mOwlCarousel2Adapter.core_adapter);
    }

    public int getCurrentItem() {
        return mHListView.getCurrentItem();
    }

    public void setSelection(int position) {
        if (mHListView != null && mOwlCarousel2Adapter != null && mOwlCarousel2Adapter.core_adapter != null) {
            if (infinite_scroll)
                mHListView.setSelection(mOwlCarousel2Adapter.core_adapter.getCount() * 10000 + position);
            else mHListView.setSelection(position);
        }
    }

    public void lockScroll() {
        lock_scroll = true;
        mHListView.lockScroll();
    }

    public void unlockScroll() {
        lock_scroll = false;
        mHListView.unlockScroll();
    }

    private class OwlCarousel2Adapter extends BaseAdapter {

        private BaseAdapter core_adapter;

        @Override
        public int getCount() {
            if (core_adapter == null) return 0;
            if (infinite_scroll) {
                return core_adapter.getCount() * 10000;
            }
            return core_adapter.getCount();
        }

        @Override
        public Object getItem(int i) {
            if (core_adapter.getCount() == 0) return null;
            if (core_adapter == null) return null;
            Log.v(TAG, "get item: " + i);
            return core_adapter.getItem(i % core_adapter.getCount());
        }

        @Override
        public long getItemId(int i) {
            if (core_adapter.getCount() == 0) return 0;
            if (core_adapter == null) return 0;
            return core_adapter.getItemId(i % core_adapter.getCount());
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (core_adapter.getCount() == 0) return null;
            boolean view_was_null = view == null;
            if (core_adapter == null) return null;
            view = core_adapter.getView(i % core_adapter.getCount(), view, viewGroup);

            if (view_was_null) {
                ViewGroup.LayoutParams lp = view.getLayoutParams();
                if (lp == null) {
                    lp = new AbsHListView.LayoutParams(getItemWidth(), ViewGroup.LayoutParams.MATCH_PARENT);
                } else {
                    lp.width = getItemWidth();
                }
                view.setLayoutParams(lp);
            }

            return view;
        }

        public BaseAdapter getCoreAdapter() {
            return core_adapter;
        }

        public void setCoreAdapter(BaseAdapter adapter) {
            core_adapter = adapter;
        }

        @Override
        public void notifyDataSetChanged() {
            if (core_adapter != null) core_adapter.notifyDataSetChanged();
            super.notifyDataSetChanged();
        }
    }

    private class Dots {

        int current_position = 0;
        private RelativeLayout outer_layout = null;
        private LinearLayout inner_layout = null;
        private int margin_left = 0;
        private int width = 0;
        private Bitmap cached_grey_dot = null;
        private Bitmap cached_red_dot = null;
        private int dot_count = 0;
        private FImageView red_dot;

        public Dots(RelativeLayout ol, LinearLayout il) {
            outer_layout = ol;
            inner_layout = il;
            margin_left = (int) DpPx.dpToPx(getContext(), 4);
            width = (int) DpPx.dpToPx(getContext(), 10);
        }

        public void moveTo(int x) {
            if (outer_layout != null) {
                outer_layout.setX(x);
            }
        }

        public int getWidth() {
            if (outer_layout != null) return outer_layout.getWidth();
            return 0;
        }

        private Bitmap getGreyDotBitmap() {
            if (cached_grey_dot != null && !cached_grey_dot.isRecycled()) return cached_grey_dot;
            Bitmap b = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
            p.setStyle(Paint.Style.FILL);
            p.setColor(0xff8e8e8e);
            c.drawCircle(width / 2, width / 2, width / 2, p);
            cached_grey_dot = b;
            return b;
        }

        private Bitmap getRedDotBitmap() {
            if (cached_red_dot != null && !cached_red_dot.isRecycled()) return cached_red_dot;
            Bitmap b = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
            p.setStyle(Paint.Style.FILL);
            p.setColor(0xff000000);
            c.drawCircle(width / 2, width / 2, width / 2, p);
            cached_red_dot = b;
            return b;
        }

        public void addDot() {
            FImageView img = new FImageView(getContext());

            img.setImageBitmap(getGreyDotBitmap());

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, width);
            lp.setMargins(margin_left, 0, 0, 0);
            img.setLayoutParams(lp);
            if (inner_layout != null) inner_layout.addView(img);

            dot_count++;

            if (dot_count == 1) {
                red_dot = new FImageView(getContext());

                red_dot.setImageBitmap(getRedDotBitmap());

                LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(width, width);
                red_dot.setLayoutParams(lp);
                if (outer_layout != null) outer_layout.addView(red_dot);
                red_dot.setX(margin_left);
            }
        }

        private int getXFromPosition(int position) {
            return position * (width + margin_left) + margin_left;
        }

        public void gotoChild(int next_child) {
            if (dot_count == 0) return;
            while (next_child >= dot_count) next_child = next_child - dot_count;
            current_position = next_child;

            final ValueAnimator anim = ObjectAnimator.ofFloat(red_dot, "X", red_dot.getX(), getXFromPosition(current_position))
                    .setDuration(200);
            anim.start();
        }

        public void gotoChildonHand(float next_child) {
            if (dot_count == 0) return;
            while (next_child >= dot_count) next_child = next_child - dot_count;
            if (next_child + 1 > dot_count) {
                next_child = dot_count - next_child;
                next_child = (dot_count - 1) * next_child;
            }

            red_dot.setX(next_child * (width + margin_left) + margin_left);
        }

        public void setMarginBottom(float margin) {
            if (outer_layout != null)
                outer_layout.setPadding(outer_layout.getPaddingLeft(),
                        outer_layout.getPaddingTop(),
                        outer_layout.getPaddingRight(),
                        (int) margin);
        }

        public void show() {
            if (outer_layout != null) outer_layout.setVisibility(View.VISIBLE);
        }

        public void reset() {
            dot_count = 0;
            current_position = 0;
            outer_layout.removeView(red_dot);
            inner_layout.removeAllViews();
        }
    }
}
