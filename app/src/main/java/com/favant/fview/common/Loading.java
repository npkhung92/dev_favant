package com.favant.fview.common;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.favant.R;
import com.favant.util.DpPx;
import com.favant.util.MainHandler;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Hung on 11/14/2015.
 */
public class Loading extends RelativeLayout {
    private static final String TAG = "Loading";
    private LoadingCircle loading_circle;
//    private ImageView loading_logo;
    private AtomicBoolean can_spin = new AtomicBoolean();
    private float rotation = 0;
    private ValueAnimator anim;

    public Loading(Context context) {
        super(context);
        init(null);
    }

    public Loading(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public Loading(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        can_spin.set(true);
        loading_circle = new LoadingCircle(getContext());
        loading_circle.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//        loading_logo = new ImageView(getContext());
//        loading_logo.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//
////        int circle_res_id = 0;
////        if (android_width < DpPx.dpToPx(getContext(), 40) && android_width > 0) {
////            circle_res_id = R.drawable.loading_circle_25;
////        } else if (android_width < DpPx.dpToPx(getContext(), 70) && android_width > 0) {
////            circle_res_id = R.drawable.loading_circle_60;
////        } else {
////            circle_res_id = R.drawable.loading_circle;
////        }
////
//        loading_logo.setImageResource(R.drawable.loading_logo);

        addView(loading_circle);
//        addView(loading_logo);
        spin();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

//        int logo_id = 0;
//        if (w < DpPx.dpToPx(getContext(), 40) && w > 0) {
//            Log.v(TAG, "loading_logo_25");
//            logo_id = R.drawable.loading_logo_25;
//        } else if (w < DpPx.dpToPx(getContext(), 70) && w > 0) {
//            Log.v(TAG, "loading_logo_60");
//            logo_id = R.drawable.loading_logo_60;
//        } else {
//            Log.v(TAG, "loading_logo");
//            logo_id = R.drawable.loading_logo;
//        }
//
//        loading_logo.setImageResource(logo_id);
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (visibility == View.VISIBLE) {
            startSpin();
            Log.v(TAG, "show loading");
        } else {
            stopSpin();
            Log.v(TAG, "hide loading");
        }
    }

    private void stopSpin() {
        if (anim != null) anim.end();
//        can_spin.set(false);
    }

    private void startSpin() {
        if (anim == null) {
            anim = ObjectAnimator.ofFloat(loading_circle, "rotation", 0, 360);
            anim.setRepeatCount(-1);
            anim.setDuration(500);
        }
        anim.start();
//        if (!can_spin.get()) {
//            can_spin.set(true);
//            spin();
//        }
    }

    private void spin() {
        if (!can_spin.get()) return;
        rotation += 10;
        if (rotation >= 360) rotation = 0;
        loading_circle.setRotation(rotation);
//        if (rotation <= 180) {
//            loading_logo.setAlpha(rotation / 180);
//        } else {
//            loading_logo.setAlpha((360 - rotation) / 180);
//        }
        MainHandler.postDelay(new Runnable() {
            @Override
            public void run() {
                spin();
            }
        }, 40);
    }

    public class LoadingCircle extends View {

        public LoadingCircle(Context context) {
            super(context);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            LinearGradient gradient = new LinearGradient(0, getHeight() / 2, getWidth(), getHeight() / 2, 0xFFFFFFFF,
                    0xFF000000, android.graphics.Shader.TileMode.CLAMP);

//            RadialGradient gradient = new RadialGradient(200, 200, 200, 0xFFFFFFFF,
//                    0xFF000000, android.graphics.Shader.TileMode.CLAMP);

            Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeWidth(DpPx.dpToPx(getContext(), 1.5f));
            mPaint.setColor(Color.parseColor("#000000"));
            mPaint.setDither(true);
            mPaint.setShader(gradient);

            canvas.drawCircle(getWidth() / 2, getWidth() / 2, getWidth() / 2.2f, mPaint);
        }
    }
}
