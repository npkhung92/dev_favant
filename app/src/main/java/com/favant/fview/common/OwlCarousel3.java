package com.favant.fview.common;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.favant.R;
import com.favant.util.DpPx;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Hung on 10/31/2015.
 */
public class OwlCarousel3 extends RelativeLayout {
    private static final String TAG = "OwlCarousel3";
    public CustomRecyclerView mRecycleView;
    private CustomLayoutManager mLayoutManager;
    protected float nitem = 1;
    protected float space_between_item = 0;
    protected Drawable background = null;
    protected float padding_top = 0, padding_bottom = 0, padding_left = 0, padding_right = 0;
    protected int animate_time = 200;
    protected int autoload_ms = 0;
    protected int scroll_per_item = 1;
    private int item_count = 0;
    private boolean infinite_scroll = false;
    private boolean show_dot = false;
    private float dot_margin_bottom = 0;
    private Dots mDots;
    private OwlCarousel3Adapter mOwlCarousel3Adapter;
    private int autoscroll_ms;
    private AtomicBoolean view_is_visible = new AtomicBoolean();
    private boolean lock_scroll;
    private String[] myDataset = new String[]{"a", "b", "c", "d"};


    public OwlCarousel3(Context context) {
        super(context);
        init(context, null);
    }

    public OwlCarousel3(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public OwlCarousel3(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (!isInEditMode()) {
            this.setHorizontalScrollBarEnabled(false);

            // Inflate inner_layout
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            inflater.inflate(R.layout.owl_carousel_3, this, true);

            mRecycleView = (CustomRecyclerView) findViewById(R.id.recycler_view);
            mRecycleView.setHasFixedSize(true);
            mLayoutManager = new CustomLayoutManager(context, 500);
            mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            mRecycleView.setLayoutManager(mLayoutManager);
//            mRecycleView.setSelector(new StateListDrawable());
            mOwlCarousel3Adapter = new OwlCarousel3Adapter();
            mRecycleView.setAdapter(mOwlCarousel3Adapter);


            View dots_wrapper = findViewById(R.id.dots_wrapper);
            dot_margin_bottom = dots_wrapper.getPaddingBottom();
            mDots = new Dots((RelativeLayout) findViewById(R.id.dots_wrapper), (LinearLayout) findViewById(R.id.dots));

            // Read inner_layout attributes
            if (attrs != null) {
                TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.Favant, 0, 0);

                try {
                    nitem = a.getFloat(R.styleable.Favant_nitem, nitem);
                    space_between_item = a.getDimension(R.styleable.Favant_space_between_item, space_between_item);
//                    background = a.getDrawable(R.styleable.Sendo_background);
                    padding_top = a.getDimension(R.styleable.Favant_padding_top, padding_top);
                    padding_bottom = a.getDimension(R.styleable.Favant_padding_bottom, padding_bottom);
                    padding_left = a.getDimension(R.styleable.Favant_padding_left, padding_left);
                    padding_right = a.getDimension(R.styleable.Favant_padding_right, padding_right);

                    animate_time = a.getInteger(R.styleable.Favant_animate_time, animate_time);
//                    if (mListView != null) mListView.setScrollTime(animate_time);

                    scroll_per_item = a.getInteger(R.styleable.Favant_scroll_per_item, scroll_per_item);
//                    if (mListView != null) mListView.setScrollPerItem(scroll_per_item);

                    infinite_scroll = a.getBoolean(R.styleable.Favant_infinite_scroll, infinite_scroll);
                    show_dot = a.getBoolean(R.styleable.Favant_show_dot, show_dot);
                    dot_margin_bottom = a.getDimension(R.styleable.Favant_dot_margin_bottom, dot_margin_bottom);
                    autoscroll_ms = a.getInt(R.styleable.Favant_autoscroll_ms, autoscroll_ms);
                    if (show_dot && Math.floor(nitem) != nitem) {
                        show_dot = false;
                    }
                    autoload_ms = a.getInt(R.styleable.Favant_autoscroll_ms, autoload_ms);
                } finally {
                    a.recycle();
                }

                if (mDots != null && show_dot) {
                    mDots.setMarginBottom(dot_margin_bottom);
                }
//                if (autoscroll_ms > 0) {
//                    view_is_visible.set(true);
//                    scroll(true);
//                }
            }
        }
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
//        if (visibility == View.VISIBLE) {
//            if (autoscroll_ms > 0) startAutoscroll();
//            Log.v(TAG, "show loading");
//        } else {
//            if (autoscroll_ms > 0) stopAutoscroll();
//            Log.v(TAG, "hide loading");
//        }
    }

    private void stopAutoscroll() {
        view_is_visible.set(false);
//        mListView.smoothScrollToPositionFromLeft(mListView.getCurrentItem(), 0);
        mRecycleView.smoothScrollToPosition(mRecycleView.getCurrentItem());
    }

    private void startAutoscroll() {
        if (!view_is_visible.get()) {
            view_is_visible.set(true);
            scroll(true);
        }
    }

    private void scroll(boolean this_is_first_call) {
        if (!view_is_visible.get()) return;

        if (!this_is_first_call) {
//            mListView.smoothScrollToPositionFromLeft(mListView.getCurrentItem() + scroll_per_item, 0, mListView.scroll_time);
            mRecycleView.smoothScrollToPosition(mRecycleView.getCurrentItem() + scroll_per_item);
        }

        Handler handler = new Handler(Looper.getMainLooper());

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                scroll(false);
            }
        }, autoscroll_ms);
    }

    public void notifyDataSetChanged() {
        setAdapter(mOwlCarousel3Adapter.core_adapter);
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        if (mOwlCarousel3Adapter != null) {
            mOwlCarousel3Adapter.setCoreAdapter(adapter);
//            mOwlCarousel3Adapter.AddDataset(myDataset);
            mOwlCarousel3Adapter.notifyDataSetChanged();

//            if (infinite_scroll) mRecycleView.setSelection(adapter.getCount() * 5000);

            if (show_dot) {
                mDots.reset();
                for (int i = 0; i < adapter.getItemCount(); i++) {
                    mDots.addDot();
                }
                if (adapter.getItemCount() > 1) {
                    mDots.show();
//                    if (!lock_scroll) mRecycleView.unlockScroll();
                }

                mRecycleView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        if (mRecycleView.getChildCount() > 0) {
                            int item_count = mOwlCarousel3Adapter.getCoreAdapter().getItemCount();
                            float last_visible = mLayoutManager.findLastVisibleItemPosition() % item_count;
                            float first_visible = mLayoutManager.findFirstVisibleItemPosition() % item_count;
                            float left = mRecycleView.getChildAt(0).getLeft();
                            float width = mRecycleView.getChildAt(0).getWidth();

                            mDots.gotoChildonHand(first_visible - left / width);
                        }
                    }
                });
            }
            if (infinite_scroll)
                mLayoutManager.scrollToPosition(mOwlCarousel3Adapter.getItemCount() / 2);

        }
    }

    private int getItemWidth() {
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screen_width = size.x;
        int w = (int) ((screen_width - space_between_item * (nitem - 1) - padding_left - padding_right) / nitem);
        return w;
    }



    private class OwlCarousel3Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private RecyclerView.Adapter core_adapter;

        public void AddDataset(String[] Dt){
//            if (core_adapter instanceof MyAdapter){
//                ((MyAdapter) core_adapter).AddDataset(Dt);
//            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (core_adapter.getItemCount() == 0) return null;
            boolean view_was_null = parent.getChildAt(0) == null;
            if (core_adapter == null) return null;

            if (view_was_null) {
                ViewGroup.LayoutParams lp = parent.getLayoutParams();
                if (lp == null) {
//                    lp = new AbsHListView.LayoutParams(getItemWidth(), ViewGroup.LayoutParams.MATCH_PARENT);
                    lp = new RecyclerView.LayoutParams(getItemWidth(), ViewGroup.LayoutParams.MATCH_PARENT);
                } else {
                    lp.width = getItemWidth();
                }
                parent.setLayoutParams(lp);
            }
            RecyclerView.ViewHolder vh = core_adapter.onCreateViewHolder(parent, viewType);
            return vh;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            core_adapter.onBindViewHolder(holder, position);
        }

        @Override
        public int getItemCount() {
            if (core_adapter == null) return 0;
            if (infinite_scroll) {
                return core_adapter.getItemCount() * 10000;
            }
            return core_adapter.getItemCount();
        }

        public RecyclerView.Adapter getCoreAdapter() {
            return core_adapter;
        }

        public void setCoreAdapter(RecyclerView.Adapter adapter) {
            core_adapter = adapter;
        }


    }

    private class Dots {

        int current_position = 0;
        private RelativeLayout outer_layout = null;
        private LinearLayout inner_layout = null;
        private int margin_left = 0;
        private int width = 0;
        private Bitmap cached_grey_dot = null;
        private Bitmap cached_red_dot = null;
        private int dot_count = 0;
        private ImageView red_dot;

        public Dots(RelativeLayout ol, LinearLayout il) {
            outer_layout = ol;
            inner_layout = il;
            margin_left = (int) DpPx.dpToPx(getContext(), 4);
            width = (int) DpPx.dpToPx(getContext(), 10);
        }

        public void moveTo(int x) {
            if (outer_layout != null) {
                outer_layout.setX(x);
            }
        }

        public int getWidth() {
            if (outer_layout != null) return outer_layout.getWidth();
            return 0;
        }

        private Bitmap getGreyDotBitmap() {
            if (cached_grey_dot != null && !cached_grey_dot.isRecycled()) return cached_grey_dot;
            Bitmap b = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
            p.setStyle(Paint.Style.FILL);
            p.setColor(0xff8e8e8e);
            c.drawCircle(width / 2, width / 2, width / 2, p);
            cached_grey_dot = b;
            return b;
        }

        private Bitmap getRedDotBitmap() {
            if (cached_red_dot != null && !cached_red_dot.isRecycled()) return cached_red_dot;
            Bitmap b = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
            p.setStyle(Paint.Style.FILL);
            p.setColor(0xffff0033);
            c.drawCircle(width / 2, width / 2, width / 2, p);
            cached_red_dot = b;
            return b;
        }

        public void addDot() {
            ImageView img = new ImageView(getContext());

            img.setImageBitmap(getGreyDotBitmap());

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, width);
            lp.setMargins(margin_left, 0, 0, 0);
            img.setLayoutParams(lp);
            if (inner_layout != null) inner_layout.addView(img);

            dot_count++;

            if (dot_count == 1) {
                red_dot = new ImageView(getContext());

                red_dot.setImageBitmap(getRedDotBitmap());

                LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(width, width);
                red_dot.setLayoutParams(lp);
                if (outer_layout != null) outer_layout.addView(red_dot);
                red_dot.setX(margin_left);
            }
        }

        private int getXFromPosition(int position) {
            return position * (width + margin_left) + margin_left;
        }

        public void gotoChild(int next_child) {
            if (dot_count == 0) return;
            while (next_child >= dot_count) next_child = next_child - dot_count;
            current_position = next_child;

            final ValueAnimator anim = ObjectAnimator.ofFloat(red_dot, "X", red_dot.getX(), getXFromPosition(current_position))
                    .setDuration(200);
            anim.start();
        }

        public void gotoChildonHand(float next_child) {
            if (dot_count == 0) return;
            while (next_child >= dot_count) next_child = next_child - dot_count;
            if (next_child + 1 > dot_count) {
                next_child = dot_count - next_child;
                next_child = (dot_count - 1) * next_child;
            }

            red_dot.setX(next_child * (width + margin_left) + margin_left);
        }

        public void setMarginBottom(float margin) {
            if (outer_layout != null)
                outer_layout.setPadding(outer_layout.getPaddingLeft(),
                        outer_layout.getPaddingTop(),
                        outer_layout.getPaddingRight(),
                        (int) margin);
        }

        public void show() {
            if (outer_layout != null) outer_layout.setVisibility(View.VISIBLE);
        }

        public void reset() {
            dot_count = 0;
            current_position = 0;
            outer_layout.removeView(red_dot);
            inner_layout.removeAllViews();
        }
    }
}
