package com.favant.fview.common.LeftNavigator;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.favant.R;
import com.favant.collection.CategoryCollection;
import com.favant.model.Category;
import com.favant.model.FCategory;
import com.favant.model.FNavigationItem;
import com.favant.util.MainHandler;
import com.favant.util.Screen;
import com.favant.util.ViewChecker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hung on 10/27/2015.
 */
public class FLeftNavigationDrawer extends RelativeLayout {
    private final static String TAG = "FLeftNavigationDrawer";
    private final int ANIMATION_TIME = 300;
    private boolean ANIMATING_STATE;
    private final static String HOME = "Home";
    private final static String NEW_ARRIVAL = "New Arrivals";
    private final static String BEST_SELLER = "Best Seller";
    private ListView left_navi_list;
    private ListView leftCategoryList;
    private View categoryListWrapper;
    private FLeftNavDrawAdapter left_nav_draw_adapter;
    private FLeftCategoryAdapter leftCategoryAdapter;
    private List<FCategory> categories = new ArrayList<>();
    private String[] category_names;
    private CategoryCollection categoryCollection;

    private ArrayList<LeftNavigationDrawerListener> listeners = new ArrayList<LeftNavigationDrawerListener>();

    public FLeftNavigationDrawer(Context context) {
        super(context);
        init(context, null);
    }

    public FLeftNavigationDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FLeftNavigationDrawer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {

    }

    public void addLeftNavigationDrawer(LeftNavigationDrawerListener mrl) {
        if (mrl != null) listeners.add(mrl);
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        categoryCollection = CategoryCollection.getInstance();


        if (ViewChecker.validate(findViewById(R.id.left_overlay), View.class)) {
            findViewById(R.id.left_overlay).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    hide();
                }
            });
        }
        categoryListWrapper = findViewById(R.id.category_list_wrapper);
        //add Item into Drawer
        left_navi_list = (ListView) findViewById(R.id.left_navi_list);
        left_nav_draw_adapter = new FLeftNavDrawAdapter(getContext(), this);

        leftCategoryList = (ListView) findViewById(R.id.category_list);
        leftCategoryAdapter = new FLeftCategoryAdapter(getContext());

        categoryCollection.getAllCategory(new CategoryCollection.AllCategoryCallbackAdapter() {
            @Override
            public void allCategory(List<FCategory> categories) {
                Log.v(TAG, String.valueOf(categories));
                FLeftNavigationDrawer.this.categories = categories;
                getCategoryName();
            }
        });

        left_navi_list.setAdapter(left_nav_draw_adapter);
//        if (left_navi_list != null) {
//            left_nav_draw_adapter = new FLeftNavDrawAdapter(getContext(), this);
//            left_nav_draw_adapter.clear();
//            left_nav_draw_adapter.addItem(new FNavigationItem.NavigationObject(null, HOME, 0, "", false));
//            left_nav_draw_adapter.addItem(new FNavigationItem.NavigationObject(null, NEW_ARRIVAL, 0, "", false));
//            left_nav_draw_adapter.addItem(new FNavigationItem.NavigationObject(null, "Hello, Sign In", 0, "", false));
//            left_nav_draw_adapter.addItem(new FNavigationItem.NavigationObject(null, "", 0, "", true));
//            left_navi_list.setAdapter(left_nav_draw_adapter);
//            showParentDivider();
//        }


    }

    private void getCategoryName() {
        categoryCollection.getCategoryNameAsString(new CategoryCollection.AllCategoryCallbackAdapter() {
            @Override
            public void stringArray(String[] category_names) {
                Log.v(TAG, String.valueOf(category_names));
                FLeftNavigationDrawer.this.category_names = category_names;
                for (int i = 0; i < category_names.length; i++) {
                    left_nav_draw_adapter.addItem(new FNavigationItem.NavigationObject(null, category_names[i], 0, "", false));
                    showParentDivider();
                }
            }
        });
    }





    public void addCategoryItems(FLeftNavDrawAdapter da) {
//        ArrayList<FNavigationItem.NavigationObject> cats = new ArrayList<FNavigationItem.NavigationObject>();
        da.addItem(new FNavigationItem.NavigationObject(null, "Women", 0, "", false));
        da.addItem(new FNavigationItem.NavigationObject(null, "Men", 0, "", false));
        da.addItem(new FNavigationItem.NavigationObject(null, "Boys", 0, "", false));
        da.addItem(new FNavigationItem.NavigationObject(null, "Girls", 0, "", false));
        da.addItem(new FNavigationItem.NavigationObject(null, "T-shirts", 0, "", false));
        da.addItem(new FNavigationItem.NavigationObject(null, "Sarongs", 0, "", false));
        da.addItem(new FNavigationItem.NavigationObject(null, "Towels", 0, "", false));
        da.addItem(new FNavigationItem.NavigationObject(null, "Boardshorts", 0, "", false));
        da.addItem(new FNavigationItem.NavigationObject(null, "Bikinis", 0, "", false));
//        return cats;
    }

    public void addSubCateItems() {
        leftCategoryAdapter.addHeader(new Category("WOMEN"));
        leftCategoryAdapter.addItem(new Category("Dresses"));
        leftCategoryAdapter.addItem(new Category("Blouses"));
        leftCategoryAdapter.addItem(new Category("Swimwear"));
        leftCategoryAdapter.addItem(new Category("Shorts"));
        showSubCateList();
    }

    public void showParentDivider() {
        Drawable divider = left_navi_list.getDivider();
        divider.setAlpha(255);
        left_navi_list.setDivider(divider);
        left_navi_list.setDividerHeight(1);
        leftCategoryList.setDivider(divider);
        leftCategoryList.setDividerHeight(1);
    }

//    public void setContent(View v) {
//        if (ViewChecker.validate(findViewById(R.id.left_navi_drawer), ViewGroup.class)) {
//            ViewGroup vg = (ViewGroup) findViewById(R.id.left_navi_drawer);
//            vg.removeAllViews();
//            vg.addView(v);
//        }
//    }
//
//    public void removeContent() {
//        if (ViewChecker.validate(findViewById(R.id.left_navi_drawer), ViewGroup.class)) {
//            ViewGroup vg = (ViewGroup) findViewById(R.id.left_navi_drawer);
//            vg.removeAllViews();
//        }
//    }

    private void showSubCateList() {
        MainHandler.post(new Runnable() {
            @Override
            public void run() {
                categoryListWrapper.setVisibility(View.VISIBLE);
                if (ViewChecker.validate(findViewById(R.id.category_list), ListView.class)) {
                    ValueAnimator anim = ObjectAnimator.ofFloat(categoryListWrapper, "translationX", -Screen.getWidth(getContext()), 0);
                    anim.setDuration(ANIMATION_TIME);
                    anim.start();

                    anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            ANIMATING_STATE = false;
                            for (int i = 0; i < listeners.size(); i++) {
                                LeftNavigationDrawerListener mrl = listeners.get(i);
                                mrl.show();
                            }
                        }
                    });
                }
            }
        });

    }

    private void hideSubCateList() {
        MainHandler.post(new Runnable() {
            @Override
            public void run() {
                if (ViewChecker.validate(findViewById(R.id.category_list), ListView.class)) {
                    ValueAnimator anim = ObjectAnimator.ofFloat(categoryListWrapper, "translationX", 0, -Screen.getWidth(getContext()));
                    anim.setDuration(ANIMATION_TIME);
                    anim.start();

                    anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            categoryListWrapper.setVisibility(View.VISIBLE);
                            ANIMATING_STATE = false;
                            for (int i = 0; i < listeners.size(); i++) {
                                LeftNavigationDrawerListener mrl = listeners.get(i);
                                mrl.show();
                            }
                        }
                    });
                }
            }
        });

    }

    public void show() {
        if (ANIMATING_STATE) return;
        ANIMATING_STATE = true;
        MainHandler.post(new Runnable() {
            @Override
            public void run() {
                setVisibility(View.VISIBLE);
                bringToFront();
                Log.v(TAG, String.valueOf(findViewById(R.id.left_navi_drawer)));
                if (ViewChecker.validate(findViewById(R.id.left_navi_drawer), LinearLayout.class)) {
                    LinearLayout left_navi_drawer = (LinearLayout) findViewById(R.id.left_navi_drawer);
//                    menu_right.setX(Screen.getWidth(getContext()));
//                    left_navi_drawer.setX(Screen.getWidth(getContext()));
                    ValueAnimator anim = ObjectAnimator.ofFloat(left_navi_drawer, "translationX", -Screen.getWidth(getContext()), 0);
                    anim.setDuration(ANIMATION_TIME);
                    anim.start();

                    anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            ANIMATING_STATE = false;
                            for (int i = 0; i < listeners.size(); i++) {
                                LeftNavigationDrawerListener mrl = listeners.get(i);
                                mrl.show();
                            }
                        }
                    });
                }
            }
        });
    }

    public void hide(final boolean reset, final boolean animation) {
        if (ANIMATING_STATE) return;
        ANIMATING_STATE = true;

        MainHandler.post(new Runnable() {
            @Override
            public void run() {
                if (ViewChecker.validate(findViewById(R.id.left_navi_drawer), LinearLayout.class)) {
                    final LinearLayout left_navi_drawer = (LinearLayout) findViewById(R.id.left_navi_drawer);
                    if (animation) {
                        final float current_x = left_navi_drawer.getX();
                        ValueAnimator anim = ObjectAnimator.ofFloat(left_navi_drawer, "translationX", 0, -Screen.getWidth(getContext()));
                        anim.setDuration(ANIMATION_TIME);
                        anim.start();

                        anim.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                setVisibility(View.GONE);
//                                if (reset) removeContent();
                                ANIMATING_STATE = false;
                                for (int i = 0; i < listeners.size(); i++) {
                                    LeftNavigationDrawerListener mrl = listeners.get(i);
                                    mrl.hide();
                                }
                            }
                        });
                    } else {
                        setVisibility(View.INVISIBLE);
//                        if (reset) removeContent();
                        ANIMATING_STATE = false;
                        for (int i = 0; i < listeners.size(); i++) {
                            LeftNavigationDrawerListener mrl = listeners.get(i);
                            mrl.hide();
                        }
                    }
                }
            }
        });
    }

    public boolean isShowing() {
        return getVisibility() == View.VISIBLE;
    }

    public void hide() {
        hide(true, true);
    }

    public void hideWithNoAnimation() {
        hide(true, false);
    }

    public static interface LeftNavigationDrawerListener {
        public void show();

        public void hide();
    }
}
