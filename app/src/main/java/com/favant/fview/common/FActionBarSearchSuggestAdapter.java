package com.favant.fview.common;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.favant.R;
import com.favant.model.PageOption;
import com.favant.model.SearchHistory;
import com.favant.router.Router;
import com.favant.router.RouterInterface;
import com.favant.util.UrlParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hung on 11/24/2015.
 */
public class FActionBarSearchSuggestAdapter extends BaseAdapter {
    private static final String TAG = "SearchSuggestAdapter";
    private final FActionBar fActionBar;
    private final Context context;
    private final RouterInterface router;
    private ArrayList<SearchData> suggest_list = new ArrayList<SearchData>();

    private String user_input;

    public FActionBarSearchSuggestAdapter(Context c, FActionBar ab, RouterInterface router) {
        super();
        context = c;
        fActionBar = ab;
        this.router = router;
    }

    @Override
    public int getCount() {
        return suggest_list.size();
    }

    @Override
    public Object getItem(int i) {
        return suggest_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.parent_category_component, null);
        }
        TextView text = (TextView) view.findViewById(R.id.title);
        if (getItem(i) instanceof SearchData) {
            final SearchData sd = (SearchData) getItem(i);
            text.setText(Html.fromHtml(sd.text));

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (sd.type == SearchData.TYPE_SUGGEST)
                        SearchHistory.getInstane().addSearch(new SearchHistory.SearchModel(user_input, sd.text, sd.url));
                    fActionBar.hideFullSearchBox(new Runnable() {
                        @Override
                        public void run() {
                            fActionBar.updateSaveSearchKey(user_input);
                            if (!"".equals(sd.url) && sd.url != null) {
                                UrlParser up = new UrlParser(context, router);
                                up.parseAndGoToPage(sd.url, false);
                            } else {
                                Bundle data = new Bundle();
                                data.putString("search_key", sd.text);

                                router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_SEARCH).setData(data));
                            }

//                            MainHandler.postDelay(new Runnable() {
//                                @Override
//                                public void run() {
//                                    sActionBar.hideKeyboardForSearch();
//                                }
//                            }, MasterActivity.ANIMATION_DURATION + 300);
                        }
                    });
                }
            });
        }
        return view;
    }

    public void setData(String input, JSONObject cloud, JSONObject category, JSONObject product_name) {
        user_input = input;
        suggest_list = new ArrayList<SearchData>();
        suggest_list.add(new SearchData(input, "tim-kiem/?q=" + input, SearchData.TYPE_SUGGEST));
        if (category != null) {
            JSONArray list = category.optJSONArray("list");
            if (list != null) {
                for (int i = 0; i < list.length(); i++) {
                    JSONObject obj = list.optJSONObject(i);
                    if (obj == null) continue;

                    String cate_name = obj.optString("cate_name");
                    String cate_id = obj.optString("cate_id");
                    if (!cate_name.equals(""))
                        suggest_list.add(new SearchData(
                                input + " <font color=#ec2800>trong</font> <font color=#4786f4>" + cate_name + "</font>",
                                "tim-kiem/?q=" + input + "&category_id=" + cate_id, SearchData.TYPE_SUGGEST));
                }
            }
        }

        if (cloud != null) {
            JSONArray list = cloud.optJSONArray("list");
            if (list != null) {
                for (int i = 0; i < list.length(); i++) {
                    JSONObject obj = list.optJSONObject(i);
                    if (obj == null) continue;

                    String q = obj.optString("q");
                    if (!q.equals(input) && !q.equals(""))
                        suggest_list.add(new SearchData(q, "tim-kiem/?q=" + q, SearchData.TYPE_SUGGEST));
                }
            }
        }
        notifyDataSetChanged();
    }

    public void clearList() {
        suggest_list = new ArrayList<SearchData>();
        notifyDataSetChanged();
    }

    public synchronized void setSearchHistory(ArrayList<SearchHistory.SearchModel> search_history) {
        for (SearchHistory.SearchModel sm : search_history) {
            if (sm.user_input != null && sm.url != null)
                suggest_list.add(new SearchData(sm.user_input, sm.url, SearchData.TYPE_HISTORY));
        }
        notifyDataSetChanged();
    }

    public synchronized void setTopKeyword(List<String> topKeyword) {
        for (String str : topKeyword) {
            suggest_list.add(new SearchData(str, "", SearchData.TYPE_HISTORY));
        }
        notifyDataSetChanged();
    }

    private static class SearchData {
        public static final int TYPE_SUGGEST = 1;
        public static final int TYPE_HISTORY = 2;
        public static final int TYPE_TOP_KEYWORD = 3;

        public String text = "";
        public String url = "";
        public int type;

        public SearchData(String text, String url, int type) {
            this.text = text;
            this.url = url;
            this.type = type;
        }
    }
}
