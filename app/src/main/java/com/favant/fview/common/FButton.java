package com.favant.fview.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.favant.R;

/**
 * Created by Bear on 8/22/2015.
 */
public class FButton extends Button {
    public FButton(Context context) {
        super(context);
        init(context, null);
    }

    public FButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }
    private void init(Context context, AttributeSet attrs) {
        // Read layout attributes
        if (!isInEditMode()) {
            String font_weight = "none";
            if (attrs != null) {
                TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.Favant, 0, 0);
                try {
                    font_weight = a.getString(R.styleable.Favant_fontWeight);
                } finally {
                    a.recycle();
                }
            }
            if (font_weight == null) font_weight = "none";
//            if (font_weight.equals("none")) {
//                Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/FuturaStd_Book.ttf");
//                this.setTypeface(type);
//            } else if (font_weight.equals("bold")) {
//                Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/FuturaStd_Bold.ttf");
//                this.setTypeface(type);
//            } else if (font_weight.equals("italic")) {
//                Typeface type = Typeface.createFromAsset(context.getAssets(), "fonts/FuturaStd_BookOblique.ttf");
//                this.setTypeface(type);
//            }
        }
    }
}
