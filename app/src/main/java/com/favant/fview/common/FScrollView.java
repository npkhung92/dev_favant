package com.favant.fview.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

import java.util.ArrayList;

/**
 * Created by Bear on 8/22/2015.
 */
public class FScrollView extends ScrollView {
    protected ArrayList<OnScrollChangedListener> mOnScrollChangedListener = new ArrayList<OnScrollChangedListener>();
    protected boolean mIsFling = false;
    protected boolean enableScrolling = true;
    public FScrollView(Context context) {
        super(context);
    }

    public FScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    public boolean isEnableScrolling() {
        return enableScrolling;
    }

    public void setEnableScrolling(boolean enableScrolling) {
        this.enableScrolling = enableScrolling;
    }

    public void addOnScrollChangedListener(OnScrollChangedListener listener) {
        mOnScrollChangedListener.add(listener);
    }

    @Override
    public void fling(int velocityY) {
        super.fling(velocityY);
        mIsFling = true;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (mOnScrollChangedListener != null && mOnScrollChangedListener.size() > 0) {
            if (t != oldt) {
                for (int i = 0; i < mOnScrollChangedListener.size(); i++) {
                    OnScrollChangedListener listener = mOnScrollChangedListener.get(i);
                    listener.onScrollChanged();
                }
            }
            if (mIsFling) {
                if (Math.abs(t - oldt) < 2 || t >= getMeasuredHeight() || t == 0) {
                    for (int i = 0; i < mOnScrollChangedListener.size(); i++) {
                        OnScrollChangedListener listener = mOnScrollChangedListener.get(i);
                        listener.onScrollEnd();
                    }
                    mIsFling = false;
                }
            }
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (isEnableScrolling()) {
            return super.onInterceptTouchEvent(ev);
        } else {
            return false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (isEnableScrolling()) {
            return super.onTouchEvent(ev);
        } else {
            return false;
        }
    }
}
