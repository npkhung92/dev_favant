package com.favant.fview.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.favant.R;
import com.favant.util.ViewChecker;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Hung on 1/14/2016.
 */
public class FCustomRating extends LinearLayout {
    private Context mContext;
    private float rating = 0;
    private float current_rating = 0;
    private HashMap<ViewGroup, Float> star_map = new HashMap<ViewGroup, Float>();

    public FCustomRating(Context context) {
        super(context);
        init(context, null);
    }

    public FCustomRating(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FCustomRating(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }
    private void init(Context context, AttributeSet attrs) {
        mContext = context;
        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.Favant, 0, 0);
            try {
                rating = a.getFloat(R.styleable.Favant_rating, 0);
            } finally {
                a.recycle();
            }
        }
        setOrientation(HORIZONTAL);
        initStar();
    }

    public float getCurrentStarPoint(){
        return current_rating;
    }

    private void initStar(){
        LayoutInflater layout_inflater = LayoutInflater.from(mContext);
        if (getChildCount() == 0) {
            for (int i = 0; i < 5; i++) {
                final ViewGroup star = (ViewGroup) layout_inflater.inflate(R.layout.star, this, false);
//                star.setPadding(0, 0, 10, 0);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, ViewGroup.LayoutParams.MATCH_PARENT);
                ImageView star_grey = (ImageView) star.findViewById(R.id.star);
                ImageView star_full = (ImageView) star.findViewById(R.id.star_full);
                star_grey.setLayoutParams(params);
                star_grey.setImageResource(R.drawable.star_rating_full_grey);
                star_full.setLayoutParams(params);
                star_full.setImageResource(R.drawable.star_rating_full);
                star_full.setVisibility(View.INVISIBLE);
                addView(star);
                rating++;
                star_map.put(star, rating);
//                if (i == 4){
//                    current_rating = 0;
//                }
                star.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        for (Map.Entry<ViewGroup , Float> e : star_map.entrySet()){
                            if (star == e.getKey()){
                                current_rating = e.getValue();
                                Log.v("rating ", "rating now is " + current_rating);
                                for (int i = 0; i < 5; i++) {
                                    ViewGroup star = (ViewGroup) getChildAt(i);
                                    if (ViewChecker.validate(star.findViewById(R.id.star_full), ImageView.class)) {
                                        ImageView star_full = (ImageView) star.findViewById(R.id.star_full);
                                        if (i + 1 > current_rating) {
                                            float left = current_rating - i;
                                            if (left <= 0) {
                                                star_full.setVisibility(INVISIBLE);
                                            }
                                        } else {
                                            star_full.setVisibility(VISIBLE);
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }
    }
}
