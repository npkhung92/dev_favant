package com.favant.fview.common;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.favant.MasterActivity;
import com.favant.R;
import com.favant.router.RouterInterface;
import com.favant.util.DpPx;

import java.util.ArrayList;

/**
 * Created by Bear on 8/22/2015.
 */
public class FActionBar extends RelativeLayout {
    public static final int SEARCH_BOX_HIDDEN = 0;
    public static final int SEARCH_BOX_SHOW = 1;
    public static final int SEARCH_BOX_SHOW_FULL = 2;
    public static final int FULLSEARCH_FROM_EDITTEXT = 1;
    public static final int FULLSEARCH_FROM_SEARCHBUTTON = 2;
    private static final String TAG = "FActionBar";
    private static final int DISALLOW_SUGGEST = 1;
    private static final int ALLOW_SUGGEST = 2;
    private static String save_search_key = "";
    private static ArrayList<FActionBar> list_action_bar = new ArrayList<FActionBar>();
    private TextView page_title;
    private ImageView logo;
    private FActionBarCallBack mCallBack;
    private FFragment mFragment;
    private MasterActivity mMasterActivity;
    private boolean mLockSearchBoxToShow = false;
    private ViewGroup content_container;
    private FEditText.EditTextImeBackListener search_text_back_listener;
    private ListView search_suggest;
    private int first_height;
    private FActionBarSearchSuggestAdapter search_suggest_adapter;
    private int search_suggest_state = DISALLOW_SUGGEST;
    private int page_type_for_floating;
    private View search_overlay;
    private int additional_height_to_float = 0;
    private Runnable cart_update_listener;
    private int search_box_state = SEARCH_BOX_HIDDEN;
    private int fullsearch_open_from = FULLSEARCH_FROM_EDITTEXT;
    private boolean floating_animating;
    private RouterInterface router;


    public FActionBar(Context context) {
        super(context);
    }

    public FActionBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FActionBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public static void updateSaveSearchKey(String search_key) {
        save_search_key = search_key;
        Log.v(TAG, "save search key =" + save_search_key);
        Log.v(TAG, "update ab: " + list_action_bar.size() + " -> " + save_search_key);
        for (FActionBar ab : list_action_bar) {
            EditText edt = (EditText) ab.findViewById(R.id.search_text);
            edt.setText(save_search_key);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (mFragment == null) return;
        Log.v(TAG, "FActionBar Attach on Fragment " + mFragment.getClass());
        list_action_bar.add(FActionBar.this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mFragment == null) return;
        Log.v(TAG, "FActionBar Detach on Fragment " + mFragment.getClass());
        list_action_bar.remove(FActionBar.this);
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();
        measure(0, 0);
        first_height = getMeasuredHeight();
        hideTitle();

        Button bar = getBarButton();
        if (bar != null) bar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallBack != null) mCallBack.barClicked();
            }
        });

        Button more = getMoreButton();
        if (more != null) more.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallBack != null) mCallBack.moreClicked();
            }
        });

        Button back = getBackButton();
        if (back != null) back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMasterActivity != null) {
                    mMasterActivity.onBackPressed();
                }
            }
        });

        Button cart = getCartButton();
        if (cart != null) cart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallBack != null) mCallBack.cartClicked();
            }
        });

        search_suggest = (ListView) findViewById(R.id.search_suggest);
        search_suggest_adapter = new FActionBarSearchSuggestAdapter(getContext(), this, router);
        search_suggest.setAdapter(search_suggest_adapter);

        search_overlay = findViewById(R.id.search_overlay);
        search_overlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                hideFullSearchBox(new Runnable() {
                    @Override
                    public void run() {
                        hideKeyboardForSearch();
                    }
                });
            }
        });

        Button search = getSearchButton();
        final FEditText search_text = getSearchText();
        final FEditTextWrapper search_text_wrapper = (FEditTextWrapper) findViewById(R.id.search_text_wrapper);
        if (search != null) search.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                fullsearch_open_from = FULLSEARCH_FROM_SEARCHBUTTON;
                showSearchBox(true, true);
                showFullSearchBox(null);
//                if (search_text.getText().toString().length() > 0)
//                    search_suggest.setVisibility(View.VISIBLE);
//                search_text.setSelection(search_text.getText().toString().length());
                search_suggest_state = ALLOW_SUGGEST;
            }
        });
    }

    private TextView getTitle() {
        return (TextView) findViewById(R.id.page_title);
    }

    public void setPageTitle(String title) {
        TextView page_title = getTitle();
        if (title.length() > 14) {
            String subStr = title.substring(0, 13) + "...";
            page_title.setText(subStr.toCharArray(), 0, title.length());
            showTitle();
            return;
        }
        page_title.setText(title.toCharArray(), 0, title.length());
        showTitle();
    }

    public void showTitle() {
        TextView page_title = getTitle();
        if (page_title != null) {
            page_title.setVisibility(View.VISIBLE);
        }
    }

    public void hideTitle() {
        TextView page_title = getTitle();
        if (page_title != null) {
            page_title.setVisibility(View.INVISIBLE);
        }
    }

    public Button getBarButton() {
        return (Button) findViewById(R.id.bar);
    }

    public void showBarButton() {
        Button bar = getBarButton();
        if (bar != null)
            bar.setVisibility(VISIBLE);
    }

    public Button getBackButton() {
        return (Button) findViewById(R.id.back);
    }

    public void showBackButton() {
        Button back = getBackButton();
        if (back != null)
            back.setVisibility(VISIBLE);
    }

    public Button getLocationButton() {
        return (Button) findViewById(R.id.loc);
    }

    public void showLocationButton() {
        Button loc = getLocationButton();
        if (loc != null)
            loc.setVisibility(VISIBLE);
    }

    public Button getSearchButton() {
        return (Button) findViewById(R.id.search);
    }

    public void showSearchButton() {
        Button search = getSearchButton();
        if (search != null)
            search.setVisibility(VISIBLE);
    }

    public Button getCartButton() {
        return (Button) findViewById(R.id.cart);
    }

    public void showCartButton() {
        Button cart = getCartButton();
        if (cart != null)
            cart.setVisibility(VISIBLE);
    }

    public Button getMoreButton() {
        return (Button) findViewById(R.id.more);
    }

    public void showMoreButton() {
        Button more = getMoreButton();
        if (more != null)
            more.setVisibility(VISIBLE);
    }

    private FImageView getLogo() {
        return (FImageView) findViewById(R.id.favant_logo);
    }

    public void setLogo(int resId) {
        FImageView logo = getLogo();
        if (logo != null) {
            Drawable dr = getContext().getResources().getDrawable(resId);
            logo.setImageDrawable(dr);
            showLogo();
        }
    }

    public void showLogo() {
        FImageView logo = getLogo();
        if (logo != null) {
            logo.setVisibility(View.VISIBLE);
        }
    }

    public void hideLogo() {
        FImageView logo = getLogo();
        if (logo != null) {
            logo.setVisibility(View.INVISIBLE);
        }
    }

    public void setContentContainer(ViewGroup cc) {
        content_container = cc;
    }


    public RelativeLayout getSearchBox() {
        return (RelativeLayout) findViewById(R.id.search_box);
    }

    public FEditText getSearchText() {
        return (FEditText) findViewById(R.id.search_text);
    }

    public void showKeyboardForSearch() {
        FEditText st = getSearchText();
        if (st != null) {
            st.requestFocus();
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(st, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public void hideKeyboardForSearch() {
        FEditText st = getSearchText();
        if (st != null) {
            st.clearFocus();
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(st.getWindowToken(), 0);
        }
    }

    public void showSearchBox(boolean animation, boolean request_focus) {
        if (search_box_state == SEARCH_BOX_HIDDEN) {
            RelativeLayout search_box = getSearchBox();
            Button search = getSearchButton();
            if (search_box != null && search != null) {
                search.setVisibility(View.INVISIBLE);
                if (animation) {
                    int width = search_box.getWidth();
                    search_box.setPivotX(width);
                    search_box.setScaleX(0);
                }

                search_box.setVisibility(View.VISIBLE);
                if (animation) {
                    ValueAnimator fadeAnim = ObjectAnimator.ofFloat(search_box, "ScaleX", 0f, 1f);
                    fadeAnim.start();
                    if (request_focus) {
                        fadeAnim.addListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                // TODO Auto-generated method stub
                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {
                                // TODO Auto-generated method stub
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
//                                showKeyboardForSearch();
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {
                                // TODO Auto-generated method stub
                            }
                        });
                    }
                } else {
                    if (request_focus) {
//                        showKeyboardForSearch();
                    }
                }
            }
            search_box_state = SEARCH_BOX_SHOW;
        }
    }


    public void showSearchBox() {
        showSearchBox(true, false);
    }

    public void showSearchBoxAndLock(boolean animation, boolean request_focus) {
        mLockSearchBoxToShow = true;
        showSearchBox(animation, request_focus);

        RelativeLayout search_box = getSearchBox();

        Button search_button = getSearchButton();
        search_button.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);

        FActionBar.LayoutParams lp = (LayoutParams) search_box.getLayoutParams();
        lp.rightMargin = search_button.getMeasuredHeight();
        search_box.setLayoutParams(lp);
    }

    public void hideSearchBox(boolean animation, final Runnable cb) {
        if (mLockSearchBoxToShow) return;
        if (search_box_state == SEARCH_BOX_SHOW) {
            final RelativeLayout search_box = getSearchBox();
            Button search = getSearchButton();
            if (search_box != null && search != null) {
                search.setVisibility(View.VISIBLE);
                if (animation) {
                    int width = search_box.getWidth();
                    search_box.setPivotX(width);
                    search_box.setScaleX(1);
                    ValueAnimator fadeAnim = ObjectAnimator.ofFloat(search_box, "ScaleX", 1f, 0f);
                    fadeAnim.start();

                    fadeAnim.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            // TODO Auto-generated method stub
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                            // TODO Auto-generated method stub
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            search_box.setVisibility(View.INVISIBLE);
                            search_box.setScaleX(1);

                            hideKeyboardForSearch();
                            if (cb != null) cb.run();
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            // TODO Auto-generated method stub
                        }
                    });
                } else {
                    search_box.setVisibility(View.INVISIBLE);

                    hideKeyboardForSearch();
                    if (cb != null) cb.run();
                }
            }
            search_box_state = SEARCH_BOX_HIDDEN;
        }
    }

    public void hideSearchBox() {
        hideSearchBox(true, null);
    }

    public void hideSearchBoxWithCallback(Runnable cb) {
        hideSearchBox(true, cb);
    }

    public void hideSearchBoxWithUnlock() {
        mLockSearchBoxToShow = false;
        hideSearchBox();
    }

    public void showFullSearchBox(final Runnable cb) {
        if (search_box_state == SEARCH_BOX_SHOW_FULL) return;
        search_overlay.setVisibility(View.VISIBLE);
        search_suggest.setVisibility(View.VISIBLE);
        final RelativeLayout search_box = getSearchBox();
        final FTextView cancel_search = (FTextView) findViewById(R.id.cancel_search);
        RelativeLayout.LayoutParams lp = (LayoutParams) search_box.getLayoutParams();
        lp.leftMargin = (int) DpPx.dpToPx(getContext(), 5);
        search_box.setLayoutParams(lp);
        search_box.setTranslationX(0);
        search_box.setTranslationX(DpPx.dpToPx(getContext(), 50));
        search_box.bringToFront();
        ((View) search_box.getParent()).invalidate();

        ValueAnimator anim = ObjectAnimator.ofFloat(search_box, "translationX", 0);
        anim.start();

        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                FActionBar.LayoutParams lp = (LayoutParams) search_box.getLayoutParams();
                float translationX = (Float) valueAnimator.getAnimatedValue();
                float percent = translationX / DpPx.dpToPx(getContext(), 50);
                lp.rightMargin = (int) (percent * DpPx.dpToPx(getContext(), 50) + DpPx.dpToPx(getContext(), 5));
                search_box.setLayoutParams(lp);
            }
        });

        cancel_search.setVisibility(View.VISIBLE);

        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (cb != null) cb.run();
                search_box_state = SEARCH_BOX_SHOW_FULL;
            }
        });

        search_text_back_listener = new FEditText.EditTextImeBackListener() {
            @Override
            public boolean onImeBack() {
                hideFullSearchBox(new Runnable() {

                    @Override
                    public void run() {
                        hideKeyboardForSearch();
                    }
                });
                getSearchText().removeOnEditTextImeBackListener(this);
                return true;
            }
        };

        getSearchText().addOnEditTextImeBackListener(search_text_back_listener);
    }

    public void hideFullSearchBox(final Runnable cb) {
        if (search_box_state != SEARCH_BOX_SHOW_FULL) return;

        search_overlay.setVisibility(View.GONE);

        search_suggest.setVisibility(View.GONE);
        search_suggest_state = DISALLOW_SUGGEST;

        final RelativeLayout search_box = getSearchBox();
        final FTextView cancel_search = (FTextView) findViewById(R.id.cancel_search);

        RelativeLayout.LayoutParams lp = (LayoutParams) search_box.getLayoutParams();
        if (getResources().getDisplayMetrics().density <= 1.5)
            lp.leftMargin = (int) DpPx.dpToPx(getContext(), 45);
        else
            lp.leftMargin = (int) DpPx.dpToPx(getContext(), 55);
        search_box.setLayoutParams(lp);
        search_box.setTranslationX(0);
        search_box.setTranslationX(DpPx.dpToPx(getContext(), -50));

        ValueAnimator anim = ObjectAnimator.ofFloat(search_box, "translationX", DpPx.dpToPx(getContext(), 0));
        anim.start();

        if (mLockSearchBoxToShow) {
            anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    FActionBar.LayoutParams lp = (LayoutParams) search_box.getLayoutParams();
                    float translationX = (Float) valueAnimator.getAnimatedValue();
                    float percent = 1 - translationX / DpPx.dpToPx(getContext(), -50);
                    lp.rightMargin = (int) (percent * DpPx.dpToPx(getContext(), 50));
                    search_box.setLayoutParams(lp);
                }
            });
        }

        cancel_search.setVisibility(View.GONE);

        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {

                search_box_state = SEARCH_BOX_SHOW;
                if (fullsearch_open_from == FULLSEARCH_FROM_EDITTEXT) {
                    if (cb != null) cb.run();
                }
                if (fullsearch_open_from == FULLSEARCH_FROM_SEARCHBUTTON) {
                    hideSearchBoxWithCallback(cb);
                }
            }
        });
    }
//    public void hideSearchButton() {
//        Button search = getSearchButton();
//        if (search != null) search.setVisibility(View.GONE);
//
//        RelativeLayout.LayoutParams lp = (LayoutParams) findViewById(R.id.page_title_wrapper).getLayoutParams();
//        lp.rightMargin = (int) DpPx.dpToPx(getContext(), 50);
//        lp.addRule(RelativeLayout.LEFT_OF, 0);
//
//        findViewById(R.id.page_title_wrapper).setLayoutParams(lp);
//    }

    public boolean isHiding() {
        boolean hiding = false;
        if (getY() < 0) hiding = true;
        if (content_container != null && content_container.getY() < first_height) hiding = true;
        return hiding;
    }

    //Interfaces
    public void setCallback(FActionBarCallBack cb) {
        this.mCallBack = cb;
    }

    public void setFragment(FFragment fFragment) {
        mFragment = fFragment;
    }

    public void setMasterActivity(MasterActivity ma) {
        mMasterActivity = ma;
    }

    public void setRouter(RouterInterface router) {
        this.router = router;
    }

    public static interface FActionBarCallBack {
        public void barClicked();

        public void moreClicked();

        public void cartClicked();
    }

    public abstract static class FActionBarCallBackAdapter implements FActionBarCallBack {
        @Override
        public void barClicked() {

        }

        @Override
        public void moreClicked() {

        }

        @Override
        public void cartClicked() {

        }
    }
}
