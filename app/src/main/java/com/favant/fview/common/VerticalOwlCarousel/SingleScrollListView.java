package com.favant.fview.common.VerticalOwlCarousel;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ListView;

/**
 * Created by Hung on 12/5/2015.
 */
public class SingleScrollListView extends ListView {

    private static final String TAG = "SingleScrollListView";
    public int scroll_time = 200;
    private float last_motion_y = -1;
    private int tmp_direction = 0;
    private int scroll_per_item = 1;
    private int last_first_visible_position = -1;
    private boolean lock_scroll;

    public SingleScrollListView(Context context) {
        super(context);
    }

    public SingleScrollListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SingleScrollListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setScrollPerItem(int spi) {
        scroll_per_item = spi;
    }

    public void setScrollTime(int st) {
        scroll_time = st;
    }

    public int getCurrentItem() {
        int current_item = getFirstVisiblePosition();
        Log.v(TAG, String.valueOf(getFirstVisiblePosition()));
        if (getChildCount() > 0 && getChildAt(0).getTop() < -50)
            current_item += 1;
        return current_item;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                last_first_visible_position = getCurrentItem();
                Log.v(TAG, "last first vis pos " + String.valueOf(last_first_visible_position));
                last_motion_y = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (lock_scroll) return false;
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_MOVE:
                if (last_motion_y != -1) {
                    if (ev.getY() < last_motion_y) tmp_direction = 1;
                    else if (ev.getY() > last_motion_y) tmp_direction = -1;
                }
                break;
            case MotionEvent.ACTION_UP:
                if (ev.getY() < last_motion_y || (ev.getY() == last_motion_y && tmp_direction == 1))
                    smoothScrollToPositionFromTop(last_first_visible_position + scroll_per_item, 0, scroll_time);
                else if (ev.getY() > last_motion_y || (ev.getY() == last_motion_y && tmp_direction == -1)) {
                    smoothScrollToPositionFromTop(last_first_visible_position - scroll_per_item, 0, scroll_time / 2);
                }
                Log.v(TAG, ev.getY() + " " + last_motion_y + " " + tmp_direction);
                last_motion_y = -1;
                last_first_visible_position = -1;
                return true;
        }
        return super.onTouchEvent(ev);
    }

    public void lockScroll() {
        lock_scroll = true;
    }

    public void unlockScroll() {
        lock_scroll = false;
    }
}
