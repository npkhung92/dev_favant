package com.favant.fview.common;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.favant.R;
import com.favant.util.MainHandler;
import com.favant.util.ViewChecker;

import java.util.ArrayList;

/**
 * Created by Hung on 10/26/2015.
 */
public class FPopup extends RelativeLayout {
    private static final String TAG = "FPopup";
    private final int ANIMATION_TIME = 250;
    private View page_view;
    private boolean ANIMATING_STATE = false;

    private ArrayList<PopupListener> listeners = new ArrayList<PopupListener>();
    public FPopup(Context context) {
        super(context);
        init(context, null);
    }

    public FPopup(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FPopup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }
    private void init(Context context, AttributeSet attrs) {
    }
    @Override
    public void onFinishInflate() {
        super.onFinishInflate();
        if (ViewChecker.validate(findViewById(R.id.popup_overlay), View.class)) {
            findViewById(R.id.popup_overlay).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    hide();
                }
            });
        }
        if (ViewChecker.validate(findViewById(R.id.ic_close), View.class)) {
            findViewById(R.id.ic_close).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    hide();
                }
            });
        }
    }

    public void setTitle(String text) {
        if (ViewChecker.validate(findViewById(R.id.popup_title), FTextView.class)) {
            FTextView tv = (FTextView) findViewById(R.id.popup_title);
            tv.setText(text);
        }
    }
    public void setDoneBtn(String text) {
        if (ViewChecker.validate(findViewById(R.id.popup_btn), FTextView.class)) {
            FButton btn = (FButton) findViewById(R.id.popup_btn);
            btn.setText(text);
        }
    }
    public void setContent(View v) {
        if (ViewChecker.validate(findViewById(R.id.popup_wrapper_content), ViewGroup.class)) {
            ViewGroup vg = (ViewGroup) findViewById(R.id.popup_wrapper_content);
            vg.removeAllViews();
            vg.addView(v);
        }
    }

    public void removeContent() {
        if (ViewChecker.validate(findViewById(R.id.popup_wrapper_content), ViewGroup.class)) {
            ViewGroup vg = (ViewGroup) findViewById(R.id.popup_wrapper_content);
            vg.removeAllViews();
        }
    }

    public void reset() {
        removeContent();
        if (ViewChecker.validate(findViewById(R.id.popup_title), FTextView.class)) {
            FTextView tv = (FTextView) findViewById(R.id.popup_title);
            tv.setText("");
        }
    }

    public boolean isShowing() {
        return getVisibility() == View.VISIBLE;
    }

    public void show() {
        if (ANIMATING_STATE) return;
        ANIMATING_STATE = true;
        MainHandler.post(new Runnable() {
            @Override
            public void run() {
                setVisibility(View.VISIBLE);
                bringToFront();
                if (ViewChecker.validate(findViewById(R.id.popup_box), ViewGroup.class)) {
                    ViewGroup vg = (ViewGroup) findViewById(R.id.popup_box);
                    float current_y = vg.getY();
//                    vg.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                    int height = vg.getMeasuredHeight();
                    vg.measure(0, 0);
                    ValueAnimator anim = ObjectAnimator.ofFloat(vg, "translationY", vg.getMeasuredHeight(), 0);
                    anim.setDuration(ANIMATION_TIME);
                    if (ViewChecker.validate(page_view, View.class)) {
                        ValueAnimator scaleX = ObjectAnimator.ofFloat(page_view, "scaleX", 1f, 0.95f)
                                .setDuration(ANIMATION_TIME);
                        ValueAnimator scaleY = ObjectAnimator.ofFloat(page_view, "scaleY", 1f, 0.95f)
                                .setDuration(ANIMATION_TIME);
                        ValueAnimator rotateX1 = ObjectAnimator.ofFloat(page_view, "rotationX", 0, 4)
                                .setDuration(30 * ANIMATION_TIME * 2 / 100);
                        ValueAnimator rotateX2 = ObjectAnimator.ofFloat(page_view, "rotationX", 4, 0)
                                .setDuration(70 * ANIMATION_TIME * 2 / 100);
                        AnimatorSet anset = new AnimatorSet();
                        anset.play(anim).with(scaleX).with(scaleY);
//                        anset.play(rotateX1).before(rotateX2);
                        anset.start();

                        anset.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                for (int i = 0; i < listeners.size(); i++) {
                                    PopupListener mrl = listeners.get(i);
                                    mrl.show();
                                }
                                ANIMATING_STATE = false;
                            }
                        });
                    }
                }
            }
        });
    }

    public void hide(final boolean reset) {
        if (ANIMATING_STATE) return;
        ANIMATING_STATE = true;
        MainHandler.post(new Runnable() {
            @Override
            public void run() {
                setVisibility(View.VISIBLE);
                if (ViewChecker.validate(findViewById(R.id.popup_box), ViewGroup.class)) {
                    final ViewGroup vg = (ViewGroup) findViewById(R.id.popup_box);
                    final float current_y = vg.getY();
                    vg.measure(0, 0);
                    ValueAnimator anim = ObjectAnimator.ofFloat(vg, "translationY", 0, vg.getMeasuredHeight());
                    anim.setDuration(ANIMATION_TIME);
                    anim.addListener(new AnimatorListenerAdapter() {
                        public void onAnimationEnd(Animator animation) {
                            if (reset) reset();
                            setVisibility(View.GONE);
                        }
                    });
                    if (ViewChecker.validate(page_view, View.class)) {
                        ValueAnimator scaleX = ObjectAnimator.ofFloat(page_view, "scaleX", 0.95f, 1f)
                                .setDuration(ANIMATION_TIME);
                        ValueAnimator scaleY = ObjectAnimator.ofFloat(page_view, "scaleY", 0.95f, 1f)
                                .setDuration(ANIMATION_TIME);
                        ValueAnimator rotateX1 = ObjectAnimator.ofFloat(page_view, "rotationX", 0, -4)
                                .setDuration(30 * ANIMATION_TIME * 2 / 100);
                        ValueAnimator rotateX2 = ObjectAnimator.ofFloat(page_view, "rotationX", -4, 0)
                                .setDuration(70 * ANIMATION_TIME * 2 / 100);
                        AnimatorSet anset = new AnimatorSet();
                        anset.play(anim).with(scaleX).with(scaleY);
//                        anset.play(rotateX1).before(rotateX2);
                        anset.start();

                        anset.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                for (int i = 0; i < listeners.size(); i++) {
                                    PopupListener mrl = listeners.get(i);
                                    mrl.hide();
                                }
                                ANIMATING_STATE = false;
                            }
                        });
                    }
                }
            }
        });
    }

    public void hide() {
        hide(true);
    }

    public void setPageView(View v) {
        page_view = v;
    }

    public void addPopupListener(PopupListener mrl) {
        if (mrl != null) listeners.add(mrl);
    }

    public void removePopupListener(PopupListener mrl) {
        if (mrl != null && listeners.contains(mrl)) listeners.remove(mrl);
    }

    public static interface PopupListener {
        public void show();

        public void hide();
    }
}
