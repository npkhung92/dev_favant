package com.favant.fview.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.favant.R;
import com.favant.util.ViewChecker;

/**
 * Created by Bear on 9/21/2015.
 */
public class FRating extends LinearLayout {


    private Context mContext;
    private float rating;

    public FRating(Context context) {
        super(context);
        init(context, null);
    }

    public FRating(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FRating(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        mContext = context;
        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.Favant, 0, 0);
            try {
                rating = a.getFloat(R.styleable.Favant_rating, 0);
            } finally {
                a.recycle();
            }
        }
        setOrientation(HORIZONTAL);
        initStar();
    }

    public void setRating(float rate) {
        rating = rate;
        initStar();
    }

    private void initStar() {
        LayoutInflater layout_inflater = LayoutInflater.from(mContext);
        removeAllViews();
        for (int i = 0; i < 5; i++) {
            ViewGroup star = (ViewGroup) layout_inflater.inflate(R.layout.star, this, false);
            if (i + 1 > rating) {
                float left = rating - i;
                if (ViewChecker.validate(star.findViewById(R.id.star_full), ImageView.class)) {
                    ImageView star_full = (ImageView) star.findViewById(R.id.star_full);
                    if (left <= 0) {
                        star_full.setVisibility(INVISIBLE);
                    } else {
                        Bitmap b = ((BitmapDrawable) star_full.getDrawable()).getBitmap();
                        left = left * 0.6f + 0.2f;
                        Bitmap new_b = Bitmap.createBitmap(b, 0, 0, (int) (b.getWidth() * left), b.getHeight());
                        star_full.setImageBitmap(new_b);
                    }
                }
            }
            addView(star);
        }
    }
}
