package com.favant.fview.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.favant.R;
import com.favant.util.ViewChecker;

/**
 * Created by Hung on 10/18/2015.
 */
public class FQuantity extends LinearLayout {
    private FEditText quantity_txt;
    private FImageView dec;
    private FImageView inc;
    private QuantityUpdateListener update_listener;
    public FQuantity(Context context) {
        super(context);
    }

    public FQuantity(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FQuantity(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        if (ViewChecker.validate(findViewById(R.id.quantity), FEditText.class)) {
            quantity_txt = (FEditText) findViewById(R.id.quantity);
            quantity_txt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    validateQuantityTxt();
                    return false;
                }
            });

            if (ViewChecker.validate(findViewById(R.id.quantity_wrapper), View.class)) {
                findViewById(R.id.quantity_wrapper).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        quantity_txt.requestFocus();
                        quantity_txt.setSelection(quantity_txt.getText().length());
                        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(quantity_txt, InputMethodManager.SHOW_IMPLICIT);
                    }
                });
            }
        }
        if (ViewChecker.validate(findViewById(R.id.quantity_dec), FImageView.class)) {
            dec = (FImageView) findViewById(R.id.quantity_dec);
            dec.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    int current_qty;
                    try {
                        current_qty = Integer.parseInt(quantity_txt.getText().toString());
                    } catch (Exception e) {
                        current_qty = 1;
                    }
                    if (current_qty > 1) {
                        current_qty--;
                        setQuantity(current_qty, true);
                    }
                }
            });
        }
        if (ViewChecker.validate(findViewById(R.id.quantity_inc), FImageView.class)) {
            inc = (FImageView) findViewById(R.id.quantity_inc);
            inc.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    int current_qty;
                    try {
                        current_qty = Integer.parseInt(quantity_txt.getText().toString());
                    } catch (Exception e) {
                        current_qty = 1;
                    }
                    current_qty++;
                    setQuantity(current_qty, true);
                }
            });
        }
    }

    private void validateQuantityTxt() {
        if (quantity_txt.getText().toString().equals("")) {
            quantity_txt.setText("1");
        }
        notifyUpdate();
    }

    public void setQuantity(int i, boolean notify) {
        if (i > 0 && quantity_txt != null) {
            quantity_txt.setText(String.valueOf(i));
            if (notify) notifyUpdate();
        }
    }

    public void disableButton() {
        if ((dec != null) && (inc != null)) {
            dec.setClickable(false);
            inc.setClickable(false);
            findViewById(R.id.quantity_wrapper).setClickable(false);
        }
    }

    private void notifyUpdate() {
        if (update_listener != null) {
            int current_qty;
            try {
                current_qty = Integer.parseInt(quantity_txt.getText().toString());
            } catch (Exception e) {
                current_qty = 1;
            }
            update_listener.update(current_qty);
        }
    }

    public void setUpdateListener(QuantityUpdateListener qul) {
        update_listener = qul;
    }

    @Override
    public boolean dispatchKeyEventPreIme(KeyEvent event) {
        if (getContext() != null && quantity_txt != null) {
            InputMethodManager imm = (InputMethodManager) getContext()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);

            if (imm.isActive() && event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                validateQuantityTxt();
            }
        }
        return super.dispatchKeyEventPreIme(event);
    }

    public static interface QuantityUpdateListener {
        public void update(int quantity);
    }
}
