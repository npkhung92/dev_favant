package com.favant.fview.common;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.favant.R;
import com.favant.model.ProductDetailModel;
import com.favant.util.ViewChecker;

/**
 * Created by Bear on 9/27/2015.
 */
public class FColorBox extends RelativeLayout {
    public FColorBox(Context context) {
        super(context);
    }

    public FColorBox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FColorBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setColor(ProductDetailModel.AttributeOption opt) {
        if (ViewChecker.validate(findViewById(R.id.color), ViewGroup.class)
                && ViewChecker.validate(findViewById(R.id.color_image), FImageView.class)) {
            ViewGroup color_wrapper = (ViewGroup) findViewById(R.id.color);
            FImageView color_image = (FImageView) findViewById(R.id.color_image);
            color_image.removeImage();
            if (opt.color_hex_rgb != null && !opt.color_hex_rgb.equals("")) {
                int c = 0;
                try {
                    if (!opt.color_hex_rgb.contains("#"))
                        c = Color.parseColor("#" + opt.color_hex_rgb);
                    else c = Color.parseColor(opt.color_hex_rgb);
                } catch (IllegalArgumentException e) {
                }
                color_wrapper.setBackgroundColor(c);
            } else if (opt.image != null && !opt.image.equals("")) {
                color_image.setImageUrl(opt.image, false);
            }
        }
    }
}
