package com.favant.fview.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.favant.R;
import com.favant.util.ViewChecker;

/**
 * Created by Hung on 10/24/2015.
 */
public class FRatingTable extends LinearLayout {
    private int[] stars = new int[]{0, 0, 0, 0, 0};
    private int star_sum = 0;

    public FRatingTable(Context context) {
        super(context);
    }

    public FRatingTable(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FRatingTable(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setStar(int star5, int star4, int star3, int star2, int star1) {
        stars[0] = star5;
        stars[1] = star4;
        stars[2] = star3;
        stars[3] = star2;
        stars[4] = star1;
        star_sum = star5 + star4 + star3 + star2 + star1;
    }

    @Override
    protected void onFinishInflate() {
        validate();
    }

    private void setStarProgress(int res_id_red, int res_id_grey, int star) {
        if (star_sum == 0) return;
        if (ViewChecker.validate(findViewById(res_id_red), View.class)
                && ViewChecker.validate(findViewById(res_id_grey), View.class)) {
            View progress_red = findViewById(res_id_red);
            View progress_grey = findViewById(res_id_grey);

            LinearLayout.LayoutParams lp = (LayoutParams) progress_red.getLayoutParams();
            float percent = star * 100 / star_sum;
            lp.weight = percent;
            progress_red.setLayoutParams(lp);

            LinearLayout.LayoutParams lp2 = (LayoutParams) progress_grey.getLayoutParams();
            lp2.weight = 100 - percent;
            progress_grey.setLayoutParams(lp2);
        }
    }

    public void validate() {
        if (ViewChecker.validate(findViewById(R.id.star5), FTextView.class)) {
            FTextView tv = (FTextView) findViewById(R.id.star5);
            tv.setText(String.valueOf(stars[0]));
            setStarProgress(R.id.star5_progress_red, R.id.star5_progress_grey, stars[0]);
        }
        if (ViewChecker.validate(findViewById(R.id.star4), FTextView.class)) {
            FTextView tv = (FTextView) findViewById(R.id.star4);
            tv.setText(String.valueOf(stars[1]));
            setStarProgress(R.id.star4_progress_red, R.id.star4_progress_grey, stars[1]);
        }
        if (ViewChecker.validate(findViewById(R.id.star3), FTextView.class)) {
            FTextView tv = (FTextView) findViewById(R.id.star3);
            tv.setText(String.valueOf(stars[2]));
            setStarProgress(R.id.star3_progress_red, R.id.star3_progress_grey, stars[2]);
        }
        if (ViewChecker.validate(findViewById(R.id.star2), FTextView.class)) {
            FTextView tv = (FTextView) findViewById(R.id.star2);
            tv.setText(String.valueOf(stars[3]));
            setStarProgress(R.id.star2_progress_red, R.id.star2_progress_grey, stars[3]);
        }
        if (ViewChecker.validate(findViewById(R.id.star1), FTextView.class)) {
            FTextView tv = (FTextView) findViewById(R.id.star1);
            tv.setText(String.valueOf(stars[4]));
            setStarProgress(R.id.star1_progress_red, R.id.star1_progress_grey, stars[4]);
        }
    }
}
