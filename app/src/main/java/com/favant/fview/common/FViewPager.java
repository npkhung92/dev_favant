package com.favant.fview.common;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Bear on 8/22/2015.
 */
public class FViewPager extends ViewPager {

    public boolean mScrollable = false;

    public FViewPager(Context context) {
        super(context);
    }

    public FViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (!mScrollable) return false;
        else return super.onTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (!mScrollable) return false;
        else return super.onInterceptTouchEvent(ev);
    }
}
