package com.favant.fview.common;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.favant.MasterActivity;
import com.favant.R;
import com.favant.fview.cart.CartPopup;
import com.favant.fview.common.LeftNavigator.FLeftNavigationDrawer;
import com.favant.router.Router;
import com.favant.router.RouterInterface;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Bear on 8/22/2015.
 */
public abstract class FFragment extends Fragment {
    protected int master_page;
    protected int content_page;
    protected ViewGroup container = null;
    protected View rootView = null;
    protected LayoutInflater mInflater;
    protected MasterActivity mMasterActivity;
    protected AtomicBoolean fragment_created;
    protected boolean show_loading_spinner = true;
    protected RouterInterface router;
    private Runnable afterLoginCallback;
    private int page_position;
    private CopyOnWriteArrayList<Runnable> onCreateObserver = new CopyOnWriteArrayList<Runnable>();
    private Activity mActivity;
    private FPopup popup;
    private CartPopup cartPopup;
    private FLeftNavigationDrawer left_navi_drawer;
    private FMenuRight menu_right;
    private ViewGroup content_container;
    private boolean onCreateViewCalled, loadContentCalled;
    private boolean call_load_content_after_on_create_view;
    private boolean invokeOnCreateCalled;
    private boolean call_invoke_ga_after_on_create_view = false;
    private View login_box;
    private View blur_view;
    private String fb_access_token;
    private String refererUrl;

    public FFragment() {
        fragment_created = new AtomicBoolean(false);
    }

    public void setLayout(int master_page, int content_page) {
        this.master_page = master_page;
        this.content_page = content_page;
    }

    public void setMasterActivity(MasterActivity ma) {
        mMasterActivity = ma;
    }

    public int getPagePosition() {
        return page_position;
    }

    public void setPagePosition(int ps) {
        page_position = ps;
    }

    @Override
    public final synchronized View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        onCreateViewCalled = true;
        if (rootView != null) {
            if (rootView.getParent() instanceof ViewGroup) {
                ((ViewGroup) rootView.getParent()).removeView(rootView);
                return rootView;
            }
        }
        ViewGroup rootView = (ViewGroup) inflater.inflate(master_page, container, false);
        this.mInflater = inflater;
        this.rootView = rootView;
        this.container = container;

        content_container = (ViewGroup) findViewById(R.id.content_container);
        left_navi_drawer = (FLeftNavigationDrawer) findViewById(R.id.left_navi_wrapper);
        final FActionBar ab = getFActionBar();
        if (ab != null) {
            ab.setFragment(this);
            ab.setMasterActivity(mMasterActivity);
            loadFActionBar(ab);
        }

        if (call_load_content_after_on_create_view) {
            loadContent();
            invokeOnCreate();
        }

        return rootView;
    }

    public void loadContent() {
        if (!onCreateViewCalled) {
            call_load_content_after_on_create_view = true;
            return;
        }
        if (loadContentCalled) return;
        loadContentCalled = true;
        if (content_container != null) {
            final FActionBar ab = getFActionBar();
            if (ab != null) {
                ab.setContentContainer(content_container);
            }
            View v = mInflater.inflate(content_page, content_container, false);
            content_container.addView(v);
        }
    }

    protected FActionBar getFActionBar() {
        View action_bar = findViewById(R.id.faction_bar);
        if (action_bar != null && action_bar instanceof FActionBar) {
            FActionBar actionBar = (FActionBar) action_bar;
            actionBar.setRouter(router);
            return actionBar;
        }
        return null;
    }

    protected FPopup getPopup() {
        return popup;
    }

    protected CartPopup getCartPopup() {
        return cartPopup;
    }

    protected FMenuRight getMenuRight() {
        return menu_right;
    }

    public void setMenuRight(FMenuRight menu_right) {
        this.menu_right = menu_right;
    }

//    protected FLeftNavigationDrawer getLeftNavigationDrawer(){
//        return left_navi_drawer;
//    }
//
//    public void setLeftNavigationDrawer(FLeftNavigationDrawer leftNavigationDrawer){
//        this.left_navi_drawer = leftNavigationDrawer;
//    }

    public void invokeOnCreate() {
        if (!onCreateViewCalled) {
            call_load_content_after_on_create_view = true;
            return;
        }
        if (invokeOnCreateCalled) return;
        invokeOnCreateCalled = true;
        onPreCreate();
        onCreate();
        for (Runnable r : onCreateObserver) r.run();

        fragment_created.set(true);
    }

    protected void addOnCreateObserver(Runnable r) {
        onCreateObserver.add(r);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mActivity = activity;
    }

    protected View findViewById(int id) {
        if (rootView == null) return null;
        return rootView.findViewById(id);
    }

    protected abstract void loadFActionBar(FActionBar ab);

    protected abstract void onPreCreate();

    protected abstract void onCreate();

    public boolean handleBackPressed() {
        FPopup popup = getPopup();
        if (popup != null && popup.isShowing()) {
            popup.hide();
            return true;
        }
        FLeftNavigationDrawer leftNavigationDrawer = getLeftNavDrawer();
        if (leftNavigationDrawer != null && leftNavigationDrawer.isShowing()) {
            leftNavigationDrawer.hide();
            return true;
        }
        FMenuRight menuRight = getMenuRight();
        if (menuRight != null && menuRight.isShowing()) {
            menuRight.hide();
            return true;
        }
        return false;
    }

    protected FLeftNavigationDrawer getLeftNavDrawer() {
        return left_navi_drawer;
    }

    public void setLeftNavDrawer(FLeftNavigationDrawer fnd) {
        this.left_navi_drawer = fnd;
    }

    protected Context getContext() {
        return mActivity;
    }

//    public void webServiceGetJson(String key, final Net.JsonResult callback) {
//        WebService.getJson(key, new Net.JsonResult() {
//            @Override
//            public void success(Object res) {
//                if (getContext() != null && mMasterActivity != null && rootView != null && callback != null)
//                    callback.success(res);
//            }
//
//            @Override
//            public void error(Exception e) {
//
//            }
//        });
//    }
//
//    public void webServicePostJson(String key, List<NameValuePair> params, final Net.JsonResult callback) {
//        WebService.postJson(key, params, new Net.JsonResult() {
//            @Override
//            public void success(Object res) {
//                if (getContext() != null && mMasterActivity != null && rootView != null && callback != null)
//                    callback.success(res);
//            }
//
//            @Override
//            public void error(Exception e) {
//
//            }
//        });
//    }

    public void hide() {

    }

    public void show() {

    }

    public void resume() {

    }

    public void initKeyboard() {
        mMasterActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    public void setPopupWrapper(FPopup popup) {
        this.popup = popup;
    }

    public void loadCartPopup() {
        cartPopup = new CartPopup(getContext(), mMasterActivity);
        cartPopup.loadPopup();
        cartPopup.loadCartData();
    }

    public void showCartPopup() {
        FPopup popup = getPopup();
        popup.setContent(cartPopup.getPopupContent());
        popup.setTitle("Cart");
        popup.show();
    }

    public void goBackAction(Bundle data) {
        String login = data.getString("login", "");
        if (login.equals("login_success")) {

            if (afterLoginCallback != null) afterLoginCallback.run();
        }
        afterLoginCallback = null;
    }

    public void setAfterLoginCallback(Runnable afterLoginCallback) {
        this.afterLoginCallback = afterLoginCallback;
    }

    public void setRouter(Router router) {
        this.router = router;
    }
}
