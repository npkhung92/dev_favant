package com.favant.fview.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.RelativeLayout;

import com.favant.MasterActivity;
import com.favant.R;
import com.favant.util.DpPx;
import com.favant.util.MainHandler;

/**
 * Created by Bear on 10/3/2015.
 */
public class FEditTextWrapper extends RelativeLayout {
    private static final int MAX_HEIGHT = 30;
    private FImageView close_icon;
    private FEditText editText;
    private boolean is_password;
    private FImageView show_password;
    private Runnable onClearCallback;

    public FEditTextWrapper(Context context) {
        super(context);
        init(null);
    }

    public FEditTextWrapper(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public FEditTextWrapper(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (!isInEditMode()) {
            if (attrs != null) {
                TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.Favant, 0, 0);
                try {
                    is_password = a.getBoolean(R.styleable.Favant_is_password, false);
                } finally {
                    a.recycle();
                }
            }
        }
    }

    @Override
    protected void onFinishInflate() {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                MasterActivity ma = MasterActivity.getInstance();
//                if (ma != null && ma.isScrolling()) {
//                    MainHandler.postDelay(this, MasterActivity.ANIMATION_DURATION);
//                    return;
//                }
                close_icon = new FImageView(getContext());
//                close_icon.setId(R.id.close);
                measure(0, 0);
                int height = getMeasuredHeight();
                if (height > DpPx.dpToPx(getContext(), MAX_HEIGHT))
                    height = (int) DpPx.dpToPx(getContext(), MAX_HEIGHT);
                RelativeLayout.LayoutParams lp = new LayoutParams(height, height);
                lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                lp.addRule(RelativeLayout.CENTER_VERTICAL);
                if (is_password) lp.rightMargin = height + (int) DpPx.dpToPx(getContext(), 4);
                else lp.rightMargin = (int) DpPx.dpToPx(getContext(), 2);

                close_icon.setLayoutParams(lp);
                close_icon.setImageResource(R.drawable.ic_close_edittext);

                close_icon.setClickable(true);

                close_icon.setVisibility(View.GONE);

                close_icon.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (editText != null) {
                            editText.setText("");
                            if (onClearCallback != null) onClearCallback.run();
                        }
                    }
                });

                addView(close_icon);

                if (is_password) {
                    show_password = new FImageView(getContext());
                    RelativeLayout.LayoutParams lp3 = new LayoutParams(height, height);
                    lp3.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    lp3.addRule(RelativeLayout.CENTER_VERTICAL);
                    lp3.rightMargin = (int) DpPx.dpToPx(getContext(), 2);

                    show_password.setLayoutParams(lp3);
                    show_password.setImageResource(R.drawable.ic_view_password);

                    show_password.setClickable(true);

                    show_password.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (editText.getTransformationMethod() == null) {
                                editText.setTransformationMethod(new PasswordTransformationMethod());
                                show_password.setImageResource(R.drawable.ic_view_password);
                            } else {
                                editText.setTransformationMethod(null);
                                show_password.setImageResource(R.drawable.ic_view_password_red);
                            }
                            editText.setSelection(editText.length());
                        }
                    });

                    addView(show_password);
                }

                if (getChildCount() > 0 && getChildAt(0) instanceof FEditText) {
                    editText = (FEditText) getChildAt(0);
                    RelativeLayout.LayoutParams lp2 = (LayoutParams) editText.getLayoutParams();
                    lp2.rightMargin = getMeasuredHeight() + (int) DpPx.dpToPx(getContext(), 2) + (is_password ? getMeasuredHeight() + (int) DpPx.dpToPx(getContext(), 2) : 0);

                    editText.setLayoutParams(lp2);

                    editText.addOnEditTextImeBackListener(new FEditText.EditTextImeBackListener() {
                        @Override
                        public boolean onImeBack() {
                            hideCloseIcon();
                            return false;
                        }
                    });
                }

                if (editText != null) {
                    editText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                            if (charSequence.toString().equals("")) {
                                hideCloseIcon();
                            } else if (editText.isFocused()) {
                                showCloseIcon();
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });

                    editText.setOnFocusChangeListener(new OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View view, boolean b) {
                            if (!b) {
                                hideCloseIcon();
                            } else if (!editText.getText().toString().equals("")) {
                                showCloseIcon();
                            }
                        }
                    });

                    editText.addOnEditorActionListener(new FEditText.SEditorAction() {
                        @Override
                        public void onEditorAction(int actionCode) {
                            if (actionCode == EditorInfo.IME_ACTION_NEXT
                                    || actionCode == EditorInfo.IME_ACTION_DONE
                                    || actionCode == EditorInfo.IME_ACTION_SEARCH
                                    || actionCode == EditorInfo.IME_ACTION_SEND) {
                                hideCloseIcon();
                            }
                        }
                    });
                }
            }
        };
        super.onFinishInflate();
        MainHandler.postDelay(r, MasterActivity.ANIMATION_DURATION * 2);
    }

    private void showCloseIcon() {
        close_icon.setVisibility(View.VISIBLE);
    }

    private void hideCloseIcon() {
        close_icon.setVisibility(View.GONE);
    }

    public void setOnClearCallback(Runnable onClearCallback) {
        this.onClearCallback = onClearCallback;
    }
}
