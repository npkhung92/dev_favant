package com.favant.fview.common;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * Created by Bear on 8/30/2015.
 */
public class FListView extends ListView {
    public FListView(Context context) {
        super(context);
    }

    public FListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
