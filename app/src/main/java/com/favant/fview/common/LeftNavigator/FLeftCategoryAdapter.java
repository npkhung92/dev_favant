package com.favant.fview.common.LeftNavigator;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.favant.R;
import com.favant.fview.common.FTextView;
import com.favant.model.Category;
import com.favant.model.FNavigationItem;

import java.util.ArrayList;

/**
 * Created by Hung on 1/15/2016.
 */
public class FLeftCategoryAdapter extends BaseAdapter {
    private final static int VIEW_TYPE_HEADER = 0;
    private final static int VIEW_TYPE_ITEM = 1;
    private final static int VIEW_TYPE_COUNT = 2;
    private Context context;
    private final static String TAG = "FLeftCategoryAdapter";
    private ArrayList<ViewData> categories = new ArrayList<ViewData>();
    public FLeftCategoryAdapter(Context context) {
        this.context = context;
    }
    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        return categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        return ((ViewData) getItem(position)).view_type;
    }



    public void addItem(Category cate){
        this.categories.add(new ViewData(VIEW_TYPE_ITEM, cate));
        Log.v(TAG, String.valueOf(cate));
    }
    public void addHeader(Category cate){
        this.categories.add(new ViewData(VIEW_TYPE_HEADER, cate));
        Log.v(TAG, String.valueOf(cate));
    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {

        if (getItemViewType(position) == VIEW_TYPE_HEADER){
            view = inflateHeader(position, view, parent);
        }
        else if (getItemViewType(position) == VIEW_TYPE_ITEM){
            view = inflateItem(position, view, parent);
        }
        return view;
    }

    private View inflateHeader(int position, View view, ViewGroup parent) {
        if (view == null){

            view = LayoutInflater.from(context).inflate(R.layout.category_header, null);
            ViewHolder headerViewHolder = new ViewHolder();
            headerViewHolder.name = (FTextView) view.findViewById(R.id.header);
            view.setTag(headerViewHolder);
        }
        final Category category = ((ViewData) getItem(position)).category;
        if (category != null) {
            ViewHolder headerViewHolder = (ViewHolder)view.getTag();
            headerViewHolder.name.setText(category.name);
        }
        return view;
    }private View inflateItem(int position, View view, ViewGroup parent) {
        if (view == null){
            view = LayoutInflater.from(context).inflate(R.layout.category_item, null);
            ViewHolder headerViewHolder = new ViewHolder();
            headerViewHolder.name = (FTextView) view.findViewById(R.id.header);
            view.setTag(headerViewHolder);
        }
        final Category category = ((ViewData) getItem(position)).category;
        if (category != null) {
            ViewHolder headerViewHolder = (ViewHolder)view.getTag();
            headerViewHolder.name.setText(category.name);
        }
        return view;
    }
    private static class ViewHolder {
        public FTextView name;
    }

    private class ViewData {
        public int view_type;
        public Category category;

        public ViewData(int vt, Category cate) {
            view_type = vt;
            category = cate;
        }
    }
}
