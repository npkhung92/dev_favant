package com.favant.fview.common.LeftNavigator;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.favant.R;
import com.favant.fview.common.FImageView;
import com.favant.fview.common.FTextView;
import com.favant.model.FNavigationItem;

import java.util.ArrayList;

/**
 * Created by Hung on 10/27/2015.
 */
public class FLeftNavDrawAdapter extends BaseAdapter {
    private final static int VIEW_TYPE_HEADER = 0;
    private final static int VIEW_TYPE_ITEM = 1;
    private final static int VIEW_TYPE_COUNT = 2;
    private final static String TAG = "FLeftNavDrawAdapter";
    private ArrayList<ViewData> items = new ArrayList<ViewData>();
    private Context context;
    private FLeftNavigationDrawer navigationDrawer;

    public FLeftNavDrawAdapter(Context context, FLeftNavigationDrawer nav_drawer) {
        this.context = context;
        navigationDrawer = nav_drawer;
    }

    public void addItem(FNavigationItem.NavigationObject itm){
        this.items.add(new ViewData(VIEW_TYPE_ITEM, itm));
        Log.v(TAG, String.valueOf(items));
    }

    public void addHeader(FNavigationItem.NavigationObject itm){
        this.items.add(new ViewData(VIEW_TYPE_HEADER, itm));
        Log.v(TAG, String.valueOf(items));
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        return ((ViewData) getItem(position)).view_type;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (getItemViewType(position) == VIEW_TYPE_HEADER){
            view = inflateHeader(position, view, parent);
        }
        else if (getItemViewType(position) == VIEW_TYPE_ITEM){
            view = inflateItem(position, view, parent);
        }
        return view;
    }

    private View inflateHeader(int position, View view, ViewGroup parent) {
        if (view == null){

            view = LayoutInflater.from(context).inflate(R.layout.left_navigation_header, null);
            HeaderViewHolder headerViewHolder = new HeaderViewHolder();
            headerViewHolder.name = (FTextView) view.findViewById(R.id.header);
            view.setTag(headerViewHolder);
        }
        final FNavigationItem.NavigationObject navigationObject = ((ViewData) getItem(position)).nav_obj;
        if (navigationObject != null) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder)view.getTag();
            headerViewHolder.name.setText(navigationObject.name);
        }
        return view;
    }

    private View inflateItem(int position, View view, ViewGroup parent) {
        if (view == null){
            view = LayoutInflater.from(context).inflate(R.layout.left_navigation_component, null);
            ComponentViewHolder componentViewHolder = new ComponentViewHolder();
            componentViewHolder.name = (FTextView) view.findViewById(R.id.name);
            componentViewHolder.next = (FImageView) view.findViewById(R.id.next_ic);
            componentViewHolder.flag = (FImageView) view.findViewById(R.id.flag);
            view.setTag(componentViewHolder);
        }
        final FNavigationItem.NavigationObject navigationObject = ((ViewData) getItem(position)).nav_obj;
        if (navigationObject != null) {

            ComponentViewHolder componentViewHolder = (ComponentViewHolder) view.getTag();
            componentViewHolder.name.setText(navigationObject.name);
            if (navigationObject.flag_id == 0) {
                componentViewHolder.next.setVisibility(View.VISIBLE);
                componentViewHolder.flag.setVisibility(View.GONE);
            }
            else {
                componentViewHolder.next.setVisibility(View.GONE);
                componentViewHolder.flag.setImageResource(navigationObject.flag_id);
                componentViewHolder.flag.setVisibility(View.VISIBLE);
            }
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigationDrawer.addSubCateItems();
                }
            });
        }
        return view;
    }

    private static class ComponentViewHolder {
        public FTextView name;
        public FImageView next;
        public FImageView flag;
    }
    private static class HeaderViewHolder {
        public FTextView name;
    }

    private class ViewData {
        public int view_type;
        public FNavigationItem.NavigationObject nav_obj;

        public ViewData(int vt, FNavigationItem.NavigationObject no) {
            view_type = vt;
            nav_obj = no;
        }
    }

    public void clear(){
        items.clear();
    }
}
