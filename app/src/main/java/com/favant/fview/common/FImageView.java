package com.favant.fview.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.favant.R;
import com.favant.util.Media3Resizer;
import com.favant.util.OkHttpImageDownloader;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.squareup.okhttp.OkHttpClient;

/**
 * Created by Bear on 8/22/2015.
 */
public class FImageView extends ImageView {
    private static boolean DEBUG = false;
//    private static String TAG = "Sendo_SImageView";
    public boolean have_image = false;
    private Bitmap bitmap;
    private String img_url = "";
    private float ratio_w = 0;
    private float ratio_h = 0;
    private boolean auto_scale_media3 = true;
    private boolean auto_load = true;
    private Drawable src;
    private String media3_url = "";
    private ImageLoadingListener mLastImageLoadingListener = null;
    private boolean cancel_request_layout = false;
    private DisplayImageOptions custom_option;

    public FImageView(Context context) {
        super(context);
        init(context, null);
    }

    public FImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public static void initImageLoader(Context context) {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(options)
                .imageDownloader(new OkHttpImageDownloader(context, new OkHttpClient()))
                .threadPoolSize(3)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024) // 50 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();
        ImageLoader.getInstance().init(config);
    }

    private void init(Context context, AttributeSet attrs) {
        if (!isInEditMode()) {
            if (attrs != null) {
                TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.Favant, 0, 0);
                try {
                    ratio_w = a.getFloat(R.styleable.Favant_image_ratio_w, 0);
                    ratio_h = a.getFloat(R.styleable.Favant_image_ratio_h, 0);
                } finally {
                    a.recycle();
                }
            }
            this.addOnAttachStateChangeListener(new OnAttachStateChangeListener() {
                @Override
                public void onViewAttachedToWindow(View v) {
//                loadImage();
                }

                @Override
                public void onViewDetachedFromWindow(View v) {
//                removeImage();
                }
            });
        }
    }

    public void removeImage() {
        img_url = "";
        media3_url = "";
        setImageBitmap(null);
    }

    private void imageLoader() {
        if (custom_option == null)
            ImageLoader.getInstance().displayImage(media3_url, FImageView.this);
        else ImageLoader.getInstance().displayImage(media3_url, FImageView.this, custom_option);
    }

    private void loadImage(final Runnable callback) {
        int width = getWidth();
        int height = getHeight();
        if ((width != 0 && height != 0) || !auto_scale_media3) {
            have_image = true;
            if (auto_scale_media3)
                media3_url = Media3Resizer.getResizedUrl(getContext(), new Media3Resizer.ResizeOption(img_url, width, height));
            else
                media3_url = Media3Resizer.validateUrl(img_url);

            imageLoader();
        }
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        cancel_request_layout = true;
        super.setImageBitmap(bm);
        cancel_request_layout = false;
    }

    @Override
    public void requestLayout() {
        if (cancel_request_layout) return;
        super.requestLayout();
    }

    public void hideImage() {
//        have_image = false;
//        setImageBitmap(null);
    }

    public void loadImage() {
        loadImage(null);
    }

    public void setAutoScaleMedia3(boolean ats) {
        auto_scale_media3 = ats;
    }

    public void setRatioW(float w) {
        ratio_w = w;
    }

    public void setRatioH(float h) {
        ratio_h = h;
    }

    public void setImageUrl(String url, boolean auto_scale_media3) {
        if (url.equals(img_url) && auto_scale_media3 == this.auto_scale_media3) return;
        this.auto_scale_media3 = auto_scale_media3;
        img_url = url;
        if (auto_load) loadImage();
    }

    public String getImageUrl() {
        return img_url;
    }

    public void setImageUrl(String url) {
        setImageUrl(url, auto_scale_media3);
//        setImageUrl(url, false);
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (img_url != null && !img_url.equals("") && auto_load) {
            loadImage();
        }
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int w = MeasureSpec.getSize(widthMeasureSpec);
        int h = MeasureSpec.getSize(heightMeasureSpec);
        if (ratio_w != 0 && ratio_h != 0 /*&& h != 0*/) {
            if (h == 0 || Math.abs(w / h - ratio_w / ratio_h) > 0.01) {
                int new_height = (int) (w * ratio_h / ratio_w);
                setMeasuredDimension(w, new_height);
                return;
            }
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setAutoload(boolean at) {
        auto_load = at;
    }

    public void setCustomImageLoaderOption(DisplayImageOptions option) {
        custom_option = option;
    }
}
