package com.favant.fview.common;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.favant.R;

/**
 * Created by Bear on 8/22/2015.
 */
public abstract class FActivity extends FragmentActivity {
    private int master_page, content_page;

    protected void setLayout(int master_page, int content_page) {
        this.master_page = master_page;
        this.content_page = content_page;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(master_page);

        FActionBar ab = getFActionBar();
        if (ab != null) {
            loadFActionBar(ab);
        }

        final ViewGroup content_container = (ViewGroup) findViewById(R.id.content_container);

        if (content_container != null) {
            View v = LayoutInflater.from(this).inflate(content_page, content_container, false);
            content_container.addView(v);
        }
    }

    protected abstract void loadFActionBar(FActionBar ab);

    protected FActionBar getFActionBar() {
        View action_bar = findViewById(R.id.faction_bar);
        if (action_bar != null && action_bar instanceof FActionBar) return (FActionBar) action_bar;
        return null;
    }

}
