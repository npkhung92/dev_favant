package com.favant.fview.common;

/**
 * Created by Bear on 8/22/2015.
 */
public interface OnScrollChangedListener {
    public void onScrollChanged();

    public void onScrollEnd();
}
