package com.favant.fview.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.favant.MasterActivity;
import com.favant.R;
import com.favant.model.ListingProductModel;
import com.favant.model.PageOption;
import com.favant.router.Router;
import com.favant.router.RouterInterface;
import com.favant.util.Currency;
import com.favant.util.ViewChecker;

/**
 * Created by Bear on 8/29/2015.
 */
public class ListingProductBox extends RelativeLayout implements View.OnClickListener {
    private Context context;
    private int listing_type = -1;
    private FImageView product_image;
    private FTextView product_name;
    private FTextView pro_final_price;
    private FTextView pro_price;
    private ListingProductModel.DataChangeListener current_data_changed_listener;
    private boolean updated_product_info = false;

    private ListingProductModel product_model = new ListingProductModel();

    public ListingProductBox(Context context) {
        super(context);
        init(context, null);
    }

    public ListingProductBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ListingProductBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void init(Context context, AttributeSet attrs) {
        this.context = context;
        setClickable(true);
        setOnClickListener(this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

//        if (ViewChecker.validate(findViewById(R.id.buynow), Button.class)) {
//            buynow_btn = (Button) findViewById(R.id.buynow);
//        }
//
//        if (ViewChecker.validate(findViewById(R.id.view_count), STextView.class)
//                && ViewChecker.validate(findViewById(R.id.view_count_icon), View.class)) {
//            view_count_txt = (STextView) findViewById(R.id.view_count);
//            view_count_icon = findViewById(R.id.view_count_icon);
//        }
//
        product_image = (FImageView) findViewById(R.id.pro_image);
//        product_image.setCustomImageLoaderOption(new DisplayImageOptions.Builder()
//                .cacheOnDisk(true)
////                .showImageOnLoading(R.drawable.image_preload)
////                .considerExifParams(true)
//                .bitmapConfig(Bitmap.Config.RGB_565)
//                .imageScaleType(ImageScaleType.EXACTLY)
//                .displayer(new FadeInBitmapDisplayer(300))
//                .build());
        if (ViewChecker.validate(findViewById(R.id.pro_name), FTextView.class)) {
            product_name = (FTextView) findViewById(R.id.pro_name);
        }
        pro_final_price = (FTextView) findViewById(R.id.pro_final_price);
        pro_price = (FTextView) findViewById(R.id.pro_price);
        pro_price.setPaintFlags(pro_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//        pro_promotion_percent = (STextView) findViewById(R.id.pro_promotion_percent);
//        product_free_shipping_img = (SImageView) findViewById(R.id.shipping_car);
//        if (ViewChecker.validate(findViewById(R.id.shop_name), STextView.class)) {
//            shop_name = (STextView) findViewById(R.id.shop_name);
//        }
//        if (ViewChecker.validate(findViewById(R.id.certified_shop), LinearLayout.class)) {
//            shop_certified = (LinearLayout) findViewById(R.id.certified_shop);
//        }
//        order_count = (STextView) findViewById(R.id.order_count);
//
//        promotion_app = (STextView) findViewById(R.id.promotion_app);
//        promotion_app_wrapper = (ViewGroup) findViewById(R.id.promotion_app_wrapper);
    }

    public ListingProductModel getListingProductModel() {
        return product_model;
    }

    public void setListingProductModel(final ListingProductModel pro_model) {
        if (product_model != null && current_data_changed_listener != null) {
            product_model.removeOnDataChangedListener(current_data_changed_listener);
            current_data_changed_listener = null;
        }
        product_model = pro_model;
        updated_product_info = false;
        updateDataProductModel(pro_model);
        current_data_changed_listener = new ListingProductModel.DataChangeListener() {
            @Override
            public void dataChanged() {
                updateDataProductModel(pro_model);
            }
        };
        pro_model.addOnDataChangedListener(current_data_changed_listener);
    }

    public void setImage(Bitmap b) {
        product_image.setImageBitmap(b);
    }

    public void setImageUrl(String url, Runnable cb) {
//        if (!url.equals(product_image.getImageUrl())) {
        if (!url.equals("http://test.favant.com/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/3/1/31901101B.jpg")) {
            product_image.removeImage();
            product_image.setImageUrl(url, false);
        }
    }

    public void setName(String name) {
        if (product_name != null) product_name.setText(name);
    }

    public void setFinalPrice(float price) {
//        pro_final_price.setText(Currency.formatCurrency(price) + "đ");
    }

    public void setPrice(Float price) {
        if (price == null) pro_price.setVisibility(View.INVISIBLE);
        else {
            pro_price.setVisibility(View.VISIBLE);
            pro_price.setText(Currency.formatCurrency(price) + "đ");
//            pro_price.setText(price);
        }
    }

    public int getListingType() {
        return listing_type;
    }

    public void setListingType(int lt) {
        listing_type = lt;
    }

    private void updateDataProductModel(ListingProductModel pro_model) {
        if (!updated_product_info) {
            updated_product_info = true;
            setImageUrl(pro_model.getImgUrl(), null);
//            setImageUrl("http://test.favant.com/media/catalog/product/cache/1/thumbnail/9df78eab33525d08d6e5fb8d27136e95/3/1/31901101B.jpg", null);
            setName(pro_model.getName());
            setPrice(pro_model.getFinalPrice());
//            setName("Test");
//            setPrice("10000d");
//            setFinalPrice(10000f);
//            if (pro_model.special_price == 0 || pro_model.is_promotion == 0) {
//                setFinalPrice(pro_model.price);
//                setPrice(0);
//                setPrice("");
//            } else {
//                setFinalPrice(pro_model.special_price);
//                setPrice(pro_model.price);
//            }
//            setPromotionPercent(pro_model.promotion_percent);
//            setFreeShipping(pro_model.free_shipping);
//        }

//        setShopName(product_model.merchant.name);
//        setShopCertified(product_model.merchant.is_certified);
//        setPromotionApp(product_model.merchant.promotion_app);
//        setOrderCount(product_model.order_count);

//        if (view_count_txt != null) setViewCount(pro_model.view_count);
//        if (buynow_btn != null) updateBuyNowButton(pro_model.isInStock());
        }
    }

    public FImageView getProduct_image() {
        return product_image;
    }

    public FTextView getProduct_name() {
        return product_name;
    }

    public FTextView getPro_final_price() {
        return pro_final_price;
    }

    public FTextView getPro_price() {
        return pro_price;
    }

    @Override
    public void onClick(View v) {

        MasterActivity mMasterAcitivity = MasterActivity.getInstance();
        RouterInterface router = mMasterAcitivity.getRouter();
        if (mMasterAcitivity != null) {
            router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_DETAIL).setData(null));
        }
    }
}
