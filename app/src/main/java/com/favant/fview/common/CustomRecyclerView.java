package com.favant.fview.common;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by Hung on 10/31/2015.
 */
public class CustomRecyclerView extends RecyclerView {
    private static final String TAG = "CustomRecyclerView";
    public int scroll_time = 200;
    private float last_motion_x = -1;
    private int tmp_direction = 0;
    private int scroll_per_item = 1;
    private int last_first_visible_position = -1;
    private boolean lock_scroll;
    private CustomLayoutManager llm;
    public CustomRecyclerView(Context context) {
        super(context);
    }

    public CustomRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void initLM(){
        llm = (CustomLayoutManager) this.getLayoutManager();
    }

    public int getCurrentItem() {
        initLM();
//        llm = (OwlCarousel3.CustomLayoutManager) getLayoutManager();
        int current_item = llm.findFirstVisibleItemPosition();
        Log.v(TAG, String.valueOf(llm.findFirstVisibleItemPosition()));
        if (getChildCount() > 0 && getChildAt(0).getLeft() < -50)
            current_item += 1;
        return current_item;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        initLM();
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                last_first_visible_position = llm.getCurrentItemPosition(this);
                last_motion_x = ev.getX();
                break;
            case MotionEvent.ACTION_MOVE:
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        if (lock_scroll) return false;
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_MOVE:
                if (last_motion_x != -1) {
                    if (ev.getX() < last_motion_x) tmp_direction = 1;
                    else if (ev.getX() > last_motion_x) tmp_direction = -1;
                }
                break;
            case MotionEvent.ACTION_UP:

                if (ev.getX() < last_motion_x || (ev.getX() == last_motion_x && tmp_direction == 1)) {
//                    smoothScrollToPositionFromLeft(last_first_visible_position + scroll_per_item, 0, scroll_time);
//                    scrollToPosition(last_first_visible_position + scroll_per_item);
                    smoothScrollToPosition(last_first_visible_position + scroll_per_item);
                    Log.v(TAG, "last first vis pos " + String.valueOf(last_first_visible_position));
                }
                else if (ev.getX() > last_motion_x || (ev.getX() == last_motion_x && tmp_direction == -1)) {
//                    smoothScrollToPositionFromLeft(last_first_visible_position - scroll_per_item, 0, scroll_time / 2);
//                    scrollToPosition(last_first_visible_position - scroll_per_item);
                    smoothScrollToPosition(last_first_visible_position - scroll_per_item);
                    Log.v(TAG, "last first vis pos " + String.valueOf(last_first_visible_position));
                }
                Log.v(TAG, ev.getX() + " " + last_motion_x + " " + tmp_direction);
                last_motion_x = -1;
                last_first_visible_position = -1;
                return true;
        }
        return super.onTouchEvent(ev);
    }
}
