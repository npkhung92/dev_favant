package com.favant.fview.common;

import android.content.Context;
import android.graphics.PointF;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

/**
 * Created by Hung on 10/31/2015.
 */
public class CustomLayoutManager extends LinearLayoutManager {
    private int duration;
    private static final String TAG = "LayoutManager";
    private Context c;
    public CustomLayoutManager(Context context, int duration) {
        super(context);
        this.duration = duration;
        c = context;
    }

    public CustomLayoutManager(Context context, int orientation, boolean reverseLayout, int duration) {
        super(context, orientation, reverseLayout);
        this.duration = duration;
        c = context;
    }

    @Override
    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position)
    {

//        View firstChild = recyclerView.getChildAt(0);
//        int itemWidth = firstChild.getWidth();
//        int distance = Math.abs((getCurrentItemPosition(recyclerView) - position) * itemWidth);
//        if (distance == 0) {
//            distance = (int) Math.abs(firstChild.getLeft());
//            Log.v(TAG, String.valueOf(firstChild.getLeft()));
//        }
//
//        WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
//        Display display = wm.getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int width = size.x;
//        CustomSmoothScroller smoothScroller = new CustomSmoothScroller(c, distance, duration);
//        Log.v(TAG, "số item " + recyclerView.getChildCount());
//        Log.v(TAG, "distance " + String.valueOf(width) + " " + String.valueOf(getCurrentItemPosition(recyclerView)) + String.valueOf(position));
//        smoothScroller.setTargetPosition(position);
//
//        startSmoothScroll(smoothScroller);
//        this.scrollToPosition(position);
        super.smoothScrollToPosition(recyclerView, state, position);
        Log.v(TAG, "current" + String.valueOf(getCurrentItemPosition(recyclerView)));
    }

    private class CustomSmoothScroller extends LinearSmoothScroller {
        private static final int TARGET_SEEK_SCROLL_DISTANCE_PX = 2000;
        private float distanceInPixels;
        private float duration;

        public CustomSmoothScroller(Context context, int distanceInPixels, int duration) {
            super(context);
            this.distanceInPixels = distanceInPixels;
            float millisecondsPerPx = calculateSpeedPerPixel(context.getResources().getDisplayMetrics());
            this.duration = distanceInPixels < TARGET_SEEK_SCROLL_DISTANCE_PX ?
                    (int) (Math.abs(distanceInPixels) * millisecondsPerPx) : duration;
        }

        @Override
        public PointF computeScrollVectorForPosition(int targetPosition) {
            return this.computeScrollVectorForPosition(targetPosition);
        }

        @Override
        public int calculateTimeForScrolling(int dx) {
            float proportion = (float) dx / distanceInPixels;
            return (int) (duration * proportion);
        }
    }

    public int getCurrentItemPosition(RecyclerView recyclerView){
        int current_item = findFirstVisibleItemPosition();
//        if (recyclerView.getChildCount() > 0 && recyclerView.getChildAt(0).getLeft() < -50)
//            current_item +=1;
        return current_item;
    }
}
