package com.favant.fview.common;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.favant.R;
import com.favant.util.DpPx;

/**
 * Created by Hung on 11/15/2015.
 */
public class PullToRefresh extends RelativeLayout {
    private static final String TAG = "PullToRefresh";
    private View firstChild;
    private View loading_icon;
    private int loading_icon_height;
    private PullToRefreshCallback refresh_callback;
    private float lastX = -1, lastY = -1;
    private FActionBar mActionBar;

    public PullToRefresh(Context context) {
        super(context);
    }

    public PullToRefresh(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PullToRefresh(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        loading_icon = LayoutInflater.from(getContext()).inflate(R.layout.listing_loading_pull_to_refresh, null);
        addView(loading_icon);

        RelativeLayout.LayoutParams lp = (LayoutParams) loading_icon.getLayoutParams();
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        loading_icon.setPadding(
                loading_icon.getPaddingLeft(),
                (int) DpPx.dpToPx(getContext(), 10),
                loading_icon.getPaddingRight(),
                (int) DpPx.dpToPx(getContext(), 10)
        );
        loading_icon.setLayoutParams(lp);

        loading_icon.measure(0, 0);
        loading_icon_height = loading_icon.getMeasuredHeight();

        firstChild = getChildAt(0);
        firstChild.setY(loading_icon_height);

        setScrollY(loading_icon_height);
    }

    public void setActionbar(FActionBar actionBar) {
        mActionBar = actionBar;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (mActionBar != null && mActionBar.isHiding()) return super.dispatchTouchEvent(ev);
        boolean intercept = false;
        boolean already_handled = false;
        if (firstChild instanceof ScrollView) {
            ScrollView scrollView = (ScrollView) firstChild;
            if (scrollView.getScrollY() == firstChild.getPaddingTop()) intercept = true;
        }
        if (firstChild instanceof ListView) {
            if (((ListView) firstChild).getChildCount() > 0
                    && ((ListView) firstChild).getFirstVisiblePosition() == 0
                    && ((ListView) firstChild).getChildAt(0).getY() == firstChild.getPaddingTop()) {
                intercept = true;
            }
        }
        switch (ev.getAction()) {
            case MotionEvent.ACTION_MOVE:
                if (intercept) {
                    if (lastY != -1) {
                        float deltaY = ev.getRawY() - lastY;
                        float deltaX = ev.getRawX() - lastX;

                        if (Math.abs(deltaY) > Math.abs(deltaX * 2) && (deltaY > 0 || getScrollY() < loading_icon_height)) {
                            int next_scroll_y = (int) (getScrollY() - deltaY / 4);
                            already_handled = true;
                            if (next_scroll_y < 0) {
                                already_handled = false;
                                next_scroll_y = 0;
                            }
                            if (next_scroll_y > loading_icon_height) {
                                already_handled = false;
                                next_scroll_y = loading_icon_height;
                            }
                            setScrollY(next_scroll_y);
                        }
                    }
                    lastX = ev.getRawX();
                    lastY = ev.getRawY();
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                if (getScrollY() > 0 && getScrollY() < loading_icon_height) {
                    ValueAnimator anim = ObjectAnimator.ofInt(this, "scrollY", loading_icon_height);
                    anim.start();
                    already_handled = true;
                } else if (getScrollY() == 0) {
                    notifyUpdate();
                    already_handled = true;
                }
                lastX = -1;
                lastY = -1;
                break;
        }

        if (already_handled) return true;
        return super.dispatchTouchEvent(ev);
    }

    private void notifyUpdate() {
        if (refresh_callback == null) {
            hideLoading();
        } else {
            refresh_callback.refresh(this);
        }
    }

    public void hideLoading() {
        ValueAnimator anim = ObjectAnimator.ofInt(PullToRefresh.this, "scrollY", loading_icon_height);
        anim.start();
    }

    public void setRefreshCallback(PullToRefreshCallback cb) {
        refresh_callback = cb;
    }

    public static interface PullToRefreshCallback {
        public void refresh(PullToRefresh pullToRefresh);
    }
}
