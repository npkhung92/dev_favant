package com.favant.fview.listing;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.favant.MasterActivity;
import com.favant.R;
import com.favant.fview.common.FImageView;
import com.favant.fview.common.FTextView;
import com.favant.fview.common.RangeSeekBar;
import com.favant.util.HeightEvaluator;
import com.favant.util.MainHandler;
import com.favant.util.ViewChecker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by Hung on 11/17/2015.
 */
public class FilterPopup {
    private static final int SHOW_HIDE_ANIMATION_TIME = 400;
    private static final String TAG = "FilterPopup";
//    private static final LinkedHashMap<String, ListingFragment.FilterParam> top_filter_param = new LinkedHashMap<String, ListingFragment.FilterParam>();
    private static final LinkedHashMap<String, ListingFragment.FilterParam> price_filter_param = new LinkedHashMap<String, ListingFragment.FilterParam>();
    private static final LinkedHashMap<ListingFragment.FilterParam, FImageView> param_checker_map = new LinkedHashMap<ListingFragment.FilterParam, FImageView>();
    private final Context context;
    private final ListingFragment mListingFragment;
    private View popup_content;
    private boolean first_show_hide_for_scroll = true;
    private boolean first_show_hide = true;
    private ScrollView scroller;
//    private View main_layout;
    protected MasterActivity mMasterActivity;
    private HashMap<String, Integer> wrapper_height = new HashMap<String, Integer>();
    private HashMap<String, Boolean> animating = new HashMap<String, Boolean>();
    private ArrayList<ListingFragment.FilterParam> filter_params = new ArrayList<ListingFragment.FilterParam>();
    private ArrayList<ListingFragment.FilterParam> binded_filter_params = new ArrayList<ListingFragment.FilterParam>();

    public FilterPopup(Context context, MasterActivity ma, ListingFragment lf) {
        this.context = context;
        this.mMasterActivity = ma;
        this.mListingFragment = lf;
    }

    private LayoutInflater getLayoutInfalter() {
        return LayoutInflater.from(context);
    }

    public View findViewById(int res_id) {
        return popup_content.findViewById(res_id);
    }

    public void loadPopup(){
        popup_content = getLayoutInfalter().inflate(R.layout.listing_filter, null);
    }

    public void loadDataForPopup(){
//        main_layout = getLayoutInfalter().inflate(R.layout.listing_filter, null);
        scroller = (ScrollView) popup_content.findViewById(R.id.scroller);
        addCategoryFilter();
        addColorFilter();
        addPriceFilter();
    }

    public View getPopupContent() {
        return popup_content;
    }

    private void setAsFilter(View v, final ListingFragment.FilterParam fp) {
        if (ViewChecker.validate(v.findViewById(R.id.check), FImageView.class)) {
            final FImageView iv = (FImageView) v.findViewById(R.id.check);
            param_checker_map.put(fp, iv);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (iv.isSelected()) {
                        iv.setSelected(false);
                        filter_params.remove(fp);
                    } else {
                        iv.setSelected(true);
                        filter_params.add(fp);
                    }
                }
            });
        }
    }

    private void bindShowHideControl(final String key, final ViewGroup header, final ViewGroup wrapper, final boolean hide) {
        if (key == null || key.equals("")) return;
        if (!animating.containsKey(key)) animating.put(key, false);
        if (!wrapper_height.containsKey(key)) wrapper_height.put(key, 0);

//        FImageView header_icon_tmp = null;
//        if (header.getChildCount() >= 2 && ViewChecker.validate(header.getChildAt(1), FImageView.class)) {
//            header_icon_tmp = (FImageView) header.getChildAt(1);
//        }
//        final FImageView header_icon = header_icon_tmp;

        if (hide) {
            ViewGroup.LayoutParams params = wrapper.getLayoutParams();
            params.height = 0;
            wrapper.setLayoutParams(params);
//            header_icon.setRotationX(180);
        }

        final View.OnClickListener onclick = new View.OnClickListener() {
            private int wrapper_height = -1;

            @Override
            public void onClick(View view) {
                if (animating.get(key) == true) return;
                animating.put(key, true);

                wrapper.setPivotY(0);

                if (wrapper_height == -1) {
                    wrapper.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    wrapper_height = wrapper.getMeasuredHeight();
                }

                AnimatorSet animSet = new AnimatorSet();
                int source_height = (wrapper.getHeight() > 0) ? wrapper.getHeight() : 0;
                int target_height = (wrapper.getHeight() > 0) ? 0 : wrapper_height;
                ValueAnimator anim = ObjectAnimator.ofObject(new HeightEvaluator(wrapper), source_height, target_height)
                        .setDuration(SHOW_HIDE_ANIMATION_TIME);
                ValueAnimator scroll = ObjectAnimator.ofInt(scroller, "scrollY", (int) header.getY());
                if (first_show_hide_for_scroll) {
                    scroll = ObjectAnimator.ofInt(scroller, "scrollY", 0);
                }
//                if (header_icon != null) {
//                    int source_rotate = (wrapper.getHeight() > 0) ? 0 : 180;
//                    int target_rotate = (wrapper.getHeight() > 0) ? 180 : 0;
//                    ValueAnimator anim2 = ObjectAnimator.ofFloat(header_icon, "rotationX", source_rotate, target_rotate)
//                            .setDuration(SHOW_HIDE_ANIMATION_TIME);
//                    animSet.playTogether(anim, anim2, scroll);
//                } else {
                    animSet.playTogether(anim, scroll);
//                }
                animSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        animating.put(key, false);
                    }
                });
                animSet.start();
                first_show_hide_for_scroll = false;
            }
        };

        header.setOnClickListener(onclick);

        if (first_show_hide) {
            MainHandler.postDelay(new Runnable() {
                @Override
                public void run() {
                    onclick.onClick(header);
                }
            }, 500);
            first_show_hide = false;
        }
    }

    private void addCategoryFilter() {
        if (ViewChecker.validate(popup_content.findViewById(R.id.filter_category_wrapper), ViewGroup.class)
                && ViewChecker.validate(popup_content.findViewById(R.id.filter_category_header), ViewGroup.class)) {
            final ViewGroup category_wrapper = (ViewGroup) popup_content.findViewById(R.id.filter_category_wrapper);
            final ViewGroup category_header = (ViewGroup) popup_content.findViewById(R.id.filter_category_header);
            category_header.setVisibility(View.VISIBLE);
            bindShowHideControl("category", category_header, category_wrapper, true);
            /* fixed view */
            category_wrapper.removeAllViews();
            for (int i = 0; i < 4; i++){
                View v = getLayoutInfalter().inflate(R.layout.listing_filter_cate_component, category_wrapper, false);
                if (ViewChecker.validate(v.findViewById(R.id.text), FTextView.class)) {
                    FTextView tv = (FTextView) v.findViewById(R.id.text);
                    tv.setText("Short Elastic Tube Dresses");
                }

                category_wrapper.addView(v);
            }

//            bindShowHideControl("category", category_header, category_wrapper, true);
//            try {
//                JSONArray values = obj.getJSONArray("value");
//                String search_key = obj.getString("search_key");
//                for (int i = 0; i < values.length(); i++) {
//                    JSONObject value = values.getJSONObject(i);
//                    String option_id_str = value.getString("option_id");
//                    String option_name = value.getString("option_name");
//                    long facet_count = value.getLong("facet_count");
//
//                    View v = getInflater().inflate(R.layout.listing_filter_cate_component, category_wrapper, false);
//                    if (ViewChecker.validate(v.findViewById(R.id.text), FTextView.class)) {
//                        FTextView tv = (FTextView) v.findViewById(R.id.text);
//                        tv.setText(Html.fromHtml(option_name + " (<font color=#e8101d>" + facet_count + "</font>)"));
//                    }
//
//                    ListingFragment.FilterParam fp = new ListingFragment.FilterParam(search_key, option_id_str);
//
//                    setAsFilter(v, fp);
//
//                    category_wrapper.addView(v);
//                }
//
//            } catch (JSONException e) {
////                if (DEBUG) Log.e(TAG, "", e);
//            }
        }
    }

    private void addColorFilter(){
        if (ViewChecker.validate(popup_content.findViewById(R.id.filter_color_header), ViewGroup.class)
                && ViewChecker.validate(popup_content.findViewById(R.id.filter_color_wrapper), ViewGroup.class)){

            final ViewGroup filter_color_header = (ViewGroup) popup_content.findViewById(R.id.filter_color_header);
            final ViewGroup filter_color_wrapper = (ViewGroup) popup_content.findViewById(R.id.filter_color_wrapper);
            filter_color_header.setVisibility(View.VISIBLE);
            bindShowHideControl("color", filter_color_header, filter_color_wrapper, true);

            filter_color_wrapper.removeAllViews();

            for (int i = 0; i < 8; i ++) {
                View v = getLayoutInfalter().inflate(R.layout.color_box, filter_color_wrapper, false);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(120, 120);
                FImageView iv = (FImageView) v.findViewById(R.id.color_image);
                iv.setImageResource(R.drawable.color_example);
                iv.setAdjustViewBounds(true);
                iv.setLayoutParams(params);
                filter_color_wrapper.addView(v);
            }
        }
    }

    private void addPriceFilter(){
        if (ViewChecker.validate(popup_content.findViewById(R.id.filter_price_header), ViewGroup.class)
                && ViewChecker.validate(popup_content.findViewById(R.id.filter_price_wrapper), ViewGroup.class)) {
            final ViewGroup filter_price_wrapper = (ViewGroup) popup_content.findViewById(R.id.filter_price_wrapper);
            final ViewGroup filter_price_header = (ViewGroup) popup_content.findViewById(R.id.filter_price_header);
            final FTextView min_price = (FTextView) popup_content.findViewById(R.id.min_price);
            final FTextView max_price = (FTextView) popup_content.findViewById(R.id.max_price);
            ViewGroup range_bar_wrapper = (ViewGroup) popup_content.findViewById(R.id.range_bar_wrapper);
            filter_price_header.setVisibility(View.VISIBLE);
            bindShowHideControl("price", filter_price_header, filter_price_wrapper, true);

            range_bar_wrapper.removeAllViews();
            RangeSeekBar<Integer> seekBar = new RangeSeekBar<Integer>(0, 200000, context);
//            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150);
//            seekBar.setLayoutParams(params);
            seekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
                @Override
                public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                    min_price.setText(String.valueOf(minValue));
                    max_price.setText(String.valueOf(maxValue));
                }
            });
            range_bar_wrapper.addView(seekBar);
        }
    }

    public void updateChecker(ArrayList<ListingFragment.FilterParam> fp_list) {
        binded_filter_params.removeAll(binded_filter_params);
        for (ListingFragment.FilterParam fp : fp_list) {
            boolean found = false;
            for (ListingFragment.FilterParam fp2 : param_checker_map.keySet()) {
                if (fp.key.equals(fp2.key)) {
                    found = true;
                    String[] values = fp2.value.split(",");
                    for (int i = 0; i < values.length; i++) {
                        if (values[i].equals(fp.value)) {
                            binded_filter_params.add(fp2);
                            break;
                        }
                    }
                }
            }
            if (!found) binded_filter_params.add(fp);
        }
        filter_params.clear();
        filter_params.addAll(binded_filter_params);
        updateAndResetChecker();
    }

    /**
     * check all checker in binded_filter_params, remove orthers
     */
    private void updateAndResetChecker() {
        for (ListingFragment.FilterParam fp : filter_params) {
            FImageView iv = param_checker_map.get(fp);
            if (!binded_filter_params.contains(fp)) {
                if (iv != null) iv.setSelected(false);
            }
        }
        for (ListingFragment.FilterParam fp : binded_filter_params) {
            FImageView iv = param_checker_map.get(fp);
            if (iv != null) iv.setSelected(true);
        }
        filter_params.removeAll(filter_params);
        filter_params.addAll(binded_filter_params);
    }

    public void syncFilterParams(ArrayList<ListingFragment.FilterParam> fps) {
        binded_filter_params.clear();
        binded_filter_params.addAll(fps);

        updateAndResetChecker();
    }
}
