package com.favant.fview.listing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.favant.model.ListingProductModel;

import java.util.ArrayList;

/**
 * Created by Bear on 8/30/2015.
 */
public class ListingAdapter extends BaseAdapter {
    protected ArrayList<ListingProductModel> product_model_list = new ArrayList<ListingProductModel>();
    protected Context context;
    protected ListingFragment mListingFragment;

    public ListingAdapter(Context context, ListingFragment lf) {
        this.context = context;
        mListingFragment = lf;
    }

    protected LayoutInflater getInflater() {
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return li;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return product_model_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }

    public void addProModel(ArrayList<ListingProductModel> list) {
        product_model_list.addAll(list);
    }

    public ArrayList<ListingProductModel> getProModel() {
        return product_model_list;
    }

    public void clearData() {
        product_model_list.clear();
    }

    public void removeAllProducts() {
        product_model_list.removeAll(product_model_list);
    }
}
