package com.favant.fview.listing;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.favant.R;
import com.favant.fview.common.ListingProductBox;

/**
 * Created by Bear on 9/3/2015.
 */
public class ListingAdapterGrid extends ListingAdapter {
    public ListingAdapterGrid(Context context, ListingFragment lf) {
        super(context, lf);
    }
    @Override
    public int getCount() {
        int row_count = product_model_list.size() / 2;
        if (row_count * 2 != product_model_list.size()) row_count++;
        return row_count;
    }

    @Override
    public Object getItem(int i) {
        return product_model_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final Holder holder;

        if (view != null && view.getTag() instanceof Holder) {
            holder = (Holder) view.getTag();
        } else {
            holder = new Holder();
        }

        if (view == null) {
            view = getInflater().inflate(R.layout.listing_product_row, viewGroup, false);
            ViewGroup vg = (ViewGroup) view;

            ListingProductBox box1 = (ListingProductBox) getInflater().inflate(R.layout.listing_product_box, vg, false);
            ListingProductBox box2 = (ListingProductBox) getInflater().inflate(R.layout.listing_product_box, vg, false);

            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) box2.getLayoutParams();
            lp.width = 0;
            lp.weight = 1;
            box2.setLayoutParams(lp);

            LinearLayout.LayoutParams lp2 = (LinearLayout.LayoutParams) box1.getLayoutParams();
            lp2.width = 0;
            lp2.weight = 1;
            lp2.leftMargin = 0;
            box1.setLayoutParams(lp2);

            holder.box1 = box1;
            holder.box2 = box2;

            vg.addView(box1);
            vg.addView(box2);

            view.setTag(holder);
        }

        if (view instanceof ViewGroup) {
            final ListingProductBox box1, box2;
            holder.box1.setListingProductModel(product_model_list.get(2 * i));
            if (product_model_list.size() > 2 * i + 1) {
                if (holder.box2.getVisibility() == View.GONE)
                    holder.box2.setVisibility(View.VISIBLE);
                if (product_model_list.size() > 2 * i + 1)
                    holder.box2.setListingProductModel(product_model_list.get(2 * i + 1));
            } else holder.box2.setVisibility(View.GONE);
        }

        return view;
    }

    protected static class Holder {
        public int res_id = -1;
        public ListingProductBox box1;
        public ListingProductBox box2;
    }
}
