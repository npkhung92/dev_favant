package com.favant.fview.listing;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.favant.R;
import com.favant.fview.common.FActionBar;
import com.favant.fview.common.FFragment;
import com.favant.fview.common.LeftNavigator.FLeftNavigationDrawer;
import com.favant.fview.common.FListView;
import com.favant.fview.common.FMenuRight;
import com.favant.fview.common.FPopup;
import com.favant.fview.common.FScrollView;
import com.favant.fview.common.PullToRefresh;
import com.favant.model.Category;
import com.favant.model.ListingProductModel;
import com.favant.model.Merchant;
import com.favant.util.MainHandler;
import com.favant.util.Net;
import com.favant.util.WebService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Bear on 8/30/2015.
 */
public class ListingFragment extends FFragment {

    private static String TAG = "Favant_ListingFragment";
    public static final String DEFAULT_SORTTYPE = "vasup";
    public static final int LISTING_TYPE_GRID = 0;
    //    public static final int LISTING_TYPE_DETAIL = 1;
    public static final int LISTING_TYPE_LIST = 1;
    private static boolean DEBUG = true;
    //    public ListingMenu mMenu;
    public ListingFilter mFilter;
    public View right_menu_content = null;
    public View right_filter_content = null;
    protected String url_str = "";
    protected long product_count = 0;
    protected int limit = 20;
    protected int page = 0;
    protected String sort_type = DEFAULT_SORTTYPE;
    protected String sort_direction = "desc";
    protected int listing_type = LISTING_TYPE_GRID;
    protected Object load_more_lock = new Object();
    protected boolean loading_more = false;
    protected boolean load_more_stopped = false;
    protected ListView mFListView;
    protected ListingAdapter mListingAdapter;
    protected View mFListViewFooterView;
    protected ArrayList<FilterParam> filter_params = new ArrayList<ListingFragment.FilterParam>();
    protected String filter_params_str = "";
    protected ArrayList<ListingProductModel> pro_model_list = new ArrayList<ListingProductModel>();
    protected PullToRefresh pull_to_refresh;
    private FScrollView mScroller;
    private int category_id = 10;
    private Category category_model = new Category();
    private View mBackToTop;
    private ConcurrentHashMap<Integer, Merchant> shop_info_cache = new ConcurrentHashMap<Integer, Merchant>();
    private int count = 0;
    private FilterPopup my_popup;

    public ListingFragment() {
        setLayout(R.layout.master_page, R.layout.listing);
    }


    @Override
    protected void loadFActionBar(FActionBar ab) {
        ab.setCallback(new ListingActionBarCallback());
        ab.showCartButton();
        ab.showBarButton();
        ab.showBackButton();
        ab.showSearchButton();
//        ab.setPageTitle("Short Elastic ...");

    }

    @Override
    protected synchronized void onPreCreate() {
        if (!parseArguments()) {
//            mMasterActivity.goBack();
            return;
        }
    }

    @Override
    protected void onCreate() {
        loadPopup();
        loadMore();
        bindFilterNavControl();
        bindListView();
    }

    private boolean parseArguments() {
        Bundle bundle = getArguments();
        if (bundle == null) return false;
        category_id = bundle.getInt("category_id", 0);
        url_str = bundle.getString("url", url_str);
        if (!url_str.equals("")) parseUrl(url_str);

        if (category_id == 0) return false;
        return true;
    }

    protected void parseUrl(String surl) {
        List<NameValuePair> params = null;
        try {
            String nurl = StringEscapeUtils.unescapeHtml4(surl);
            params = URLEncodedUtils.parse(new URI(nurl), "UTF-8");

            for (NameValuePair param : params) {
//                Log.v(TAG, param.getName() + " : " + param.getValue());
                String name = param.getName();
                String value = param.getValue();

                if (name.equals("sortType")) {
                    String[] values = value.split("_");
                    if (values.length == 2) {
                        sort_type = values[0];
                        sort_direction = values[1];
                    } else if (values.length == 3) {
                        sort_type = values[0] + "_" + values[1];
                        sort_direction = values[2];
                    }
                } else {
                    String[] values = value.split(",");
                    for (int i = 0; i < values.length; i++) {
                        filter_params.add(new FilterParam(name, values[i]));
                    }
                }
            }
            calculateFilterParamsString();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getSortType() {
        return sort_type;
    }

    public String getSortDirection() {
        return sort_direction;
    }

    public int getListingType() {
        return listing_type;
    }

    public void setSortType(String sort_type, String sort_direction) {
        this.sort_type = sort_type;
        this.sort_direction = sort_direction;
//        reload();
    }

    // Listing Type
    public void setListingType(int lt) {
        int first_visible = mFListView.getFirstVisiblePosition();
        if (listing_type != LISTING_TYPE_GRID && lt == LISTING_TYPE_GRID) {
            first_visible = (int) Math.ceil((float) first_visible / 2);
        } else if (listing_type == LISTING_TYPE_GRID && lt != LISTING_TYPE_GRID) {
            first_visible = first_visible * 2;
        }
        listing_type = lt;
        switch (lt) {
            case LISTING_TYPE_GRID:
                mListingAdapter = new ListingAdapterGrid(getContext(), this);
                mListingAdapter.addProModel(pro_model_list);
                break;
            case LISTING_TYPE_LIST:
                mListingAdapter = new ListingAdapterList(getContext(), this);
                mListingAdapter.addProModel(pro_model_list);
                break;
//            case LISTING_TYPE_DETAIL:
//                mListingAdapter = new ListingAdapterDetail(getContext(), this);
//                mListingAdapter.addProModel(pro_model_list);
//                break;
        }
        mFListView.setAdapter(mListingAdapter);
        mFListView.setSelection(first_visible);
    }

    protected void bindFilterNavControl() {
        ((FilterNav) findViewById(R.id.filter_nav)).setListingFragment(this);
    }

    protected void bindListView() {
        mFListView = (FListView) findViewById(R.id.listing_listview);
        mBackToTop = findViewById(R.id.back_to_top);

        mFListViewFooterView = mInflater.inflate(R.layout.listing_loading, null);
        mFListViewFooterView.setVisibility(View.GONE);
        mFListView.addFooterView(mFListViewFooterView);
        mFListView.setDividerHeight(0);
        mFListView.setDivider(null);

        final FActionBar ab = getFActionBar();
//        if (ab != null) {
//            ab.setScrollableToPerformShowHideAnimation(mFListView);
//            ab.setAdditionalHeightToFloatOnCC((int) DpPx.dpToPx(mMasterActivity, 41));
//        }
//
        mBackToTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFListView.getFirstVisiblePosition() > 10) {
                    mFListView.setSelection(10);
                }
                mFListView.smoothScrollToPositionFromTop(0, 0, 300);
//                if (ab != null) ab.showFloating();
            }
        });

        switch (listing_type) {
            case LISTING_TYPE_GRID:
                mListingAdapter = new ListingAdapterGrid(getContext(), this);
                break;
            case LISTING_TYPE_LIST:
                mListingAdapter = new ListingAdapterList(getContext(), this);
                break;
//            case LISTING_TYPE_DETAIL:
//                mListingAdapter = new ListingAdapterDetail(getContext(), this);
//                break;
        }

        mFListView.setAdapter(mListingAdapter);

        mFListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
                if (mFListView.getLastVisiblePosition() >= mListingAdapter.getCount() - 1) {
//                    loadMore();
                }
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                if (mFListView.getFirstVisiblePosition() > 0) {
                    mBackToTop.setVisibility(View.VISIBLE);
                    ValueAnimator anim = ObjectAnimator.ofFloat(mBackToTop, "alpha", 1f);
                    anim.start();
                } else {
                    ValueAnimator anim = ObjectAnimator.ofFloat(mBackToTop, "alpha", 0f);
                    anim.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mBackToTop.setVisibility(View.GONE);
                        }
                    });
                    anim.start();
                }
            }
        });

        pull_to_refresh = (PullToRefresh) findViewById(R.id.pull_to_refresh);
        if (pull_to_refresh != null) {
            pull_to_refresh.setActionbar(getFActionBar());
            pull_to_refresh.setRefreshCallback(new PullToRefresh.PullToRefreshCallback() {
                @Override
                public void refresh(PullToRefresh pullToRefresh) {
                    reloadOnRefresh();
                }
            });
        }
    }

    private void loadCategoryData() {
        WebService.getJson("menu/app-get-menu/" + category_id, new Net.JsonResult() {
            @Override
            public void success(Object res) {
                Log.v(TAG, String.valueOf(res));
                if (res instanceof JSONObject) {
                    try {
                        JSONObject obj = (JSONObject) res;
                        category_model.category_id = obj.getInt("Category_id");
                        category_model.name = obj.getString("Name");
                        category_model.path = obj.getString("Path");
                        if (category_model.getCategoryLevel() <= 2) {
                            FActionBar action_bar = getFActionBar();
                            if (action_bar != null) {
                                action_bar.showMoreButton();
                            }
                        }
                        category_model.path_minimal = category_model.path.replace("1/2/", "");

                        Runnable r = new Runnable() {
                            @Override
                            public void run() {
                                loadFilter();
                            }
                        };
                        updateCategoryUI();
                        loadMore();

                        if (fragment_created.get()) r.run();
                        else addOnCreateObserver(r);

                    } catch (JSONException e) {
                        if (DEBUG) Log.e(TAG, String.valueOf(e));
                    } catch (Exception e) {
                        if (DEBUG) Log.e(TAG, String.valueOf(e));
                    }
                }
            }

            @Override
            public void error(Exception e) {
                Log.e(TAG, String.valueOf(e));
            }
        });
    }

    public void setFilterParams(ArrayList<FilterParam> fps) {
        filter_params.clear();
        filter_params.addAll(fps);

        mFilter.syncFilterParams(filter_params);
        calculateFilterParamsString();
    }

    private void updateCategoryUI() {
        // Set Page Title
        FActionBar action_bar = getFActionBar();
        if (action_bar != null) {
//            action_bar.setPageTitle(category_model.name);
        }
    }

    protected void calculateFilterParamsString() {
        ArrayList<FilterParam> tmp_filter_params = new ArrayList<FilterParam>();
//        tmp_filter_params.addAll(filter_params);

        for (FilterParam fp : filter_params) {
            tmp_filter_params.add(new FilterParam(fp));
        }

        int i = 1;
        while (true) {
            if (i >= tmp_filter_params.size()) break;
            boolean found = false;
            for (int j = i - 1; j >= 0; j--) {
                FilterParam fpi = tmp_filter_params.get(i);
                FilterParam fpj = tmp_filter_params.get(j);
                if (fpi.key.equals(fpj.key)) {
                    fpj.value += "," + fpi.value;
                    tmp_filter_params.remove(i);
                    found = true;
                    break;
                }
            }
            if (found) continue;
            i++;
        }
        String tmp = "";
        for (i = 0; i < tmp_filter_params.size(); i++) {
            FilterParam fp = tmp_filter_params.get(i);
            if (tmp.equals("")) tmp += fp.key + "=" + fp.value;
            else tmp += "&" + fp.key + "=" + fp.value;
        }
        filter_params_str = tmp;

        Log.v(TAG, "update filter param: " + filter_params_str);
    }

    protected String getLoadMoreUrl() {
//        String str = "service/category/getProducts/?cate_id=" + 30 + "?page=" + page + "&limit=" + limit;
        String str = "service/category/getProducts/?cate_id=29&page=1&limit=12&w=187&h=186";
        Log.v(TAG, str);
        return str;
    }

    protected void loadMore(final Runnable before_load) {
        synchronized (load_more_lock) {
            if (loading_more || load_more_stopped) return;
            loading_more = true;
            page++;
            final int tmp_page = page;
            WebService.getJson(getLoadMoreUrl(), new Net.JsonResult() {
                @Override
                public void success(final Object res) {
                    Log.v(TAG, String.valueOf(res));
                    if (res instanceof JSONObject) {
                        if (before_load != null) before_load.run();
                        JSONObject obj = (JSONObject) res;
                        JSONArray list = obj.optJSONArray("products");
                        int length = list.length();

                        if (length == 0) {
                            stopLoadMore();
                            return;
                        }

                        Type array_listing_product_type = new TypeToken<ArrayList<ListingProductModel>>() {
                        }.getType();
                        Gson gson = new Gson();
                        final ArrayList<ListingProductModel> pro_model_list_2 = gson.fromJson(list.toString(), array_listing_product_type);
                        pro_model_list = gson.fromJson(list.toString(), array_listing_product_type);
                        if (pro_model_list_2.size() <= 0) {
                            stopLoadMore();
                        }
//                        for (int i = 0; i < length; i++) {
//                            JSONObject pro = list.optJSONObject(i);
//                            final ListingProductModel pro_model = new ListingProductModel();
//                            pro_model_list.add(pro_model);
//                            pro_model_list_2.add(pro_model);
//
//                            if (product_count != 0 && product_count == pro_model_list.size())
//                                stopLoadMore();
//                        }


                        Runnable r = new Runnable() {
                            @Override
                            public void run() {
                                if (mFListViewFooterView != null)
                                    mFListViewFooterView.setVisibility(View.VISIBLE);
                                if (pull_to_refresh != null) pull_to_refresh.hideLoading();

                                mListingAdapter.addProModel(pro_model_list_2);
                                mListingAdapter.notifyDataSetChanged();

                                if (tmp_page == 1) {
//                                        hideLoading();
                                }
                            }
                        };

                        if (fragment_created.get()) r.run();
                        else addOnCreateObserver(r);

//                        } catch (JSONException e) {
//                            stopLoadMore();
//                            Log.e(TAG, "", e);
//                        }
                        loading_more = false;
                    }
                }

                @Override
                public void error(Exception e) {
                    Log.e(TAG, String.valueOf(e));
                }
            });
        }
    }

//    protected void getShopInfo(final ListingProductModel pro_model) {
//        if (pro_model.admin_id != -1) {
//            if (shop_info_cache.containsKey(pro_model.admin_id)) {
//                pro_model.merchant = shop_info_cache.get(pro_model.admin_id);
//                Log.v(TAG, "hit cache shop_info: " + pro_model.admin_id);
//            } else {
//                WebService.getJson("detail/restful/get-shop-info/?merchant_id=" + pro_model.admin_id, new Net.JsonResultAdapter() {
//                    @Override
//                    public void success(Object res) {
//                        if (res instanceof JSONObject) {
//                            JSONObject obj = (JSONObject) res;
//                            pro_model.merchant.name = obj.optString("shop_name");
//                            if (obj.optInt("Is_certified") == 1)
//                                pro_model.merchant.is_certified = true;
//                            pro_model.merchant.promotion_app = (float) obj.optDouble("promotion_app");
//
//                            shop_info_cache.put(pro_model.admin_id, pro_model.merchant);
//                            pro_model.notifyDataChanged();
//                        }
//                    }
//                });
//            }
//        }
//    }

    protected void loadMore() {
        loadMore(null);
    }

    public void clearProductList() {
        mFListView.setSelection(0);
        pro_model_list.removeAll(pro_model_list);
        mListingAdapter.clearData();
        mListingAdapter.notifyDataSetChanged();
        page = 0;
    }

    public void reload() {
        clearProductList();
        if (load_more_stopped) {
            load_more_stopped = false;
            showListViewLoading();
        }
        loadMore();
    }

    public void reloadOnRefresh() {
        page = 0;
        mFListViewFooterView.setVisibility(View.GONE);
        if (load_more_stopped) {
            load_more_stopped = false;
            showListViewLoading();
        }
        loadMore(new Runnable() {

            @Override
            public void run() {
                mFListView.setSelection(0);
                pro_model_list.removeAll(pro_model_list);
                mListingAdapter.clearData();
                mListingAdapter.notifyDataSetChanged();
            }
        });
    }

    public void loadFilter() {
        FMenuRight mr = getMenuRight();
        mFilter = new ListingFilter(mMasterActivity, this, mr, category_model, getContext(), rootView);
        mFilter.loadData();
    }

    // Filter
    public void openRightFilter() {
        FMenuRight mr = getMenuRight();
        if (mr != null && right_filter_content != null) {
            mr.setContent(right_filter_content);
            mr.show();
        }
    }

    public void setRightFilterContent(View v) {
        right_filter_content = v;
        FMenuRight mr = getMenuRight();
        if (mr != null && right_filter_content != null) {
            mr.setContent(right_filter_content);
            if (mFilter != null) mFilter.updateChecker(filter_params);
        }
    }

    public void hideMenuRight() {
        FMenuRight mr = getMenuRight();
        if (mr != null) {
            mr.hide();
        }
    }

    public void hideMenuRightWithNoAnimation() {
        FMenuRight mr = getMenuRight();
        if (mr != null && right_menu_content != null) {
            mr.setContent(right_menu_content);
            mr.hideWithNoAnimation();
        }
    }

    public void setRightMenuContent(View v) {
        right_menu_content = v;
    }

    protected void hideListViewLoading() {
        if (mFListView != null && mFListViewFooterView != null)
            mFListView.removeFooterView(mFListViewFooterView);
    }

    protected void showListViewLoading() {
        if (mFListView != null && mFListViewFooterView != null && mFListView.getFooterViewsCount() == 0) {
            Log.v(TAG, "add Footer View");
            mFListView.addFooterView(mFListViewFooterView);
        }
    }

    protected void stopLoadMore() {
        hideListViewLoading();
        loading_more = false;
        load_more_stopped = true;
    }


    public void loadPopup() {
        my_popup = new FilterPopup(getContext(), mMasterActivity, this);
        my_popup.loadPopup();
    }

    public void showFilterPopup() {
        FPopup popup = getPopup();
        if (my_popup.getPopupContent() != null && popup != null) {
            my_popup.loadDataForPopup();
            popup.setContent(my_popup.getPopupContent());
            popup.setTitle("Filter");
            popup.setDoneBtn("Apply");
//            popup.showDoneButton();
            popup.show();
//            popup.setPopupDoneListener(my_popup.done_listener);
        }
    }

    public void hidePopup() {
        FPopup popup = getPopup();
        if (popup != null) {
            popup.hide();
        }
    }

    public static class FilterParam {
        public String key;
        public String value;

        public FilterParam(FilterParam fp) {
            key = fp.key;
            value = fp.value;
        }

        public FilterParam(String k, String v) {
            key = k;
            value = v;
        }

        @Override
        public String toString() {
            return "{filter param: " + key + " -> " + value + "}";
        }
    }

    private class ListingActionBarCallback extends FActionBar.FActionBarCallBackAdapter {

        @Override
        public void barClicked() {
//            super.barClicked();

            FLeftNavigationDrawer fnd = getLeftNavDrawer();
            if (fnd != null) {
                if (!fnd.isShowing())
                    fnd.show();
                else
                    fnd.hide();
            }
        }
    }
}
