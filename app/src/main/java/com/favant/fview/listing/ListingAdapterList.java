package com.favant.fview.listing;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.favant.R;
import com.favant.fview.common.ListingProductBox;

/**
 * Created by Hung on 11/17/2015.
 */
public class ListingAdapterList extends ListingAdapter {

    public ListingAdapterList(Context context, ListingFragment lf) {
        super(context, lf);
    }


    @Override
    public int getCount() {
        return product_model_list.size();
    }

    @Override
    public Object getItem(int position) {
        return product_model_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = getInflater().inflate(R.layout.listing_product_box_list, parent, false);
        }

        if (view instanceof ViewGroup && ((ViewGroup) view).getChildAt(0) instanceof ListingProductBox) {
            ListingProductBox box = (ListingProductBox) ((ViewGroup) view).getChildAt(0);
            box.setListingType(mListingFragment.getListingType());
            box.setListingProductModel(product_model_list.get(position));
        }

        return view;
    }
}
