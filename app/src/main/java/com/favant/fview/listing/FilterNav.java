package com.favant.fview.listing;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.favant.R;
import com.favant.fview.common.FImageView;
import com.favant.fview.common.FTextView;
import com.favant.util.HeightEvaluator;
import com.favant.util.ViewChecker;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Bear on 9/13/2015.
 */
public class FilterNav extends RelativeLayout implements View.OnClickListener {
    private static final int DROPDOWN_ANIMATION_TIME = 200;
    private static final int SELECTED_COLOR = Color.parseColor("#cc0000");
    private static final int UNSELECTED_COLOR = Color.parseColor("#666666");
    private static boolean DEBUG = true;
    private boolean promotionDropdownOpened = false;
    private boolean mainDropdownOpened = false;
    private ListingFragment mListingFragment = null;

    //    private LinkedHashMap<String, String> main_dropdown = new LinkedHashMap<String, String>();
    private ArrayList<DropDownItem> main_dropdown = new ArrayList<DropDownItem>();
//    private ArrayList<PromotionDropDownItem> promotion_dropdown = new ArrayList<PromotionDropDownItem>();
    private LinkedHashMap<View, DropDownItem> view_main_dropdown_map = new LinkedHashMap<View, DropDownItem>();

    private FTextView main_dropdown_title = null;
    private FImageView main_dropdown_icon = null;

    private FTextView price_title = null;
    private FImageView price_icon = null;

    private ViewGroup listing_type = null;
    private FImageView listing_type_icon = null;

    private ArrayList<ListingTypeIconMap> listing_type_icon_map = new ArrayList<ListingTypeIconMap>();
//    private LinkedHashMap<View, PromotionDropDownItem> view_promotion_dropdown_map = new LinkedHashMap<View, PromotionDropDownItem>();
    private FTextView promotion_dropdown_title;
    private FImageView promotion_dropdown_icon;
    private ListingFilter mFilter;

    public FilterNav(Context context) {
        super(context);
    }

    public FilterNav(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FilterNav(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    private LayoutInflater getInflater() {
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return li;
    }

    public void setListingFragment(ListingFragment lf) {
        mListingFragment = lf;
        bindView();
    }

    public void setListingFilter(ListingFilter ft) {
        mFilter = ft;
    }
    public void bindView(){
        super.onFinishInflate();

        // drop down
        findViewById(R.id.dropdown_opener).setOnClickListener(this);
        findViewById(R.id.dropdown_overlay).setOnClickListener(this);
        main_dropdown_title = (FTextView) findViewById(R.id.dropdown_title);
        main_dropdown_icon = (FImageView) findViewById(R.id.dropdown_icon);

        addMainDropDownContent();

//        // promotion
//        findViewById(R.id.promotion_wrapper).setOnClickListener(this);
//        promotion_dropdown_title = (FTextView) findViewById(R.id.promotion_title);
//        promotion_dropdown_icon = (FImageView) findViewById(R.id.promotion_icon);
//
//        findViewById(R.id.finish_promotion_dropdown).setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finishPromotionDropdown();
//            }
//        });
//
//        addPromotionDropDownContent();

        findViewById(R.id.fitler_opener).setOnClickListener(this);

        // listing type
        listing_type_icon_map.add(new ListingTypeIconMap(ListingFragment.LISTING_TYPE_GRID, R.drawable.listing_list_grey));
//        listing_type_icon_map.add(new ListingTypeIconMap(ListingFragment.LISTING_TYPE_LIST, R.drawable.listing_detail_grey));
        listing_type_icon_map.add(new ListingTypeIconMap(ListingFragment.LISTING_TYPE_LIST, R.drawable.listing_grid_grey));

        if (ViewChecker.validate(findViewById(R.id.listing_type), ViewGroup.class)) {
            listing_type = (ViewGroup) findViewById(R.id.listing_type);
            listing_type.setOnClickListener(this);
        }
        if (ViewChecker.validate(findViewById(R.id.listing_type_image), FImageView.class)) {
            listing_type_icon = (FImageView) findViewById(R.id.listing_type_image);
        }
    }

    private void addMainDropDownContent() {
        String default_nav = mListingFragment.getSortType();

//        if (default_nav.equals("rank")) {
//            main_dropdown.add(new DropDownItem("Đề cử", "Đề cử", "rank", "desc"));
//        } else {
//            main_dropdown.add(new DropDownItem("Đề cử", "Đề cử", "vasup", "desc"));
//        }
        main_dropdown.add(new DropDownItem("Featured", "Featured", "rank", "desc"));
        main_dropdown.add(new DropDownItem("Newest", "Newest", "product", "desc"));
        main_dropdown.add(new DropDownItem("Price Hight to Low", "Price", "price", "desc"));
        main_dropdown.add(new DropDownItem("Price Low to High", "Price", "price", "asc"));
        main_dropdown.add(new DropDownItem("Sale", "Sale", "norder_30", "desc"));
        main_dropdown.add(new DropDownItem("Rating", "Rating", "rating", "desc"));


        if (ViewChecker.validate(findViewById(R.id.dropdown_wrapper), ViewGroup.class)) {
            ViewGroup vg = (ViewGroup) findViewById(R.id.dropdown_wrapper);
            vg.setBackgroundColor(Color.parseColor("#ffffff"));
            boolean found_default_nav = false;

            for (DropDownItem item : main_dropdown) {
                if (item.key.equals(default_nav)) {
                    if (main_dropdown_title != null && main_dropdown_icon != null) {
                        found_default_nav = true;
                        main_dropdown_title.setText(item.short_name);
                        main_dropdown_title.setTextColor(SELECTED_COLOR);
                        main_dropdown_icon.setImageResource(R.drawable.red_down);
                    }
                }
                View nav = getInflater().inflate(R.layout.listing_filter_nav_component, null);
                if (ViewChecker.validate(nav.findViewById(R.id.text), FTextView.class)
                        && ViewChecker.validate(nav.findViewById(R.id.check), FImageView.class)) {
                    FTextView tv = (FTextView) nav.findViewById(R.id.text);
                    FImageView check = (FImageView) nav.findViewById(R.id.check);

                    tv.setText(item.name);
                    if (item.key.equals(default_nav)) {
                        tv.setTextColor(SELECTED_COLOR);
                        check.setSelected(true);
                    }

                    vg.addView(nav);

                    nav.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mainSubNavClicked(v);
                        }
                    });

                    view_main_dropdown_map.put(nav, item);
                }
            }
            if (!found_default_nav) {
                main_dropdown_title.setText("Featured");
            }
        }
    }
    private void toggleMainDropDown(Runnable cb) {
        if (mainDropdownOpened) closeMainDropDown(cb);
        openMainDropDown(cb);
    }

    private void openMainDropDown(final Runnable cb) {
        if (mainDropdownOpened) return;
        promotionDropdownOpened = false;
        findViewById(R.id.dropdown_overlay).setVisibility(View.VISIBLE);
        View wrapper = findViewById(R.id.dropdown_wrapper);
        wrapper.setVisibility(View.VISIBLE);
        wrapper.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ValueAnimator anim = ObjectAnimator.ofObject(new HeightEvaluator(wrapper), 0, wrapper.getMeasuredHeight())
                .setDuration(DROPDOWN_ANIMATION_TIME);

        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mainDropdownOpened = true;
                if (cb != null) cb.run();
            }
        });
        anim.start();
    }

    private void closeMainDropDown(final Runnable cb) {
        if (!mainDropdownOpened) return;
        final View wrapper = findViewById(R.id.dropdown_wrapper);
        ValueAnimator anim = ObjectAnimator.ofObject(new HeightEvaluator(wrapper), wrapper.getHeight(), 0)
                .setDuration(DROPDOWN_ANIMATION_TIME);

        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                findViewById(R.id.dropdown_overlay).setVisibility(View.GONE);
                wrapper.setVisibility(View.GONE);
                mainDropdownOpened = false;
                if (cb != null) cb.run();
            }
        });
        anim.start();
    }

    private void resetMainSubNavs() {
        for (View nav : view_main_dropdown_map.keySet()) {
            if (ViewChecker.validate(nav.findViewById(R.id.text), FTextView.class)
                    && ViewChecker.validate(nav.findViewById(R.id.check), FImageView.class)) {
                FTextView tv = (FTextView) nav.findViewById(R.id.text);
                FImageView check = (FImageView) nav.findViewById(R.id.check);

                tv.setTextColor(UNSELECTED_COLOR);
                check.setSelected(false);
            }
        }
    }

    private void mainSubNavClicked(final View v) {
        if (!view_main_dropdown_map.containsKey(v)) return;

//        resetPrice();
        resetMainSubNavs();

        if (ViewChecker.validate(v.findViewById(R.id.text), FTextView.class)
                && ViewChecker.validate(v.findViewById(R.id.check), FImageView.class)) {
            FTextView tv = (FTextView) v.findViewById(R.id.text);
            FImageView check = (FImageView) v.findViewById(R.id.check);

            tv.setTextColor(SELECTED_COLOR);
            check.setSelected(true);
        }

        if (main_dropdown_title != null) {
            main_dropdown_title.setText(view_main_dropdown_map.get(v).short_name);
            main_dropdown_title.setTextColor(SELECTED_COLOR);
        }

        closeMainDropDown(new Runnable() {
            @Override
            public void run() {
                if (mListingFragment != null) {
                    DropDownItem item = view_main_dropdown_map.get(v);
                    mListingFragment.setSortType(item.key, item.direction);
//                    main_dropdown_icon.setImageResource(item.direction.equals("desc") ? R.drawable.arrow_up_down_red_down : R.drawable.arrow_up_down_red_up);
                }
            }
        });
    }

    private void openFilterMenu() {
        mListingFragment.showFilterPopup();
//        Runnable r = new Runnable() {
//            @Override
//            public void run() {
//                mListingFragment.showFilterPopup();
//            }
//        };
//        r.run();
//        if (mainDropdownOpened) closeMainDropDown(r);
//        else r.run();
    }

    private ListingTypeIconMap getNextListingType() {
        if (mListingFragment == null) return listing_type_icon_map.get(0);
        int current_listing_type = mListingFragment.getListingType();
        for (int i = 0; i < listing_type_icon_map.size(); i++) {
            ListingTypeIconMap ltim = listing_type_icon_map.get(i);
            if (ltim.listing_type == current_listing_type) {
                if (i == listing_type_icon_map.size() - 1) return listing_type_icon_map.get(0);
                else return listing_type_icon_map.get(i + 1);
            }
        }
        return listing_type_icon_map.get(0);
    }

    private void listingTypeToggle() {
        ListingTypeIconMap ltim = getNextListingType();

        listing_type_icon.setImageResource(ltim.res_id);
        if (mListingFragment != null) {
            mListingFragment.setListingType(ltim.listing_type);
        }
    }

    private static class DropDownItem {
        public final String name;
        public final String short_name;
        public final String key;
        public final String direction;

        public DropDownItem(String name, String short_name, String key, String direction) {
            this.name = name;
            this.short_name = short_name;
            this.key = key;
            this.direction = direction;
        }
    }

    private class ListingTypeIconMap {
        public int listing_type;
        public int res_id;

        public ListingTypeIconMap(int lt, int rid) {
            listing_type = lt;
            res_id = rid;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dropdown_opener:
                toggleMainDropDown(null);
                break;
            case R.id.dropdown_overlay:
                closeMainDropDown(null);
                break;
            case R.id.fitler_opener:
                openFilterMenu();
                break;
            case R.id.listing_type:
                listingTypeToggle();
                break;
        }
    }
}
