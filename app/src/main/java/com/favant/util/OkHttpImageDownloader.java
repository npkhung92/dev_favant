package com.favant.util;

import android.content.Context;

import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Bear on 8/22/2015.
 */
public class OkHttpImageDownloader extends BaseImageDownloader {

    private OkHttpClient client;

    public OkHttpImageDownloader(Context context, OkHttpClient client) {
        super(context);
        this.client = client;
    }

    @Override
    protected InputStream getStreamFromNetwork(String imageUri, Object extra) throws IOException {
        Request request = new Request.Builder().url(imageUri).build();
        return client.newCall(request).execute().body().byteStream();
    }
}
