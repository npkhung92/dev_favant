package com.favant.util;

import android.os.Handler;
import android.os.Looper;

/**
 * Created by Bear on 8/22/2015.
 */
public class MainHandler {

    private static Handler mHandler = new Handler(Looper.getMainLooper());

    public static void post(Runnable r) {
        mHandler.post(r);
    }

    public static void postDelay(Runnable r, int delayMillis) {
        mHandler.postDelayed(r, delayMillis);
    }

    public static void removeCallBacks(Runnable r) {
        mHandler.removeCallbacks(r);
    }
}