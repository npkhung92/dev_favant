package com.favant.util;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Hung on 11/1/2015.
 */
public class ThreadPool {


    // Sets the amount of time an idle thread will wait for a task before terminating
    private static final int KEEP_ALIVE_TIME = 120;
    // Sets the Time Unit to seconds
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
    /**
     * NOTE: This is the number of total available cores. On current versions of
     * Android, with devices that use plug-and-play cores, this will return less
     * than the total number of cores. The total number of cores is not
     * available in current Android implementations.
     */
    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();

    //    public ThreadPool() {
//        super(NUMBER_OF_CORES, NUMBER_OF_CORES * 2, KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT, new LinkedBlockingQueue<Runnable>());
//    }
    private static ThreadPoolExecutor instance = null;

    /**
     * @return a singleton of ThreadPool
     */
    public static ThreadPoolExecutor getTPE() {
        if (instance == null) {
            instance = new ThreadPoolExecutor(NUMBER_OF_CORES * 4, NUMBER_OF_CORES * 8, KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT, new LinkedBlockingQueue<Runnable>());
            instance.setThreadFactory(new ExceptionCatchingThreadFactory(instance.getThreadFactory()));
        }
        return instance;
    }

    private static class ExceptionCatchingThreadFactory implements ThreadFactory {
        private final ThreadFactory delegate;

        private ExceptionCatchingThreadFactory(ThreadFactory delegate) {
            this.delegate = delegate;
        }

        public Thread newThread(final Runnable r) {
            Thread t = delegate.newThread(r);
            t.setUncaughtExceptionHandler(ErrorHandler.getInstance());
            return t;
        }
    }
}
