package com.favant.util;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by Bear on 8/22/2015.
 */
public class Screen {

    public static Point getSize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    public static int getWidth(Context context) {
        return getSize(context).x;
    }

    public static int getHeight(Context context) {
        return getSize(context).y;
    }
}
