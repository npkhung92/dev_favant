package com.favant.util;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by Bear on 8/22/2015.
 */
public class Media3Resizer {
    private static final String domain = "https://media3.scdn.vn/";
//    private static final String TAG = "Media3Resizer";
    public static String salt = "268b63f6e";
    private static AllowSize allowSize = new AllowSize();

    public static String validateUrl(String raw_url) {
        if (raw_url == null) return "";
        if (raw_url.startsWith("/")) {
            raw_url = raw_url.substring(1);
        }
//        raw_url = raw_url.replaceAll("http://", "https://");
//        raw_url = raw_url.replaceAll("^//", "https://");
        if (!raw_url.contains(domain) && !raw_url.contains("http")) {
            raw_url = domain + raw_url;
        }
        return raw_url;
    }

    private static String removeOldResize(String url) {
        String nurl;
        nurl = url.replaceAll("_simg_[a-f0-9]+_[0-9]+x[0-9]+_maxb", "");
        nurl = nurl.replaceAll("_simg_[a-f0-9]+_[0-9]+x[0-9]+_max", "");
        return nurl;
    }

    private static String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return "";
    }

    private static String getToken(int width, int height, String method, String salt) {
        String input_checkum = "" + height + width + method + salt;
        String md5 = MD5(input_checkum);
        if (md5.length() < 6) return null;
        return md5.substring(0, 6);
    }

    public static String getResizedUrl(Context context, ResizeOption option) {
        Size s = allowSize.getAllowedSize(context, option.w, option.h);
        int width = s.width;
        int height = s.height;

        String url = validateUrl(option.url);
        url = removeOldResize(url);
        String[] url_part = url.split("\\.(?=[^\\.]+$)");
        if (url_part.length != 2) return null;
        String file_name = url_part[0];
        String ext = url_part[1];
        String checksum = getToken(width, height, option.method, salt);
        if (checksum == null) return null;
        url = file_name + "_simg_" + checksum + "_" + width + "x" + height + "_" + option.method + "." + ext;
        return url;
    }

    public static class ResizeOption {
        private final String url;
        private final int w;
        private final int h;
        private String method = "maxb";

        public ResizeOption(String url, int w, int h) {
            this.url = url;
            this.w = w;
            this.h = h;
        }
    }

    private static class Size {
        public int width = 0;
        public int height = 0;

        public Size(int w, int h) {
            width = w;
            height = h;
        }
    }

    private static class AllowSize {
        private ArrayList<Size> allow_sizes = new ArrayList<Size>();

        private AllowSize() {
            // for product
            allow_sizes.add(new Size(100, 100));
            allow_sizes.add(new Size(150, 150));
            allow_sizes.add(new Size(240, 240));
            allow_sizes.add(new Size(360, 360));
            allow_sizes.add(new Size(540, 540));
            allow_sizes.add(new Size(720, 720));
//            allow_sizes.add(new Size(1080, 1080));
            // for shop logo
            allow_sizes.add(new Size(300, 150));
        }

        public Size getAllowedSize(Context c, int w, int h) {
            if (c != null) {
                if (Screen.getWidth(c) > 720) {
                    int nw = w * 720 / Screen.getWidth(c);
                    h = h * nw / w;
                    w = nw;
                }
            }
            int nw = 0, nh = 0;
            float min = Float.MAX_VALUE;
            for (Size s : allow_sizes) {
                if (Math.abs(s.width / s.height - w / h) <= 0.001) {
                    if (Math.abs(s.width * s.height - w * h) < min) {
                        nw = s.width;
                        nh = s.height;
                        min = Math.abs(s.width * s.height - w * h);
                    }
                }
            }
            if (nw != 0 && nh != 0) return new Size(nw, nh);
            return new Size(w, h);
        }
    }

}
