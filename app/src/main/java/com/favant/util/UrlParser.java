package com.favant.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.favant.model.PageOption;
import com.favant.router.Router;
import com.favant.router.RouterInterface;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Hung on 11/3/2015.
 */
public class UrlParser {
    private final static boolean DEBUG = true;
    private final static String TAG = "Favant_UrlParser";
    private final RouterInterface router;
    private final Context context;

    private boolean page_animation = true;

    public UrlParser(Context context, RouterInterface router) {
        this.context = context;
        this.router = router;
    }

    public boolean isHomePage(String url) {
        url = url.replace("sendo://", "http://");

        if (url.matches("^(http|https)://(www\\.)?sendo\\.vn/?")) return true;
        return false;
    }

    public boolean parseAndGoToPage(String url, boolean stop_on_web_page) {
        return parseAndGoToPage(url, stop_on_web_page, new Bundle());
    }

    public boolean parseAndGoToPage(String url, boolean stop_on_web_page, Bundle data) {
        if (data == null) data = new Bundle();
        url = url.replace("sendo://", "http://");
        String domain_match = "^(http|https)://[^/]+/";
        int current_group = 1;
        if (!url.contains("http") && !url.contains("intent://") && !url.matches(domain_match)) {
            domain_match = "^/";
            if (!url.startsWith("/")) url = "/" + url;
            current_group = 0;
        }
        Log.v(TAG, url);
        // HOMEPAGE
        if (url.matches(domain_match + "/?$")) {
            router.gotoHomepage();
        }
        if (url.contains("intent://")) {
            try {
                Log.v(TAG, "open intent: " + url);
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + url.replace("intent://", "")));
                context.startActivity(myIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }

            router.goBack();
        }
        // SEARCH
        if (url.matches(domain_match + "tim-kiem.*")) {
            data.putString("url", url);

            router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_SEARCH).setData(data));
            return true;
        }
        if (url.matches(domain_match + "[a-zA-z-]+.htm.*")) {
            data.putString("url", url);

            router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_SEARCH).setData(data));
            return true;
        }
        // PROMOTION
        if (url.matches(domain_match + "khuyen-mai.*")) {
            data.putString("url", url);

            router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_PROMOTION).setData(data));
            return true;
        }
        // DEAL
        if (url.matches(domain_match + "deal.*")) {
            data.putString("url", url);

            router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_DEAL).setData(data));
            return true;
        }

        // CHAT
        if (url.matches(domain_match + "chat.*")) {
            Pattern p = Pattern.compile(domain_match + "chat/chat_id=([0-9]+)&user_jid=([a-zA-Z0-9]+)/?");
            Matcher m = p.matcher(url);
            if (m.find()) {
                String chat_id_str = m.group(current_group + 1);

                Log.v(TAG, "chat_id_str=" + chat_id_str);

                long chat_id = Long.parseLong(chat_id_str);

                String user_jid = m.group(current_group + 2);

                data.putLong("chat_id", chat_id);
                data.putString("to_user_jid", user_jid);

                router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_CHAT).setData(data));
            }
            return true;
        }
        // DEAL
        if (url.matches(domain_match + "deal.*")) {
            data.putString("url", url);

            router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_DEAL).setData(data));
            return true;
        }
        if (url.matches(domain_match + "hot-deal.*")) {
            data.putString("url", url);

            router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_DEAL).setData(data));
            return true;
        }
        // SHOP PRODUCT
        if (url.matches(domain_match + "shop/[a-zA-Z-]+/san-pham/[^/]+-[0-9]+/?.*$")) {
            Pattern p = Pattern.compile(domain_match + "shop/[a-zA-Z-]+/san-pham/[^/]+-([0-9]+)/?.*$");
            Matcher m = p.matcher(url);
            if (m.find()) {
                String product_id_str = m.group(current_group + 1);
                int product_id = Integer.parseInt(product_id_str);

                data.putInt("id", product_id);
                data.putString("url", url);


                router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_DETAIL).setData(data));
                return true;
            }
        }
        // SHOP LISTING
        if (url.matches(domain_match + "shop/[a-zA-Z-]+/?.*")) {
            Pattern p = Pattern.compile(domain_match + "shop/([a-zA-Z-]+)/?");
            Matcher m = p.matcher(url);
            if (m.find()) {
                String shop_username = m.group(current_group + 1);

                data.putString("shop_username", shop_username);

                router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_SHOP_LISTING_PRODUCT).setData(data));
            }
            return true;
        }
        // BRAND
        if (url.matches(domain_match + "thuong-hieu/[a-z0-9-]+/?.*")) {
            Pattern p = Pattern.compile(domain_match + "thuong-hieu/([a-z0-9-]+)/?.*");
            Matcher m = p.matcher(url);
            if (m.find()) {
                String brand_key = m.group(current_group + 1);

                data.putString("brand_key", brand_key);

                router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_BRAND_DETAIL).setData(data));
                return true;
            }
        }
        //NOTIFICATION

        // PROFILE EDIT
        if (url.matches(domain_match + "thong-tin-tai-khoan/?")) {
            router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_PROFILE_EDIT));
            return true;
        }
        // PROFILE ADDRESS
        if (url.matches(domain_match + "thong-tin-tai-khoan/dia-chi-nhan-hang/?")) {
            router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_PROFILE_ADDRESS));
            return true;
        }
        // PROFILE FAVORITE PRODUCT
        if (url.matches(domain_match + "thong-tin-tai-khoan/san-pham-quan-tam/?")) {
            router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_PROFILE_FAVORITE_SHOP_PRODUCT));
            return true;
        }
        // PROFILE LOYALTY
        if (url.matches(domain_match + "thong-tin-tai-khoan/vi-sen/?")) {
            router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_PROFILE_LOYALTY));
            return true;
        }
        // PROFILE FAVORITE SHOP
        if (url.matches(domain_match + "customer/favorites/shop/?")) {
            data.putString("page_type", "shop");

            router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_PROFILE_FAVORITE_SHOP_PRODUCT).setData(data));
            return true;
        }
        // PROFILE HOI DAP
        if (url.matches(domain_match + "thong-tin-tai-khoan/hoi-dap/?")) {
            router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_PROFILE_COMMENT));
            return true;
        }
        // PROFILE ORDER LIST
        if (url.matches(domain_match + "sales/order/history/?")) {
            router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_PROFILE_ORDER));
            return true;
        }
        // PROFILE ORDER DETAIL
        if (url.matches(domain_match + "sales/order/view/order_id/[0-9]+/?")) {
            Pattern p = Pattern.compile(domain_match + "sales/order/view/order_id/([0-9]+)/?$");
            Matcher m = p.matcher(url);
            if (m.find()) {
                String order_id_str = m.group(current_group + 1);
                int order_id = Integer.parseInt(order_id_str);

                data.putInt("order_id", order_id);

                router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_PROFILE_ORDER_DETAIL).setData(data));
                return true;
            }
        }
        // PRODUCT DETAIL
        if (url.matches(domain_match + "[a-zA-Z-]+/[a-zA-Z-]+/[a-zA-Z-]+/[^/]+-[0-9]+/?.*$")) {
            Pattern p = Pattern.compile(domain_match + "[^/]+/[^/]+/[^/]+/[^/]+-([0-9]+)/?.*$");
            Matcher m = p.matcher(url);
            if (m.find()) {
                String product_id_str = m.group(current_group + 1);
                int product_id = Integer.parseInt(product_id_str);

                data.putInt("id", product_id);
                data.putString("url", url);


                router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_DETAIL).setData(data));
                return true;
            }
        }
        // PRODUCT DETAIL
        if (url.matches(domain_match + "san-pham/[^/]+-[0-9]+/?.*$")) {
            Pattern p = Pattern.compile(domain_match + "san-pham/[^/]+-([0-9]+)/?.*$");
            Matcher m = p.matcher(url);
            if (m.find()) {
                String product_id_str = m.group(current_group + 1);
                int product_id = Integer.parseInt(product_id_str);

                data.putInt("id", product_id);
                data.putString("url", url);


                router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_DETAIL).setData(data));
                return true;
            }
        }
        // LISTING 3
        if (url.matches(domain_match + "[a-zA-Z-]+/[a-zA-Z-]+/[a-zA-Z-]+/?.*")) {
            Pattern p = Pattern.compile(domain_match + "([a-zA-Z-]+/[a-zA-Z-]+/[a-zA-Z-]+/?)");
            Matcher m = p.matcher(url);
            if (m.find()) {
                String cat_path = m.group(current_group + 1);
                String cat_path_with_slash = "", cat_path_without_slash = "";

                if (!cat_path.endsWith("/")) {
                    cat_path_with_slash = cat_path + "/";
                    cat_path_without_slash = cat_path;
                } else {
                    cat_path_with_slash = cat_path;
                    cat_path_without_slash = cat_path.substring(0, cat_path.length() - 1);
                }

//                Category c = CategoryRepository.getInstance().getCategoryByUrlPath(mMasterActivity, cat_path_with_slash);
//                if (c == null)
//                    c = CategoryRepository.getInstance().getCategoryByUrlPath(mMasterActivity, cat_path_without_slash);
//                if (c != null) {
//                    Log.v(TAG, "found cat lv 3: " + c);
//                    int cat_path_id = c.category_id;
//
//                    data.putString("url", url);
//                    data.putInt("category_id", cat_path_id);
//
//                    mMasterActivity.addPage(new PageOption().setPageType(MasterActivity.PAGE_TYPE_LISTING).setData(data));
//                    mMasterActivity.gotoNextPage(page_animation, false);
//                    return true;
//                }
            }
        }
        // LISTING 2
        if (url.matches(domain_match + "[a-zA-Z-]+/[a-zA-Z-]+/?.*")) {
            Pattern p = Pattern.compile(domain_match + "([a-zA-Z-]+/[a-zA-Z-]+/?)");
            Matcher m = p.matcher(url);
            if (m.find()) {
                String cat_path = m.group(current_group + 1);

                String cat_path_with_slash = "", cat_path_without_slash = "";
                if (!cat_path.endsWith("/")) {
                    cat_path_with_slash = cat_path + "/";
                    cat_path_without_slash = cat_path;
                } else {
                    cat_path_with_slash = cat_path;
                    cat_path_without_slash = cat_path.substring(0, cat_path.length() - 1);
                }

//                Category c = CategoryRepository.getInstance().getCategoryByUrlPath(mMasterActivity, cat_path_with_slash);
//                if (c == null)
//                    c = CategoryRepository.getInstance().getCategoryByUrlPath(mMasterActivity, cat_path_without_slash);
//                if (c != null) {
//                    Log.v(TAG, "found cat lv 2: " + c);
//                    int cat_path_id = c.category_id;
//
//                    data.putString("url", url);
//                    data.putInt("category_id", cat_path_id);
//
//                    mMasterActivity.addPage(new PageOption().setPageType(MasterActivity.PAGE_TYPE_LISTING).setData(data));
//                    mMasterActivity.gotoNextPage(page_animation, false);
//                    return true;
//                }
            }
        }
        // LISTING 1
        if (url.matches(domain_match + "[a-zA-Z-]+/?.*")) {
            Pattern p = Pattern.compile(domain_match + "([a-zA-Z-]+/?)");
            Matcher m = p.matcher(url);
            if (m.find()) {
                String cat_path = m.group(current_group + 1);
                if (!cat_path.endsWith("/")) cat_path += "/";

//                ArrayList<VisualCategory.CateObject> main_category = new VisualCategory().getMainCategory();
//
//                for (VisualCategory.CateObject c : main_category) {
//                    if (c.url_key.equals(cat_path) || (c.url_key + "/").equals(cat_path)) {
//                        int[] c_ids = new int[c.category_id.length];
//                        for (int i = 0; i < c.category_id.length; i++)
//                            c_ids[i] = c.category_id[i];
//
//                        data.putIntArray("category_id", c_ids);
//
//                        mMasterActivity.addPage(new PageOption().setPageType(MasterActivity.PAGE_TYPE_CATEGORY).setData(data));
//                        mMasterActivity.gotoNextPage();
//                        return true;
//                    }
//                }
            }
        }
        if (!stop_on_web_page) {
            data.putString("url", url);

            router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_WEBVIEW).setData(data));
        }
        return false;
    }

    public void setPageAnimation(boolean b) {
        page_animation = b;
    }
}
