package com.favant.util;

import android.util.Log;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Bear on 9/15/2015.
 */
public class Net {
    private final static boolean DEBUG = false;
    private final static String TAG = "Favant_net";
    private static ThreadPoolExecutor mThreadPool;

    static {
        mThreadPool = new ThreadPoolExecutor(8, 8, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
    }

    public static void get(final String url, final ArrayList<NameValuePair> headers, final boolean cb_on_main_thread, final StringResult cb) {
        if (DEBUG) Log.v(TAG, "create thread for getting net: " + url);
        mThreadPool.execute(new Runnable() {
            @Override
            public void run() {

                OkHttpClient okHttpClient = new OkHttpClient();
                String is;

                try {
                    Request.Builder builder = new Request.Builder()
                            .url(url)
                            .addHeader("User-Agent", System.getProperty("http.agent"));

                    if (headers != null)
                        for (NameValuePair nvp : headers)
                            builder.addHeader(nvp.getName(), nvp.getValue());

                    is = okHttpClient.newCall(builder.build()).execute().body().string();

                    Log.v(TAG, url + " => " + is);
                } catch (IOException e) {
                    callback(null, e);
                    return;
                }
                callback(is, null);
            }

            public void callback(final Object o, final Exception e) {
                if (cb == null) return;

                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        if (o == null) cb.error(e);
                        else cb.success((String) o);
                    }
                };
                if (cb_on_main_thread) MainHandler.post(r);
                else r.run();
            }
        });
    }

    public static void getJson(final String url, ArrayList<NameValuePair> headers, final boolean cb_on_main_thread, final JsonResult cb) {
        get(url, headers, false, new StringResult() {
            @Override
            public void success(String res) {
                JSONException e1, e2;
                boolean success = false;
                try {
                    long start_time = System.currentTimeMillis();
                    final JSONObject jObj = new JSONObject(res);
                    long end_time = System.currentTimeMillis();
                    Log.v(TAG, "get " + url + " in " + (end_time - start_time) + " ms");
                    success = true;
                    callback(jObj, null);
                } catch (JSONException e) {
                    e1 = e;
                }

                try {
                    long start_time = System.currentTimeMillis();
                    final JSONArray jObj = new JSONArray(res);
                    long end_time = System.currentTimeMillis();
                    Log.v(TAG, "get " + url + " in " + (end_time - start_time) + " ms");
                    success = true;
                    callback(jObj, null);
                } catch (JSONException e) {
                    e2 = e;
                }
                if (!success) callback(null, new Exception("cant parse json for: " + res));
            }

            @Override
            public void error(Exception e) {
                callback(null, e);
            }

            public void callback(final Object o, final Exception e) {
                if (cb == null) return;

                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        if (o == null) cb.error(e);
                        else cb.success(o);
                    }
                };
                if (cb_on_main_thread) MainHandler.post(r);
                else r.run();
            }
        });
    }

    public static void post(final String url, final ArrayList<NameValuePair> headers, final List<NameValuePair> params, final HttpEntity entity, final boolean cb_on_main_thread, final StringResult cb) {
        if (DEBUG) Log.v(TAG, "create thread for posting net: " + url);
        mThreadPool.execute(new Runnable() {
            @Override
            public void run() {
                InputStream is = null;
                try {
                    // defaultHttpClient
                    DefaultHttpClient httpClient = new DefaultHttpClient();
                    httpClient.getParams().setParameter(CoreProtocolPNames.USER_AGENT,
                            System.getProperty("http.agent"));
                    HttpPost httpPost = new HttpPost(url);
                    if (headers != null)
                        for (NameValuePair nvp : headers)
                            httpPost.addHeader(nvp.getName(), nvp.getValue());

                    if (entity == null)
                        httpPost.setEntity(new UrlEncodedFormEntity(params));
                    else httpPost.setEntity(entity);
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                    HttpEntity httpEntity = httpResponse.getEntity();
                    is = httpEntity.getContent();

                } catch (UnsupportedEncodingException e) {
                    callback(null, e);
                    return;
                } catch (ClientProtocolException e) {
                    callback(null, e);
                    return;
                } catch (IOException e) {
                    callback(null, e);
                    return;
                }

                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                    if (DEBUG) Log.v(TAG, "got result for " + url + ": " + sb);
                    is.close();
                    final String result = sb.toString();
                    callback(result, null);
                } catch (Exception e) {
                    callback(null, e);
                    return;
                }
            }

            public void callback(final Object o, final Exception e) {
                if (cb == null) return;

                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        if (o == null) cb.error(e);
                        else cb.success((String) o);
                    }
                };
                if (cb_on_main_thread) MainHandler.post(r);
                else r.run();
            }
        });
    }

    public static void postJson(String url, ArrayList<NameValuePair> headers, List<NameValuePair> params, HttpEntity entity, final boolean cb_on_main_thread, final JsonResult cb) {
        post(url, headers, params, entity, false, new StringResult() {
            @Override
            public void success(final String res) {
                JSONException e1, e2;
                boolean success = false;
                try {
                    final JSONObject jObj = new JSONObject(res);
                    success = true;
                    callback(jObj, null);
                } catch (JSONException e) {
                    e1 = e;
                }

                try {
                    final JSONArray jObj = new JSONArray(res);
                    success = true;
                    callback(jObj, null);
                } catch (JSONException e) {
                    e2 = e;
                }
                if (!success) callback(null, new Exception("cant parse json for: " + res));
            }

            @Override
            public void error(Exception e) {
                callback(null, e);
            }

            public void callback(final Object o, final Exception e) {
                if (cb == null) return;

                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        if (o == null) cb.error(e);
                        else cb.success(o);
                    }
                };
                if (cb_on_main_thread) MainHandler.post(r);
                else r.run();
            }
        });
    }

    public static interface StringResult {
        public void success(String res);

        public void error(Exception e);
    }

    public static interface JsonResult {
        // Can be JSONArray or JSONObject
        public void success(Object res);

        public void error(Exception e);
    }

    public static abstract class JsonResultAdapter implements JsonResult {
        @Override
        public void success(Object res) {

        }

        @Override
        public void error(Exception e) {

        }
    }
}
