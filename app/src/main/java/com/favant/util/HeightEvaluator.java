package com.favant.util;

import android.animation.IntEvaluator;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Bear on 9/15/2015.
 */
public class HeightEvaluator extends IntEvaluator {
    private View v;

    public HeightEvaluator(View v) {
        this.v = v;
    }

    @NonNull
    @Override
    public Integer evaluate(float fraction, Integer startValue, Integer endValue) {
        int num = (Integer) super.evaluate(fraction, startValue, endValue);
        ViewGroup.LayoutParams params = v.getLayoutParams();
        params.height = num;
        v.setLayoutParams(params);
        return num;
    }

}
