package com.favant.util;

import android.view.View;

/**
 * Created by Bear on 8/22/2015.
 */
public class ViewChecker {
    public static boolean validate(View v, Class<?> cls) {
        if (v != null && cls.isInstance(v)) return true;
        return false;
    }
}
