package com.favant.util;

import android.view.animation.Animation;

/**
 * Created by Hung on 10/17/2015.
 */
public class AnimationListenerAdapter implements Animation.AnimationListener {
    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
