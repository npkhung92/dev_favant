package com.favant.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Hung on 3/13/2016.
 */
public class Currency {

    public static String formatCurrency(float number) {
        Locale locale = new Locale("en", "US");
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getNumberInstance(locale);
        formatter.applyPattern("###,###,###");
        return formatter.format(number);
    }

    public static String formatCurrency(long number) {
        Locale locale = new Locale("en", "US");
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getNumberInstance(locale);
        formatter.applyPattern("###,###,###");
        return formatter.format(number);
    }

    public static String formatVNCurrency(float number) {
        Locale locale = new Locale("de", "DE");
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getNumberInstance(locale);
        formatter.applyPattern("###,###,###");
        return formatter.format(number);
    }
}
