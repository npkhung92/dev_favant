package com.favant.util;

import android.os.Looper;

import com.favant.MasterActivity;

/**
 * Created by Bear on 8/25/2015.
 */
public class ErrorHandler implements Thread.UncaughtExceptionHandler {
    private static ErrorHandler mErrorHandler = null;
    private final MasterActivity mMasterActivity;

    private ErrorHandler(MasterActivity ma) {
        mMasterActivity = ma;
    }

    public static ErrorHandler create(MasterActivity masterActivity) {
        if (mErrorHandler == null) mErrorHandler = new ErrorHandler(masterActivity);
        return mErrorHandler;
    }

    public static ErrorHandler getInstance() {
        return mErrorHandler;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {

        boolean is_fatal = false;
        if (Looper.myLooper() == Looper.getMainLooper()) {
            is_fatal = true;
        }

    }
}
