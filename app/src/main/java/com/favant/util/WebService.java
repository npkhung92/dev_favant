package com.favant.util;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bear on 8/26/2015.
 */
public class WebService {


    public static String domain = "http://test.favant.com/";
//    public static String domain = "https://www.sendo.com/";

    public static void getJson(String uri, Net.JsonResult cb) {
        ArrayList<NameValuePair> headers = new ArrayList<NameValuePair>();
//        String access_token = Profile.getInstane().getAccessToken();
//        if (access_token == null) access_token = "";
//        headers.add(new BasicNameValuePair("Authorization", access_token));
        Net.getJson(domain + uri, headers, true, cb);
    }

    public static void getJsonOnThread(String uri, Net.JsonResult cb) {
        ArrayList<NameValuePair> headers = new ArrayList<NameValuePair>();
//        String access_token = Profile.getInstane().getAccessToken();
//        if (access_token == null) access_token = "";
//        headers.add(new BasicNameValuePair("Authorization", access_token));
        Net.getJson(domain + uri, headers, false, cb);
    }

    public static void postJson(String uri, List<NameValuePair> params, Net.JsonResult cb) {
        ArrayList<NameValuePair> headers = new ArrayList<NameValuePair>();
//        String access_token = Profile.getInstane().getAccessToken();
//        if (access_token == null) access_token = "";
//        headers.add(new BasicNameValuePair("Authorization", access_token));
        Net.postJson(domain + uri, headers, params, null, true, cb);
    }

    public static void postJsonOnThread(String uri, List<NameValuePair> params, Net.JsonResult cb) {
        ArrayList<NameValuePair> headers = new ArrayList<NameValuePair>();
//        String access_token = Profile.getInstane().getAccessToken();
//        if (access_token == null) access_token = "";
//        headers.add(new BasicNameValuePair("Authorization", access_token));
        Net.postJson(domain + uri, headers, params, null, false, cb);
    }

    public static void postJson(String uri, HttpEntity params, Net.JsonResult cb) {
        ArrayList<NameValuePair> headers = new ArrayList<NameValuePair>();
//        String access_token = Profile.getInstane().getAccessToken();
//        if (access_token == null) access_token = "";
//        headers.add(new BasicNameValuePair("Authorization", access_token));
        Net.postJson(domain + uri, headers, null, params, true, cb);
    }

    public static void postJsonOnThread(String uri, HttpEntity params, Net.JsonResult cb) {
        ArrayList<NameValuePair> headers = new ArrayList<NameValuePair>();
//        String access_token = Profile.getInstane().getAccessToken();
//        if (access_token == null) access_token = "";
//        headers.add(new BasicNameValuePair("Authorization", access_token));
        Net.postJson(domain + uri, headers, null, params, false, cb);
    }

    public static void postString(String uri, HttpEntity params, Net.StringResult cb) {
        ArrayList<NameValuePair> headers = new ArrayList<NameValuePair>();
//        String access_token = Profile.getInstane().getAccessToken();
//        if (access_token == null) access_token = "";
//        headers.add(new BasicNameValuePair("Authorization", access_token));
        Net.post(domain + uri, headers, null, params, true, cb);
    }
}
