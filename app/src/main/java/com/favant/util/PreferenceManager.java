package com.favant.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by Hung on 11/24/2015.
 */
public class PreferenceManager {
    public static final String SHARED_PREFERENCE_KEY = "favant";
    private static final String TAG = "PreferenceManager";

    private final Context context;
    private final Object owner;

    public PreferenceManager(Context context, Object owner) {
        this.context = context;
        this.owner = owner;
    }

    private String getPreferenceKey(String key) {
        return owner.getClass().getName() + "|" + key;
    }

    public boolean saveToPreference(String key, String value) {
        if (context == null) return false;
        SharedPreferences sharedPref = context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getPreferenceKey(key), value);
        editor.commit();

        Log.v(TAG, getPreferenceKey(key) + " => " + value);
        return true;
    }

    public String getStringFromPreference(String key, String default_value) {
        if (context == null) return default_value;
        SharedPreferences sharedPref = context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_MULTI_PROCESS);
        String value = sharedPref.getString(getPreferenceKey(key), default_value);
        return value;
    }

    public String getStringFromPreference(String key) {
        return getStringFromPreference(key, null);
    }
}
