package com.favant.collection;

import android.util.Log;

import com.favant.model.FCategory;
import com.favant.util.Net;
import com.favant.util.WebService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASUS on 2/28/2016.
 */
public class CategoryCollection extends Collection {
    private static final String TAG = "CategoryCollection";

    public static CategoryCollection instance;
    private List<FCategory> categories = new ArrayList<>();

    public static CategoryCollection getInstance() {
        if (instance == null) instance = new CategoryCollection();
        return instance;
    }

    public static void clearInstance() {
        instance = null;
    }

    public void getAllCategory(final AllCategoryCallback allCategoryCallback) {
        WebService.getJson("service/category/getAllCategoryApp/", new Net.JsonResultAdapter() {
            @Override
            public void success(Object res) {
                if (res instanceof JSONArray) {
                    JSONArray array = (JSONArray) res;
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.optJSONObject(i);
                        Type object_type = new TypeToken<FCategory>() {
                        }.getType();
                        Gson gson = new Gson();
                        FCategory category = gson.fromJson(object.toString(), object_type);
                        Log.v(TAG, category.toString());
                        categories.add(category);
                    }

                    if (allCategoryCallback != null) allCategoryCallback.allCategory(categories);
                }
            }
        });
    }

    public interface AllCategoryCallback {
        void allCategory(List<FCategory> categories);

        void stringArray(String[] category_names);

    }

    public abstract static class AllCategoryCallbackAdapter implements AllCategoryCallback {
        @Override
        public void allCategory(List<FCategory> categories) {
        }

        @Override
        public void stringArray(String[] category_names) {
        }
    }

    public void getCategoryNameAsString(AllCategoryCallback cb) {
        ArrayList<String> category_name_list = new ArrayList<String>();
        for (FCategory category : categories) {
            category_name_list.add(category.getName());
        }
        String[] category_names = new String[category_name_list.size()];
        category_name_list.toArray(category_names);
        if (cb != null) cb.stringArray(category_names);
    }
}
