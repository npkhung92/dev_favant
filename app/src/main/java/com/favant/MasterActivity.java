package com.favant;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;

import com.favant.fview.common.FEditText;
import com.favant.fview.common.FImageView;
import com.favant.fview.common.FMenuRight;
import com.favant.fview.common.FPopup;
import com.favant.model.PageOption;
import com.favant.router.Router;
import com.favant.router.RouterInterface;
import com.favant.util.ConnectionDetector;
import com.favant.util.MainHandler;
import com.favant.util.Screen;
import com.favant.util.UrlParser;

import java.util.concurrent.atomic.AtomicInteger;

public class MasterActivity extends FragmentActivity {

    public static final String TAG = "Favant_MasterActivity";
    public static final int ANIMATION_DURATION = 400;
    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);
    private static MasterActivity mMasterActivity;
    private static Context mContext;
    public int status_bar_height;
    public int keyboard_height;
    // Connection detector
    ConnectionDetector cd;
    //    private FLeftNavigationDrawer left_navi_drawer;
    private ViewGroup master_layout;
    private boolean openFromDeepLink;
    private boolean open_from_notification = false;
    private FPopup popup;
    private FMenuRight menu_right;
    private Router router;

    public static MasterActivity getInstance() {
        return mMasterActivity;
    }

    public static Context getContext() {
        return mContext;
    }

    private void init() {
        FImageView.initImageLoader(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        init();
        super.onCreate(savedInstanceState);
        mMasterActivity = this;
        setContentView(R.layout.screen_slider);
        master_layout = (ViewGroup) ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);

        final Window mRootWindow = getWindow();
        View mRootView = mRootWindow.getDecorView().findViewById(android.R.id.content);
        mRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                Rect r = new Rect();
                View view = mRootWindow.getDecorView();
                view.getWindowVisibleDisplayFrame(r);
                keyboard_height = master_layout.getHeight() - (r.bottom - r.top);
                status_bar_height = Screen.getHeight(MasterActivity.this) - master_layout.getHeight();
                // r.left, r.top, r.right, r.bottom
            }
        });
        popup = (FPopup) findViewById(R.id.popup_wrapper);

        router = new Router(this, getSupportFragmentManager(), popup, (FEditText) findViewById(R.id.keyboard_handler));

//        left_navi_drawer = (FLeftNavigationDrawer) findViewById(R.id.left_navi_wrapper);

        String url = getIntent().getStringExtra("url");
        // url from browser
        Uri data = getIntent().getData();
        if (data != null) {
            url = data.toString();
            openFromDeepLink = true;
        }
        UrlParser up = new UrlParser(this, router);
        if (url != null && !up.isHomePage(url)) {
            Bundle extra = getIntent().getBundleExtra("extra");

            up.setPageAnimation(false);
            up.parseAndGoToPage(url, false, extra);

            open_from_notification = true;
        } else {
            Log.v(TAG, "check");
            router.addPage(new PageOption().setPageType(Router.PAGE_TYPE_LISTING));
//            addPage(new PageOption().setPageType(PAGE_TYPE_HOMEPAGE).setAutoLoadContent(true).setAutoloadOnCreate(true));
        }
        initGCM();
    }

    @Override
    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);
        final String url = intent.getStringExtra("url");
        Log.v(TAG, String.valueOf(url));
        if (url != null) {
            MainHandler.postDelay(new Runnable() {
                @Override
                public void run() {
                    Bundle extra = intent.getBundleExtra("extra");

                    UrlParser up = new UrlParser(MasterActivity.this, router);
                    up.parseAndGoToPage(url, false, extra);
                }
            }, 500);
        }
    }

    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the registration ID in your app is up to you.
        return getSharedPreferences(MasterActivity.class.getName(),
                Context.MODE_PRIVATE);
    }

    @Override
    public void onPause() {
        super.onPause();
//        isResumed = false;

    }

    @Override
    protected void onDestroy() {
//        if (mRegisterTask != null) {
//            mRegisterTask.cancel(true);
//        }
//        try {
//            unregisterReceiver(mHandleMessageReceiver);
//            GCMRegistrar.onDestroy(this);
//        } catch (Exception e) {
//            Log.e("UnRegister Receiver ", "> " + e.getMessage());
//        }
        super.onDestroy();
    }

    public void initGCM() {
        cd = new ConnectionDetector(getApplicationContext());
    }

    public synchronized void cleanUnusedFragmentRight() {
//        if (pageList.size() >= CURRENT_PAGE + 1) {
//            for (int i = CURRENT_PAGE + 1; i < pageList.size(); i++) {
//                getSupportFragmentManager().beginTransaction().remove(pageList.get(i).getFragment()).commitAllowingStateLoss();
//                master_layout.removeView(pageList.get(i).fragment_wrapper);
//                pageList.remove(i);
//            }
//        }
    }

    public synchronized void cleanUnusedFragmentLeft() {
    }

    @Override
    public void onBackPressed() {
        router.onBackPressed(openFromDeepLink, open_from_notification);
    }

    public View getMasterLayout() {
        return master_layout;
    }

    public int getKeyboardHeight() {
        return keyboard_height;
    }

    public int getContentHeight() {
        return master_layout.getHeight() - keyboard_height;
    }

    public RouterInterface getRouter() {
        return router;
    }
}
