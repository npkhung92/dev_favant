package com.favant.router;

import android.os.Bundle;

import com.favant.model.PageOption;

/**
 * Created by ASUS on 2/20/2016.
 */
public interface RouterInterface {

    void addPage(PageOption po);

    void goBack(Bundle data, boolean hide_keyboard);

    void goBack();

    void removePage(int position);

    void gotoHomepage();

    void gotoHomepage(Bundle data);
}
