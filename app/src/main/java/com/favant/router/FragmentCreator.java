package com.favant.router;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.favant.MasterActivity;
import com.favant.fview.common.FFragment;
import com.favant.fview.common.FPopup;
import com.favant.fview.detail.DetailFragment;
import com.favant.fview.home.HomeFragment2.HomeFragment2;
import com.favant.fview.listing.ListingFragment;

/**
 * Created by ASUS on 2/20/2016.
 */
public class FragmentCreator {

    private final MasterActivity masterActivity;
    private final FPopup popup;
    private final Router router;

    public FragmentCreator(MasterActivity masterActivity, Router router, FPopup popup) {
        this.masterActivity = masterActivity;
        this.router = router;
        this.popup = popup;
    }

    public Fragment createFragment(String page_type, Bundle extra, int pagePosition) {
        Fragment page = null;
        if (page_type.equals(Router.PAGE_TYPE_NULL)) page = new Fragment();
        if (page_type.equals(Router.PAGE_TYPE_HOMEPAGE)) page = new HomeFragment2();
        if (page_type.equals(Router.PAGE_TYPE_LISTING)) page = new ListingFragment();
        if (page_type.equals(Router.PAGE_TYPE_DETAIL)) page = new DetailFragment();
        if (page instanceof FFragment) {
            FFragment fpage = (FFragment) page;
            fpage.setMasterActivity(masterActivity);
            if (extra != null) page.setArguments(extra);
            fpage.setPagePosition(pagePosition);
            fpage.setPopupWrapper(popup);
            fpage.setRouter(router);
//            ((FFragment) page).setLeftNavDrawer(left_navi_drawer);
//            ((FFragment) page).setMenuRight(menu_right);
        }

        return page;
    }
}
