package com.favant.router;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.favant.MasterActivity;
import com.favant.R;
import com.favant.fview.common.FEditText;
import com.favant.fview.common.FFragment;
import com.favant.fview.common.FPopup;
import com.favant.fview.home.HomeFragment;
import com.favant.model.PageOption;
import com.favant.util.ConnectionDetector;
import com.favant.util.MainHandler;

import java.util.ArrayList;

/**
 * Created by ASUS on 2/20/2016.
 */
public class Router implements RouterInterface {

    public static final String PAGE_TYPE_HOMEPAGE = "homepage";
    public static final String PAGE_TYPE_CATEGORY = "category";
    public static final String PAGE_TYPE_NULL = "null";
    public static final String PAGE_TYPE_CART = "cart";
    public static final String PAGE_TYPE_LISTING = "listing";
    public static final String PAGE_TYPE_DETAIL = "detail";
    public static final String PAGE_TYPE_SEARCH = "search";
    public static final String PAGE_TYPE_DEAL = "deal";
    public static final String PAGE_TYPE_PROMOTION = "promotion";
    public static final String PAGE_TYPE_WEBVIEW = "webview";
    public static final String PAGE_TYPE_SHOP_LISTING_PRODUCT = "shop_listing";
    public static final String PAGE_TYPE_SHOP_DETAIL = "shop_detail";
    public static final String PAGE_TYPE_CHECKOUT = "checkout";
    public static final String PAGE_TYPE_CHECKOUT_CART = "checkout_cart";
    public static final String PAGE_TYPE_BRAND = "brand_listing";
    public static final String PAGE_TYPE_BRAND_DETAIL = "brand_detail";
    public static final String PAGE_TYPE_PROFILE_WELCOME = "profile_welcome";
    public static final String PAGE_TYPE_PROFILE_LOGIN_SENDO_ID = "profile_login";
    public static final String PAGE_TYPE_PROFILE_CHECKOUT_LOGIN = "profile_checkout_login";
    public static final String PAGE_TYPE_PROFILE_FORGET_PASSWORD = "profile_forget_password";
    public static final String PAGE_TYPE_PROFILE_REGISTER = "profile_register";
    public static final String PAGE_TYPE_PROFILE_FAVORITE_SHOP_PRODUCT = "favorite_shop";
    public static final String PAGE_TYPE_PROFILE_EDIT = "profile_edit";
    public static final String PAGE_TYPE_PROFILE_ADDRESS = "profile_address";
    public static final String PAGE_TYPE_PROFILE_COMMENT = "profile_comment";
    public static final String PAGE_TYPE_PROFILE_NOTIFICATION = "profile_notification";
    public static final String PAGE_TYPE_PROFILE_ORDER = "profile_order";
    public static final String PAGE_TYPE_PROFILE_ORDER_DETAIL = "profile_order_detail";
    public static final String PAGE_TYPE_PROFILE_LOYALTY = "profile_loyalty";
    public static final String PAGE_TYPE_SETTING_NOTIFICATION = "setting_notification";
    public static final String PAGE_TYPE_CHAT = "chat_page";
    public static final String PAGE_TYPE_IMAGE_FULL = "full_screen_image";
    public static final String PAGE_TYPE_INTRO = "intro";
    public static final String PAGE_TYPE_INVITATION = "invitation";
    private static final String TAG = "Router";
    private final MasterActivity masterActivity;
    private final FPopup popup;
    private final FEditText keyboardHandler;
    private final FragmentManager fragmentManager;
    private final FragmentCreator fragmentCreator;
    public ArrayList<PageData> pageList = new ArrayList<PageData>();

    public Router(MasterActivity masterActivity, FragmentManager fragmentManager, FPopup popup, FEditText keyboardHandler) {
        this.masterActivity = masterActivity;
        this.fragmentManager = fragmentManager;
        this.popup = popup;
        this.keyboardHandler = keyboardHandler;
        fragmentCreator = new FragmentCreator(masterActivity, this, popup);
    }

    @Override
    public void addPage(PageOption po) {
        ConnectionDetector connectionDetector = new ConnectionDetector(masterActivity);
        if (connectionDetector.isConnectingToInternet()
                || po.page_type.equals(PAGE_TYPE_HOMEPAGE) || po.page_type.equals(PAGE_TYPE_CATEGORY)) {
            hideKeyboardForce(null);
            final Fragment page = fragmentCreator.createFragment(po.page_type, po.data, pageList.size());
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            if (pageList.size() >= 1) {
                transaction.setCustomAnimations(R.anim.new_page_enter, R.anim.new_page_exit);
            }
            if (po.isReverse()) {
                transaction.setCustomAnimations(R.anim.go_back_enter, R.anim.go_back_exit);
            }
            transaction.replace(R.id.fragment_container, page);
            transaction.commitAllowingStateLoss();
            if (page instanceof FFragment) {
                MainHandler.postDelay(new Runnable() {
                    @Override
                    public void run() {
                        ((FFragment) page).loadContent();
                        ((FFragment) page).invokeOnCreate();
//                        ((FFragment) page).invokeSendTrackingInformation(false);
                        ((FFragment) page).initKeyboard();
                        popup.setPageView(page.getView());
                    }
                }, masterActivity.getResources().getInteger(R.integer.page_transition_speed) * 2 + 200);
            }
            pageList.add(new PageData(pageList.size(), po.page_type, po.data, page));
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(masterActivity)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })

                    .create();


            alertDialog.setTitle("Error Network!");

            // Setting Dialog Message
            alertDialog.setMessage("Check your network connection.");
            alertDialog.show();
        }
    }

    @Override
    public void goBack(final Bundle data, final boolean hide_keyboard) {
        hideKeyboardForce(null);
        final Fragment page = pageList.get(pageList.size() - 2).getFragment();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.go_back_enter, R.anim.go_back_exit);
        transaction.replace(R.id.fragment_container, page);
        transaction.commitAllowingStateLoss();
        if (page instanceof FFragment) {
            MainHandler.postDelay(new Runnable() {
                @Override
                public void run() {
                    if (data != null) ((FFragment) page).goBackAction(data);
//                    ((FFragment) page).invokeSendTrackingInformation(true);
                    ((FFragment) page).initKeyboard();
                    popup.setPageView(page.getView());
                }
            }, masterActivity.getResources().getInteger(R.integer.page_transition_speed) + 100);
        }
        pageList.remove(pageList.size() - 1);
    }

    @Override
    public void goBack() {
        goBack(null, true);
    }

    @Override
    public void removePage(final int position) {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                Log.v(TAG, "pageList " + pageList);
                pageList.remove(position);
                int count = -1;
                for (PageData pd : pageList) {
                    count++;
                    pd.position = count;
                    if (pd.fragment instanceof FFragment)
                        ((FFragment) pd.fragment).setPagePosition(count);
                }
            }
        };
        r.run();
    }

    public void hideKeyboardForce(Runnable cb) {
        InputMethodManager imm = (InputMethodManager) masterActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(keyboardHandler, InputMethodManager.SHOW_IMPLICIT);
        imm.hideSoftInputFromWindow(keyboardHandler.getWindowToken(), 0);
        if (cb != null) MainHandler.postDelay(cb, 200);
    }

    public void onBackPressed(boolean openFromDeepLink, boolean openFromNotification) {
        if (openFromDeepLink && pageList.size() < 1) {
            masterActivity.finish();
            return;
        }
        Log.v(TAG, "back pressed");
        PageData pd = pageList.get(pageList.size() - 1);
        Log.v(TAG, "pageList size: " + pageList.size() + ", current_page = " + pageList.size() + "pd: " + pd.getFragment());
        if (pd.getFragment() instanceof FFragment && ((FFragment) pd.getFragment()).handleBackPressed()) {
            Log.v(TAG, "fragment handled backPressed " + pd.getFragment());
        } else if (pageList.size() > 1) {
            goBack();
        } else {
            if (openFromNotification && !(pd.getFragment() instanceof HomeFragment)) {
                addPage(new PageOption().setPageType(PAGE_TYPE_HOMEPAGE).setReverse(true));
                removePage(pd.position);
            } else {
                masterActivity.finish();
            }
        }
    }

    @Override
    public void gotoHomepage() {
        gotoHomepage(null);
    }

    @Override
    public void gotoHomepage(Bundle data) {
        int homepage_position = -1;
        for (int i = 0; i < pageList.size(); i++) {
            if (pageList.get(i).page_type == PAGE_TYPE_HOMEPAGE) {
                homepage_position = i;
                break;
            }
        }

        if (homepage_position != -1) {
            int i = homepage_position + 1;
            while (i < pageList.size() - 1) {
                pageList.remove(i);
            }
//            if (pageList.get(homepage_position).getFragment() instanceof HomeFragment && data != null) {
//                ((HomeFragment) pageList.get(homepage_position).getFragment()).goBackAction(data);
//            }
            boolean hide_keyboard = true;
            if (data != null) {
                String page_type = data.getString("page_type", "");
                if (page_type.equals("search")) {
                    hide_keyboard = false;
                }
            }
            goBack(data, hide_keyboard);
        }
    }

    public class PageData {
        private int position;
        private String page_type;
        private Bundle data;
        private Fragment fragment;

        public PageData(int p, String pt, Bundle d, Fragment fr) {
            position = p;
            page_type = pt;
            data = d;
            fragment = fr;
        }

        public Fragment getFragment() {
            if (fragment == null) {
                fragment = fragmentCreator.createFragment(page_type, data, position);
                if (fragment instanceof FFragment) {
                    ((FFragment) fragment).setPagePosition(position);
                }
            }
            return fragment;
        }

        public void clearFragment() {
            fragment = null;
        }

        @Override
        public String toString() {
            return "{position: " + position + ", fragment=" + fragment + "}";
        }
    }
}
